/**
 * @typedef {Object} settings
 * @property {String} couch
 */

/**
 * @param  {String} environment
 * @return {settings}
 */
function loadSettings (environment) {
    const settingsFile = require('./settings.json');
    // load config file by environment
    const settings = settingsFile[environment];

    if (!settings) {
        console.error('Can\'t find environments see settings object');
    }
    return settings;
}

module.exports = loadSettings;
