FROM node:8.2.0

EXPOSE 3000
ENV DIR                 /usr/src
ENV PROJECT_NAME        app
ENV PROJECT_DIR         $DIR/$PROJECT_NAME

WORKDIR $PROJECT_DIR

# Install app dependencies
# use changes to package.json to force Docker not to use the cache
# when we change our application's nodejs dependencies:
COPY package-lock.json package.json /tmp/

RUN cd /tmp \
    && npm install \
    && mkdir -p $PROJECT_DIR \
    && mv /tmp/node_modules $PROJECT_DIR

# Copy our application code
ADD . $PROJECT_DIR

RUN npm run build:ts
RUN npm run build:storybook

CMD [ "npm", "run", "start:prod" ]
