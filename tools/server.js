var express = require('express');
var path = require('path');
var app = express();

var DIST_DIR    = path.join(__dirname, '..', 'static');
var HTML_FILE   = path.join(DIST_DIR, 'index.html');
var PORT        = 3000;

app.use(express.static(DIST_DIR));

app.get('*', (req, res) => res.sendFile(HTML_FILE));
app.listen(PORT, () => console.log('Starting app in production mode..'));
