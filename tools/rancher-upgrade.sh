#!/bin/sh

curl -sL POST -H "Content-Type: application/json" \
    -d "{\"push_data\":{\"tag\":\"${WEBHOOK_TAG:-${2:-latest}}\"},\"repository\":{\"repo_name\":\"$CI_REGISTRY_IMAGE\"}}" "${WEBHOOK_URL:-$1}"
