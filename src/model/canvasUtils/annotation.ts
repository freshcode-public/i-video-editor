export function renderAnnotation (ctx: CanvasRenderingContext2D, text: string) {
    text = text || getDefaultTextAnnotation(this.info);

    if (this.group)
        return;

    const coord = {
        left: this.getCenterPoint().x,
        top: this.getCenterPoint().y + (this.height * this.scaleY) / 2
    };

    ctx.save();
    ctx.textAlign = 'center';
    ctx.fillStyle = '#000';

    ctx.translate(coord.left, coord.top);

    wrapText(ctx, text, 0, 15, Infinity, 15);
    ctx.restore();
}

function getDefaultTextAnnotation (info: any = {}): string {
    const create = new Date(info.createAt);
    const update = new Date(info.updateAt);

    const mask = (data: Date) => `${data.getMonth()}/${data.getDate()} ${data.getHours()}:${data.getMinutes()}`;

    const updateText = info.updateAt && `Updated at: ${mask(update)}`;
    const textCreate = info.createAt && `Created at: ${mask(create)}`;

    return [ textCreate, updateText ].join('\n');
}

function wrapText(ctx: CanvasRenderingContext2D, text: string, x: number, y: number, maxWidth: number, lineHeight: number) {
    const cars = text.split('\n');
    let line, words

    for (let ii = 0; ii < cars.length; ii++) {
        line = '';
        words = cars[ii].split(' ');

        for (var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + ' ';
            var metrics = ctx.measureText(testLine);
            var testWidth = metrics.width;

            if (testWidth > maxWidth) {
                ctx.fillText(line, x, y);
                line = words[n] + ' ';
                y += lineHeight;
            }
            else {
                line = testLine;
            }
        }

        ctx.fillText(line, x, y);
        y += lineHeight;
    }
}
