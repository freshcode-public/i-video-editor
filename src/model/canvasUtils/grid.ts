import * as f from 'fabric';

const THRESHOLD_COEFFICIENT = 0.1;

function roundToGrid(value: number, grid: number) {
    return Math.round(value / grid) * grid;
}

export function snapMoveToGrid (gridSize: number, target: f.Object, thresholdCoefficient: number = THRESHOLD_COEFFICIENT) {
    const threshold = gridSize * thresholdCoefficient;
    const width = target.width * target.scaleX;
    const height = target.height * target.scaleY;

    // Distance from snapping points
    const snap = {
        top: roundToGrid(target.top, gridSize),
        left: roundToGrid(target.left, gridSize),
        bottom: roundToGrid(target.top + height, gridSize),
        right: roundToGrid(target.left + width, gridSize)
    };
    // Distance from snapping points
    const dist = {
        top: Math.abs(snap.top - target.top),
        left: Math.abs(snap.left - target.left),
        bottom: Math.abs(snap.bottom - target.top - height),
        right: Math.abs(snap.right - target.left - width)
    };

    if (dist.bottom < dist.top) {
        if (dist.bottom > threshold)
            snap.top = target.top; // don't snap
        else
            snap.top = snap.bottom - height;
    }
    else if (dist.top > threshold)
        snap.top = target.top; // don't snap

    if (dist.right < dist.left) {
        if (dist.right > threshold)
            snap.left = target.left; // don't snap
        else
            snap.left = snap.right - width;
    }
    else if (dist.left > threshold)
        snap.left = target.left; // don't snap

    target.set({
        top: snap.top,
        left: snap.left
    });
}

export function snapScaleToGrid (gridSize: number, target: f.Object, thresholdCoefficient: number = THRESHOLD_COEFFICIENT) {
    const threshold = gridSize * thresholdCoefficient;

    var width = target.getWidth();
    var height = target.getHeight();

    // Closest snapping points
    const snap = {
        top: roundToGrid(target.top, gridSize),
        left: roundToGrid(target.left, gridSize),
        bottom: roundToGrid(target.top + height, gridSize),
        right: roundToGrid(target.left + width, gridSize)
    };

    // Distance from snapping points
    const dist = {
        top: Math.abs(snap.top - target.top),
        left: Math.abs(snap.left - target.left),
        bottom: Math.abs(snap.bottom - target.top - height),
        right: Math.abs(snap.right - target.left - width)
    };

    const attrs = {
        scaleX: target.scaleX,
        scaleY: target.scaleY,
        top: target.top,
        left: target.left
    };

    switch (target['__corner']) {
        case 'tl':
            if (dist.left < dist.top && dist.left < threshold) {
                attrs.scaleX = (width - (snap.left - target.left)) / target.width;
                attrs.scaleY = (attrs.scaleX / target.scaleX) * target.scaleY;
                attrs.top = target.top + (height - target.height * attrs.scaleY);
                attrs.left = snap.left;
            }
            else if (dist.top < threshold) {
                attrs.scaleY = (height - (snap.top - target.top)) / target.height;
                attrs.scaleX = (attrs.scaleY / target.scaleY) * target.scaleX;
                attrs.left = attrs.left + (width - target.width * attrs.scaleX);
                attrs.top = snap.top;
            }
            break;

        case 'mt':
            if (dist.top < threshold) {
                attrs.scaleY = (height - (snap.top - target.top)) / target.height;
                attrs.top = snap.top;
            }
            break;

        case 'tr':
            if (dist.right < dist.top && dist.right < threshold) {
                attrs.scaleX = (snap.right - target.left) / target.width;
                attrs.scaleY = (attrs.scaleX / target.scaleX) * target.scaleY;
                attrs.top = target.top + (height - target.height * attrs.scaleY);
            }
            else if (dist.top < threshold) {
                attrs.scaleY = (height - (snap.top - target.top)) / target.height;
                attrs.scaleX = (attrs.scaleY / target.scaleY) * target.scaleX;
                attrs.top = snap.top;
            }
            break;

        case 'ml':
            if (dist.left < threshold) {
                attrs.scaleX = (width - (snap.left - target.left)) / target.width;
                attrs.left = snap.left;
            }
            break;

        case 'mr':
            if (dist.right < threshold)
                attrs.scaleX = (snap.right - target.left) / target.width;
            break;

        case 'bl':
            if (dist.left < dist.bottom && dist.left < threshold) {
                attrs.scaleX = (width - (snap.left - target.left)) / target.width;
                attrs.scaleY = (attrs.scaleX / target.scaleX) * target.scaleY;
                attrs.left = snap.left;
            }
            else if (dist.bottom < threshold) {
                attrs.scaleY = (snap.bottom - target.top) / target.height;
                attrs.scaleX = (attrs.scaleY / target.scaleY) * target.scaleX;
                attrs.left = attrs.left + (width - target.width * attrs.scaleX);
            }
            break;

        case 'mb':
            if (dist.bottom < threshold)
                attrs.scaleY = (snap.bottom - target.top) / target.height;
            break;

        case 'br':
            if (dist.right < dist.bottom && dist.right < threshold) {
                attrs.scaleX = (snap.right - target.left) / target.width;
                attrs.scaleY = (attrs.scaleX / target.scaleX) * target.scaleY;
            }
            else if (dist.bottom < threshold) {
                attrs.scaleY = (snap.bottom - target.top) / target.height;
                attrs.scaleX = (attrs.scaleY / target.scaleY) * target.scaleX;
            }
            break;
    }

    target.set(attrs);
}
