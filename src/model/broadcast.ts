import * as _ from 'lodash';

export default class Broadcast <T = {}> {
    private _store: T;
    private listeners: Array<Function>;

    public constructor (store: T) {
        this._store = store;
        this.listeners = [];
    }

    public setStore (store: T) {
        this._store = store;
        this.listeners.filter(_.isFunction).forEach(f => f());
    }

    public getStore (): T {
        return this._store;
    }

    public unsubscribe (listener: Function) {
        this.listeners = this.listeners.filter(item => item !== listener);
    }

    public isListener (listener: Function): boolean {
        return !!_.find(this.listeners, item => item === listener)
    }

    public subscribe (listener: Function): Function {
        this.listeners.push(listener);

        return this.unsubscribe.bind(this, listener);
    }
}
