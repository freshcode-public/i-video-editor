import * as f from 'fabric';
import * as _ from 'lodash';

import { isClickZone } from '../../components/canvas/utils';
const fabric = (require('fabric') as any).fabric;

fabric.Photo = fabric.util.createClass(fabric.Object, fabric.Observable, {
    type: 'photo',

    initBehavior: function() {
        this.on('mouseup', (event: fabric.IEvent) => {
            let __btnPlay = this.__btnPlay;

            if (isClickZone(this.canvas, event, __btnPlay)) {
                this.set('hoverCursor', 'pointer');

                this.fire('object:play', { target:  this });
                event.e.stopPropagation() || event.e.preventDefault();
            }
        });

        this.on('mousemove', (event: fabric.IEvent) => {
            const __btnPlay = this['__btnPlay'];
            if (!__btnPlay) return false;
            let cursor = this.selectable ? 'move' : 'default';
            if (isClickZone(this.canvas, event, __btnPlay)) {
                cursor = 'pointer';
            }
            this.set('hoverCursor', cursor);
        });
    },

    initialize: function(src: string, options) {
        this.callSuper('initialize', options);
        this.setControlsVisibility({ bl: false, br: false, mb: false, ml: false, mr: false, mt: false, tl: false, tr: false });

        this._loaded = false;
        this._element = new Image();
        this._element.src = src;

        this._element.onload = () => {
            this._loaded = true;
            this.setCoords();
            this.fire('image:loaded', { target: this });
            this.initBehavior();
        };
    },

    _getOriginalCoord: function () {
        return { top: -(this.height / 2), left: -(this.width / 2) };
    },

    _render: function(ctx: CanvasRenderingContext2D) {
        if (this._loaded && this.visible) {
            this._renderBackground(ctx);
            this._renderBtnPlay(ctx);
        }
    },

    _renderBackground: function(ctx: CanvasRenderingContext2D) {
        const image: HTMLImageElement = this._element;
        const originalCoord: { left: number; top: number; width: number; height: number; } = this._getOriginalCoord();

        let originalSize = { width: image.width, height: image.height };
        let dominantProps = originalSize.width > originalSize.height ? [ 'width', 'height' ] : [ 'height', 'width' ];
        let coefficient = originalSize[dominantProps[0]] / originalSize[dominantProps[1]];

        originalSize[dominantProps[0]] = this[dominantProps[0]];
        originalSize[dominantProps[1]] = this[dominantProps[1]] / coefficient;

        /* background */
        ctx.fillStyle = 'rgba(237, 240, 243, 0.15)';
        ctx.fillRect(originalCoord.left, originalCoord.top, this.width, this.height);
        let offsetImage = (this.height - originalSize.height) / 2;

        /* thumbnail */
        ctx.drawImage(image, originalCoord.left, originalCoord.top + offsetImage, originalSize.width, originalSize.height);
    },

    _renderBtnPlay: function(ctx: CanvasRenderingContext2D) {
        let originalCoord: { left: number; top: number; width: number; height: number; } = this._getOriginalCoord();
        const btn = { left: originalCoord.left + 2, top: originalCoord.top + 3, width: 20, height: 20 };

        this.__btnPlay = btn;

        ctx.save();
        /* bg */
        ctx.fillStyle = 'rgba(47, 59, 76, 0.75)';
        ctx.fillRect(btn.left, btn.top, btn.width, btn.height);

        /* play icon */
        ctx.translate(originalCoord.left, originalCoord.top);
        ctx.fillStyle = '#fff';

        ctx.beginPath();
        ctx.moveTo(7, btn.height - 12);
        ctx.lineTo(btn.width - 4, btn.height - 7);
        ctx.lineTo(7, btn.height - 2);
        ctx.closePath();
        ctx.fill();
        ctx.restore();
    },

    complexity: function() {
        return 5;
    }
});
