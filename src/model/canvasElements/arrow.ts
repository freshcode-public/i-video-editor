import * as f from 'fabric';
const fabric = (require('fabric') as any).fabric;

fabric.Arrow = fabric.util.createClass(fabric.Line, fabric.Observable, {
    type: 'arrow',

    _render: function (ctx: CanvasRenderingContext2D) {
        this.callSuper('_render', ctx);

        // do not render if object is not visible
        if (!this.visible) return;

        ctx.save();
        ctx.translate(this.width / 2, this.height / 2);
        ctx.rotate(Math.atan2(this.height, this.width));

        ctx.beginPath();
        // move 10px in front of line to start the arrow so it does not have the square line end showing in front (0,0)
        ctx.moveTo(10, 0);
        ctx.lineTo(-20, 15);
        ctx.lineTo(-20, -15);
        ctx.closePath();

        ctx.fillStyle = this.stroke;
        ctx.fill();

        ctx.restore();
    },

    complexity: function () {
        return 2;
    }
});
