import * as f from 'fabric';
import * as _ from 'lodash';

import { isClickZone, getStringSize } from '../../components/canvas/utils';
const fabric = (require('fabric') as any).fabric;

fabric.Link = fabric.util.createClass(fabric.Object, fabric.Observable, {
    type: 'link',

    initBehavior: function() {
        this.on('mouseup', function(event: fabric.IEvent) {
            let text = this['_text'];
            if (!text) return false;

            if (isClickZone(this.canvas, event, text)) {
                this.set('hoverCursor', 'pointer');

                this.fire('object:play', { target:  this });
                event.e.stopPropagation() || event.e.preventDefault();
            }
        });

        this.on('mousemove', function(event: fabric.IEvent) {
            const text = this['_text'];
            if (!text) return false;
            let cursor = this.selectable ? 'move' : 'default';
            if (isClickZone(this.canvas, event, text)) {
                cursor = 'pointer';
            }
            this.set('hoverCursor', cursor);
        });
    },

    initialize: function(src: string, options) {
        this.callSuper('initialize', options);
        this.setControlsVisibility({ bl: false, br: false, mb: false, ml: false, mr: false, mt: false, tl: false, tr: false });

        this._loaded = false;
        this._element = new Image();
        this._element.src = src;

        this.initBehavior();
        this._element.onload = () => {
            this._loaded = true;
            this.setCoords();
            this.fire('image:loaded', { target: this });
        };
    },

    _getOriginalCoord: function () {
        return { top: -(this.height / 2), left: -(this.width / 2) };
    },

    _render: function(ctx: CanvasRenderingContext2D) {
        if (this._loaded && this.visible) {
            this._renderBackground(ctx);
            this._renderLinkText(ctx);
        }
    },

    _renderBackground: function(ctx: CanvasRenderingContext2D) {
        const image: HTMLImageElement = this._element;
        const originalCoord: { left: number; top: number; width: number; height: number; } = this._getOriginalCoord();
        let originalSize = { width: image.width, height: image.height };
        let dominantProps = originalSize.width > originalSize.height ? [ 'width', 'height' ] : [ 'height', 'width' ];
        originalSize[dominantProps[0]] = this[dominantProps[0]];
        originalSize[dominantProps[1]] = this[dominantProps[1]];

        /* background */
        ctx.fillStyle = 'rgba(237, 240, 243, 0.3)';
        ctx.fillRect(originalCoord.left, originalCoord.top, this.width, this.height);

        /* thumbnail */
        ctx.drawImage(image, originalCoord.left, originalCoord.top, originalSize.width, originalSize.height);
    },

    _renderLinkText: function(ctx: CanvasRenderingContext2D) {
        let originalCoord: { left: number; top: number; width: number; height: number; } = this._getOriginalCoord();
        let btn = { left: originalCoord.left, top: originalCoord.top + this.height - 15, width: this.width, height: 15 };
        this._text = btn;

        /* bg */
        ctx.fillStyle = 'rgba(47, 59, 76, 1)';
        ctx.fillRect(btn.left, btn.top, btn.width, btn.height);
        ctx.save();

        /* play icon */
        ctx.translate(originalCoord.left, originalCoord.top);
        let text = this.to || 'Hard code text'

        if (text.length == 0)
            text = _.get(this, 'to', '');

        while (getStringSize(text, 'Open Sans', 14).width > this.width - 20) {
            text = text.slice(0, text.length - 1);
        }
        ctx.fillStyle = 'white';
        ctx.font = '14px Open Sans';
        ctx.fillText(text, 10, this.height - 3, this.width - 20);
        ctx.restore();
    },

    complexity: function() {
        return 5;
    }
});
