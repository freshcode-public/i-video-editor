import { GeometryItem, EllipseItem, ActiveAreaItem, ImageItem, NoteItem, PathItem } from './abstractItems';
import * as _ from 'lodash';
import { TYPE_ITEM } from '../constants/editorTypes';

const _base: GeometryItem = {
    _id: null,
    _rev: null,
    doc_type: TYPE_ITEM.GEOMERTIC,
    meta: null,
    type: null,
    info: {},
    angle: 0,
    top: 10,
    left: 10,
    opacity: 1,
    fill: 'rgba(255, 255, 255, 1)',
    stroke: 'rgba(0, 0, 0, 1)',
    strokeWidth: 1,
    height: 100,
    width: 100,
    scaleX: 1,
    scaleY: 1,
    flipX: false,
    flipY: false
}

const Rect: Partial<GeometryItem> = {
    type: 'rect',
    fill: 'transparent',
    stroke: 'rgba(0, 255, 0, 1)'
};

const Triangle: Partial<GeometryItem> = {
    type: 'triangle',
    fill: 'transparent',
    stroke: 'rgba(255, 0, 0, 1)'
};

const Line: Partial<GeometryItem> = {
    type: 'line',
    fill: '#000'
};

const Arrow: Partial<GeometryItem> = {
    type: 'arrow',
    fill: '#000'
};

const Ellipse: Partial<EllipseItem> = {
    type: 'ellipse',
    fill: 'transparent',
    stroke: 'rgba(0, 0, 255, 1)',
    rx: 10,
    ry: 10
};

const ActiveArea: Partial<ActiveAreaItem> = {
    action: null,
    type: 'activeArea',
    fill: 'rgba(255, 0, 255, 0.3)',
    stroke: 'rgba(0, 0, 0, 0)'
};

const Image: Partial<ImageItem> = {
    type: 'image',
    url: null
}

const Photo: Partial<ImageItem> = {
    type: 'photo',
    url: null
}

const Link: any = {
    type: 'link',
    to: null
}

const Path: Partial<PathItem> = {
    type: 'path',
    path: []
}

const Textbox: Partial<NoteItem> = {
    type: 'textbox',
    text: 'Sample text',
    fontFamily: 'Open Sans',
    textAlign: 'left',
    fontSize: 16,
    fill: 'rgba(0, 0, 0, 1)',
    stroke: 'rgba(0, 0, 0, 0)',
    lineHeight: 1,
    fontStyle: 'normal'
}

const Text: Partial<NoteItem> = {
    type: 'text',
}

function setItem (...object: Partial<GeometryItem>[]): GeometryItem {
    return _.assign<GeometryItem>({}, _base, ...object);
}

export const Items = {
    Rect: setItem(Rect),
    Triangle: setItem(Triangle),
    Line: setItem(Line),
    Arrow: setItem(Arrow),
    Ellipse: setItem(Ellipse),
    Activearea: setItem(ActiveArea),
    Image: setItem(Image),
    Text: setItem(Textbox, Text),
    Textbox: setItem(Textbox),
    Photo: setItem(Photo),
    Link: setItem(Link),
    Path: setItem(Path)
};
