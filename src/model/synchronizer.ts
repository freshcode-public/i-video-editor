import { default as PouchDB } from 'pouchdb';
import * as _ from 'lodash';
import { Dispatch } from 'redux';
import * as Promise from 'promise';
import { SYNC_CHANGE, INITIAL_SYNC, SYNC_REMOVE, SYNC_COMPLETE } from '../constants/actionTypes';

PouchDB.plugin(require('pouchdb-load'));

export interface ISyncConfig {
    live?: boolean;
    initialDump?: string
    /** The connect must be reopened after completing the sync */
    reopen?: boolean;
};

export class Synchronizer {
    private isClose: boolean;
    private remoteHost: string;
    private dbname: string;
    private changes: PouchDB.Core.Changes<any>;
    private syncDescriptor: PouchDB.Replication.Sync<any>;
    private initialLoadPromise: Promise<any>;
    private errorLog = (msg) => console.log.bind(console, 'Syncronizer:', msg);
    private dispath: Dispatch<any>;

    public live: boolean;
    public localDB: PouchDB.Database;
    public isRemoteChange: boolean;

    private get localKey (): string {
        return '_local/' + this.dbname;
    }

    constructor (remoteHost: string, dispath: Dispatch<any>) {
        this.dispath = dispath;
        this.isRemoteChange = false;
        this.isClose = false;
        this.remoteHost = remoteHost;
    }

    public stopSync() {
        this.isClose = true;

        if (this.changes)
            this.changes.cancel();

        if (this.syncDescriptor)
            this.syncDescriptor.cancel();
    }

    public startSync(dbname: string, { live, initialDump, reopen }: ISyncConfig) {
        this.dbname = dbname;
        this.localDB = new PouchDB(dbname);
        this.live = live;
        this.isClose = false;
        const remoteDB = new PouchDB(`${this.remoteHost}/${dbname}`);

        // PouchDB -> Redux
        this.changes = this.localDB.changes({ live: live, since: 'now', include_docs: true })
            .on('error', this.errorLog)
            .on('change', (change) => {
                this.isRemoteChange = true;
                this.pouchToRedux(change);
                this.isRemoteChange = false;
            });

        console.time(`initial-load to [${this.dbname}]`);
        this.initialLoadPromise = this.localDB.get(this.localKey) as any;

        this.initialLoadPromise
            .catch((err: PouchDB.Core.Error) => {
                if (err.status !== 404) { // 404 means not found
                    throw err;
                }

                if (_.isString(initialDump)) {
                    console.info('Initial loading from dump:', initialDump);
                    return this.loadDump(initialDump, remoteDB);
                }
            })
            .then(() => {
                this.initialLoadPromiseFinally(remoteDB, live, reopen);
            })
            .catch(() => {
                this.initialLoadPromiseFinally(remoteDB, live, reopen);
            })

        return this.initialLoadPromise;
    }

    private initialLoadPromiseFinally(remoteDB: PouchDB.Database, live: boolean, reopen: boolean) {
        console.timeEnd(`initial-load to [${this.dbname}]`);

        this.initialSync();

        // Sync local PouchDB with remote CouchDB
        this.syncDescriptor = this.localDB
            .sync(remoteDB, { live: live, retry: true, since: 0 })
            .on('error', this.errorLog('Local <-> Remote error'))
            .on('complete', (e) => this.onCompleteSync(reopen, e));
    }

    private onCompleteSync (reopen: boolean, e: PouchDB.Replication.SyncResultComplete<any>) {
        if (!this.isClose) {
            this.dispath({ type: SYNC_COMPLETE, payload: { dbname: this.dbname } });

            if (reopen)
                this.startSync(this.dbname, { initialDump: null, live: true, reopen: false });
        }

    }

    private loadDump (initialDump: string, remoteDB: PouchDB.Database ) {
        return (this.localDB as any)
            .load(initialDump, { proxy: remoteDB })
            .then(() => {
                console.log('Initial load from dump - complete');
                return this.localDB.put({ _id: this.localKey });
            })
            .catch((err) => {
                console.warn('Initial load from dump - error', err);
            });
    }

    private pouchToRedux(change: PouchDB.Core.ChangesResponseChange<any>) {
        if (change.deleted) {
            this.dispath({ type: SYNC_REMOVE, payload: { dbname: this.dbname, id: change.id } });
        } else {
            // update or insert
            this.dispath({ type: SYNC_CHANGE, payload: { dbname: this.dbname, id: change.id, doc: change.doc } });
        }
    }

    private initialSync() {
        return this.localDB.allDocs({ include_docs: true })
            .then((items) => {
                this.isRemoteChange = true;
                const rows = items.rows.map((item) => item.doc);
                this.dispath({ type: INITIAL_SYNC, payload: { dbname: this.dbname, items: rows } })

                this.isRemoteChange = false;
                return rows;
            });
    }
}
