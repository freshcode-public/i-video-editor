import { Synchronizer } from './synchronizer';
import { Dispatch } from 'redux';
import { Map } from './helpers';

const _DBStore: Map<Synchronizer> = {};

export function addSynchronizer(dbname: string, remoteHost: string, dispath: Dispatch<any>) {
    let synchronizer = _DBStore[dbname];

    if (!synchronizer) {
        synchronizer = new Synchronizer(remoteHost, dispath);
        _DBStore[dbname] = synchronizer;
    }

    return synchronizer;
}

export function removeSynchronizer(dbname: string) {
    delete _DBStore[dbname];
}

export function isExist (dbname: string) {
    return Boolean(_DBStore[dbname]);
}

export function getSynchronizer(dbname: string) {
    return _DBStore[dbname];
}
