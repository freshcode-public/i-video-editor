export interface IEditorItem {
    _id?: string;
    _rev?: string;
    doc_type?: string;
    _attachments?: any;
    title?: string;
    nseq?: number;
}
