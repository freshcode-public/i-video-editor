export interface BaseItem {
    _id?: string;
    _rev?: string;
    doc_type: string;
}

export interface Action {
    type: string;
    data?: any;
}

export interface BaseEllipse {
    rx: number;
    ry: number;
}

export interface Coordinates {
    left: number;
    top: number;
    width: number;
    height: number;
}

export interface BaseGeometryMeta {
    fill: string;
    stroke: string;
    strokeWidth: number;
    scaleX: number;
    scaleY: number;
    flipX: boolean;
    flipY: boolean;
    angle: number;
    opacity: number;
}

export interface Path {
    path: Array<any>;
}

export interface Image {
    url: string;
    element?: HTMLMediaElement;
}

export interface Item extends Coordinates, BaseItem {
    type: string;
    info?: {
        createAt?: number;
        updateAt?: number;
    };
    meta?: any;
}

export interface ActiveAreaItem extends Item, BaseGeometryMeta {
    action: Action;
}

export interface GeometryItem extends Item, BaseGeometryMeta {}

export interface NoteItem extends Item, BaseGeometryMeta {
    text: string;
    textAlign: string;
    fontSize: number;
    fontFamily: string;
    lineHeight: number;
    fontStyle: string;
}

export interface EllipseItem extends GeometryItem, BaseEllipse {}

export interface ImageItem extends GeometryItem, Image {}

export interface PathItem extends GeometryItem, Path {}
