import * as _ from 'lodash';
import * as f from 'fabric';

import { BaseItem, Item, GeometryItem, NoteItem, EllipseItem, ImageItem, PathItem } from './abstractItems';
import { Items } from './items';

const fabric = (require('fabric') as any).fabric;

export const Geometries = {
    Activearea: 'Rect'
};

export function getFabricInstance (type: string): f.Object {
    type = _.capitalize(type);
    const customType = Geometries[type];

    if (customType) {
        type = customType;
    }

    return fabric[type];
}

export function newGeometry (target: Item, callback: Function, override: any = {}) {
    const options: Item = _.assign({}, target, override);

    const geometry = _.get<string>(options, 'type');
    const shape = getFabricInstance(geometry);
    const params: Array<any> = [ null, options ];

    if (_.isNil(geometry) || _.isNil(shape)) {
        console.warn('Fabricjs geometry "%s" is not found!', geometry);
        return callback();
    }

    switch (geometry) {
        case 'textbox':
        case 'text':
            params.splice(1, 0, (options as NoteItem).text);
            break;

        case 'line':
        case 'arrow':
            params.splice(1, 0, [ options.left, options.top, options.left + options.width, options.top + options.height ]);
            break;

        case 'link':
        case 'photo':
            params.splice(1, 0, (options as ImageItem).url);
            break

        case 'path':
            params.splice(1, 0, (options as PathItem).path);
            break;

        case 'image':
            if ((options as ImageItem).element) {
                params.splice(1, 0, (options as ImageItem).element);
            } else {
                return fabric.Image.fromURL((options as ImageItem).url, image => callback(image), options);
            }
        break;
    }

    return callback(new (Function.prototype.bind.apply(shape, params)));
}

export function clearItemProps (target: f.Object, undeclaredProps = []): Partial<GeometryItem> {
    const baseItem = Items[_.capitalize(target.type)];

    if (_.isUndefined(baseItem)) {
        return null;
    }

    // Declared item props
    const accessKeys = _.keys(baseItem).concat(undeclaredProps);
    const keys = _.intersection(_.keys(target), accessKeys);
    const clearObject = _.pick(target, keys);

    return clearObject;
}

export function preSaveGeometry (source: Item): Item {
    const item = _.cloneDeep(source);

    switch (item.type) {
        case 'line':
        case 'arrow':
            item.width = 0;
        break;

        case 'ellipse':
            (item as EllipseItem).rx = item.width / 2;
            (item as EllipseItem).ry = item.height / 2;
        break;
    }

    _.set(item, 'info.createAt', Date.now());

    return item;
}
