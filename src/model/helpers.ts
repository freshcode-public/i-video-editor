import * as Redux from 'redux';

export interface Action<T = any> extends Redux.Action {
    payload: T;
    skipHistory?: boolean;
    pending?: boolean;
}

export type IRequestTypes = { REQUEST: string; SUCCESS: string; FAILURE: string; }
const requestTypes: IRequestTypes = { REQUEST: 'REQUEST', SUCCESS: 'SUCCESS', FAILURE: 'FAILURE' };

export function createRequestTypes (base: string): IRequestTypes {
    return [ requestTypes.REQUEST, requestTypes.SUCCESS, requestTypes.FAILURE ].reduce((acc, type) => {
		acc[type] = `${base}_${type}`
		return acc
	}, {} as any);
}

export interface Map<V> {
    [key: string]: V;
}

export function isPromise<T>(promise: Promise<T>): boolean {
    if (promise !== null && typeof promise === 'object') {
        return promise && typeof promise.then === 'function';
    }

    return false;
}
