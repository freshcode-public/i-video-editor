import * as React from 'react';
import * as _ from 'lodash';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import { Toolbar, ToolbarGroup } from '../../../components/toolbar/toolbar';
import { SetToolButton, ToolbarNavigation, GridButton, SetToolbarMode, ColorToolButton, ToolbarSeparator, OpacityTool, FontSizeTool, ActiveAreaButton } from '../../../components/toolbarItems';
import { Items } from '../../../model/items';
import { IStore } from '../../../store/storeInterface';
import * as selectors from '../../../selectors';
import * as Actions from '../../../actions';
import { Button } from '../../../components/ui/button';

export interface IToolbarStore {
    selectItem: Array<Object>;
    trim: { start: number; end: number };
}

export interface IToolbarDispatch {
    onUpdateItem: Function;
}

export interface IToolbarOwnProps {
    onSelectTool: Function;
    onChangeEditableStatus: Function;
    onChangeAnnotationStatus: Function;
    onChangeGrid: Function;
    grid: { size: number; enabled: boolean; };
    editable: boolean;
    tool: Object
    dbname: string;
    annotation?: boolean;
    duration?: number;
    currentTime?: number;
    onChangeCurrentTime?: Function;
}

export interface IPresentationToolbarProps extends IToolbarStore, IToolbarDispatch, IToolbarOwnProps {}
export interface IPresentationToolbarState {}

export class IVideoToolbar extends React.Component<IPresentationToolbarProps, IPresentationToolbarState> {

    private onUpdateItem (items: Array<Object>) {
        this.props.onUpdateItem(items, this.props.dbname);
    }

    private changeCurrentFrame(shift: number) {
        const { onChangeCurrentTime } = this.props;
        const { currentTime, duration } = this.props;
        const value = currentTime + shift;

        if (value <= duration && value >= 0)
            onChangeCurrentTime(value);
    }

    private isActiveItem(types: Array<string>) {
        return !!_.find(this.props.selectItem, (i: any) => _.includes(types, i.type));
    }

    private get playerProps() {
        const { editable } = this.props;

        var currentTime;
        var duration;

        if (!editable) {
            currentTime = this.props.currentTime - _.get(this.props, 'trim.start', 0);
            duration = _.get(this.props, 'trim.end', this.props.duration) - this.props.trim.start;
        } else {
            currentTime = this.props.currentTime;
            duration = this.props.duration;
        }

        if (currentTime < 0)
            currentTime = 0;

        return { currentTime, duration };
    }


    public render () {
        return (
            <Toolbar
                onSelectTool={this.props.onSelectTool}
                selectTool={this.props.tool}
                selectItem={this.props.selectItem}
                onUpdateItem={this.onUpdateItem.bind(this)}
            >
                <ToolbarGroup position='left'>
                    <ToolbarNavigation
                        maxPosition={this.playerProps.duration}
                        changePosition={this.changeCurrentFrame.bind(this)}
                        currentPosition={this.playerProps.currentTime} />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode={''} view={this.props.editable}>
                    <SetToolbarMode value='text' icon='input' />
                    <SetToolbarMode value='geometry' icon='rect' />
                    <SetToolbarMode value='gear' icon='gear' />

                </ToolbarGroup>

                <ToolbarGroup position='left' mode='gear' view={this.props.editable} back>
                    <GridButton
                        onChangeGrid={this.props.onChangeGrid}
                        grid={this.props.grid} />
                    <Button className={`btn-default ${this.props.annotation ? 'active' : ''}`}
                        title='Change annotation status'
                        children='Change annotation status'
                        onClick={this.props.onChangeAnnotationStatus} />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode='text' view={this.props.editable} back>
                    <SetToolButton value={Items.Textbox} icon='input' />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode='opacity' view={this.props.editable} back>
                    <OpacityTool />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode='geometry' view={this.props.editable} back>
                    <SetToolButton value={Items.Rect} icon='rect' />
                    <SetToolButton value={Items.Triangle} icon='triangle' />
                    <SetToolButton value={Items.Line} icon='line' />
                    <SetToolButton value={Items.Arrow} icon='arrow' />
                    <SetToolButton value={Items.Ellipse} icon='ellipse' />
                    <SetToolButton value={Items.Path} icon='pencil' />
                    <SetToolButton value={Items.Activearea} children={'ActiveArea'} />
                </ToolbarGroup>

                <ToolbarGroup position='left' view={this.props.editable && !_.isEmpty(this.props.selectItem)}>
                    <ToolbarSeparator />
                    <ColorToolButton />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode={''} view={this.props.editable && !_.isEmpty(this.props.selectItem)}>
                    <SetToolbarMode value='opacity' icon='opacity' />
                </ToolbarGroup>

                <ToolbarGroup position='left' view={this.props.editable && this.isActiveItem([ 'text', 'textbox' ])}>
                    <FontSizeTool />
                </ToolbarGroup>

                <ToolbarGroup position='left' view={this.props.editable && this.isActiveItem([ 'activeArea' ])}>
                    <ActiveAreaButton actions={[
                        { typeView: 'number', type: 'goToSlide', label: 'Go To Sec', minValue: 0, maxValue: this.props.duration },
                        { typeView: 'number', type: 'delayedPlayback', label: 'Delayed playback', minValue: 0, maxValue: this.props.duration },
                        { typeView: null, type: 'stopTimer', label: 'Auto pause' },
                        { typeView: null, type: 'stop', label: 'Pause' },
                        { typeView: null, type: 'play', label: 'Play' }
                    ]} />
                </ ToolbarGroup>

                <ToolbarGroup position='right' view={this.props.editable}>
                    <Button className='btn-default btn-icon floppy' title='Save' onClick={this.props.onChangeEditableStatus.bind(this, false)} />
                </ToolbarGroup>
                <ToolbarGroup position='right' view={!this.props.editable}>
                    <Button className='btn-default btn-icon pencil' title='Edit' onClick={this.props.onChangeEditableStatus.bind(this, true)} />
                </ToolbarGroup>
            </Toolbar>
        )
    }
}

function mapStateToProps (store: IStore): IToolbarStore {
    const selectItemsId = selectors.editorItem.getEditorSelectItemsId(store);
    const selectItem = _.values(_.pick(selectors.editorItem.getEditorItems(store), selectItemsId));

    return {
        selectItem: selectItem,
        trim: selectors.iVideo.getTrim(store)
    };
}

function mapDispatchToProps (dispatch: Dispatch<any>): IToolbarDispatch {
    return {
        onUpdateItem: bindActionCreators(Actions.updateItem, dispatch),
    };
}

export default connect<IToolbarStore, IToolbarDispatch, IToolbarOwnProps>(mapStateToProps, mapDispatchToProps)(IVideoToolbar);
