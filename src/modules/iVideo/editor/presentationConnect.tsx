import * as React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as _ from 'lodash';

import { editorComponent, containers } from '../../../index';
import { IStore } from '../../../store/storeInterface';
import * as Actions from '../../../actions';
import * as selectors from '../../../selectors';
import { IIVideoDispatch, IIVideoPropsOwn, IIVideoStore, IVideo, IIVideoProps } from './iVideo';
import { TimeLine } from './timeline';

function mapStateToProps (store: IStore, ownProps: IIVideoPropsOwn): IIVideoStore {
    const completedSync = _.get(store.syncronizer, [ ownProps.dbname, 'completed' ], ownProps.completedSync);

    return {
        items: _.map(selectors.editorItem.getEditorItemsId(store), id => selectors.editorItem.getEditorItem(store, id)),
        completedSync: completedSync,
        trim: selectors.iVideo.getTrim(store)
    };
}

function mapDispatchToProps (dispatch: Dispatch<any>): IIVideoDispatch {
    return {
        onObjectCreated: bindActionCreators(Actions.createItem, dispatch),
        onUpdateItem: bindActionCreators(Actions.updateItem, dispatch),
        onRemoveItem: bindActionCreators(Actions.removeItem, dispatch),
        onSelectItem: bindActionCreators(Actions.selectItem, dispatch),
    };
}

export const IVideoConnect = connect<IIVideoStore, IIVideoDispatch, IIVideoPropsOwn>(mapStateToProps, mapDispatchToProps)(IVideo);


function mapStateToPropsEditor (store: IStore, ownProps) {
    const selectItemsId = selectors.editorItem.getEditorSelectItemsId(store);
    const selection = ownProps.selection;

    const items = selectors.editorItem.getEditorItemsByMeta(store, (item) =>
        _.inRange(ownProps.currentTime, _.get(item, 'meta.start'), _.get(item, 'meta.end')) || (_.includes(selectItemsId, item._id) && selection)
    );

    return {
        children: _.map(items, (id) => (<containers.EditorItem key={id} id={id} select={_.includes(selectItemsId, id)} />)),
    }
}

export const EditorConnect = connect(mapStateToPropsEditor)(editorComponent.Editor as any);


function mapStateToPropsTimeLine (store: IStore, ownProps) {
    return {
        items: _.map(selectors.editorItem.getEditorItemsId(store), (id) => selectors.editorItem.getEditorItem(store, id)),
        selectItemsId: selectors.editorItem.getEditorSelectItemsId(store),
        trim: selectors.iVideo.getTrim(store)
    }
}

function mapDispatchToPropsTimeLine (dispatch: Dispatch<any>) {
    return {
        onSelectItem: bindActionCreators(Actions.selectItem, dispatch),
        onChangeTrim: bindActionCreators(Actions.changeTrim, dispatch)
    };
}
export const TimeLineConnect = connect(mapStateToPropsTimeLine, mapDispatchToPropsTimeLine)(TimeLine as any);
