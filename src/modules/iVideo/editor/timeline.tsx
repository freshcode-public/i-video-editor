import * as React from 'react';
import * as _ from 'lodash';
const InputRange: any = require('react-input-range');

export interface IProps {
    duration: number;
    currentTime: number;
    items: Array<Object>;
    selectItemsId: Array<string>;
    onObjectModified: Function;
    onSelectItem: Function;
    changeCurrentTime: Function;
    onChangeTrim?: Function;
}

export interface IRange {
    max?: number;
    min?: number;
}

export interface IState {
    trim?: { start: number; end: number };
    currentTime?: number;
    isUpdated?: boolean;
}


export class TimeLine extends React.Component<IProps, IState> {
    public state: IState = {
        trim: {
            end: _.get(this.props, 'trim.end', this.props.duration),
            start: _.get(this.props, 'trim.start', 0)
        },
        isUpdated: false,
        currentTime: this.props.currentTime
    };

    public componentWillReceiveProps(nextProps: IProps) {
        if (!this.state.isUpdated) {
            this.setState({
                currentTime: nextProps.currentTime,
                trim: {
                    end: _.get(nextProps, 'trim.end', nextProps.duration),
                    start: _.get(nextProps, 'trim.start', 0)
                }
            });
        }
    }

    private handlerOnSelectItem(target: any) {
        const { onSelectItem } = this.props;

        if (_.has(target, '_id') && !this.isSelectItem(_.get(target, '_id'))) {
            return onSelectItem([ target._id ]);
        }

        onSelectItem([]);
    }

    private isSelectItem (id: string): boolean {
        return _.includes(this.props.selectItemsId, id);
    }

    private changeItemTime(itemId, value) {
        let item = _.find(this.props.items, (i: any) => i._id === itemId);
        if (item) {
            item = _.assign({}, item, {
                meta: {
                    end: value.max,
                    start: value.min
                }
            });

            this.props.onObjectModified(item);
        }
    }

    private changeCurrentTime(value: number) {
        const { changeCurrentTime } = this.props;

        this.setState({ isUpdated: false });
        changeCurrentTime(value);
    }

    private changeTrimPosition(value: IRange) {
        const { onChangeTrim } = this.props;

        this.setState({ isUpdated: false });
        if (_.isFunction(onChangeTrim)) {
            onChangeTrim({ start: value.min, end: value.max }, 'two');
        }
    }

    private renderPlayerTrack() {
        const { duration, items } = this.props;
        const { currentTime } = this.state;
        const height = 43.6 * (items.length + 1);
        const left = [ 100 / duration * currentTime, '%' ].join('');
        const onChangeLocalCurentTime = (v: number) => this.setState({ currentTime: v, isUpdated: true });

        return (
            <div className='tracker palyer'>
                <div className='info' onClick={this.handlerOnSelectItem.bind(this, null)}>Video</div>
                <div className='content'>
                    <div className='slider'>
                        <InputRange
                            minValue={0}
                            maxValue={duration}
                            value={currentTime}
                            onChangeComplete={this.changeCurrentTime.bind(this)}
                            onChange={onChangeLocalCurentTime.bind(this)} />
                        <div className='walker video-progress' style={{ left, height }} />
                    </div>
                </div>
            </div>
        );
    }

    private renderItemTrack(item): JSX.Element {
        const { duration } = this.props;

        const show = !!item && _.has(item, ['meta', 'end']) && _.has(item, ['meta', 'start']);
        const className = [ 'tracker item', this.isSelectItem(item._id) ? 'selected' : '' ].join(' ').trim();
        const alias = _.get(item, 'alias', item.type);

        return show && (
            <div key={item._id} className={className}>
                <div className='info'>
                    <div className='select' onClick={this.handlerOnSelectItem.bind(this, item)} />
                    <label className='alias' children={alias} />
                </div>
                <div className='content'>
                    <div className='slider'>
                        <WrapperInputRange minValue={0} maxValue={duration} value={{ min: item.meta.start, max: item.meta.end }} onChange={this.changeItemTime.bind(this, item._id)} />
                    </div>
                </div>
            </div>
        );
    }

    private renderTrimTrack() {
        const { duration, items } = this.props;
        const height = 43.6 * items.length;
        const { trim } = this.state;
        const calcLeft = (position: number) =>  [ 100 / duration * position, '%' ].join('');
         const onChangeTrimLocal = (v: IRange) =>  this.setState({ trim: { start: v.min, end: v.max }, isUpdated: true });

        return (
            <div className='tracker trim'>
                <div className='info' onClick={this.handlerOnSelectItem.bind(this, null)}>Trim</div>
                <div className='content'>
                    <div className='slider'>
                        <InputRange
                            minValue={0}
                            maxValue={duration}
                            onChange={onChangeTrimLocal.bind(this)}
                            onChangeComplete={this.changeTrimPosition.bind(this)}
                            value={{ min: trim.start, max: trim.end }} />
                        <div className='walker trim' style={{ left: calcLeft(trim.start), height }} />
                        <div className='walker trim' style={{ left: calcLeft(trim.end), height }} />
                    </div>
                </div>
            </div>
        );
    }

    public render() {
        return (
            <div className='time-line--component'>
                {this.renderPlayerTrack()}
                {this.renderTrimTrack()}
                {_.map(this.props.items, (i) => this.renderItemTrack(i))}
            </div>
        );
    }
}

class WrapperInputRange extends React.Component<any,any> {
    public state: {
        changeStart: boolean;
        value: any
    } = { value: this.props.value, changeStart: false };

    constructor(props) {
        super(props);

        this.up = _.debounce(props.onChange.bind(this), 300);
        this.update = this.update.bind(this);
        this.onChangeStart = this.onChangeStart.bind(this);
        this.onChangeComplete = this.onChangeComplete.bind(this);
    }

    private update(value) {
        this.setState({ value: value });
        this.up(value);
    }

    private up: Function;

    public componentWillReceiveProps(nextProp) {
        if (!this.state.changeStart) {
            this.setState({ value: nextProp.value });
        }
    }

    private onChangeStart () {
        this.setState({ changeStart: true });
    }

    private onChangeComplete () {
        this.setState({ changeStart: false });
    }

    render () {
        const props = {
            onChange: this.update,
            value: this.state.value,
            maxValue: this.props.maxValue,
            minValue: this.props.minValue
        }

        return <InputRange {...props} onChangeStart={this.onChangeStart} onChangeComplete={this.onChangeComplete} />
    }
}
