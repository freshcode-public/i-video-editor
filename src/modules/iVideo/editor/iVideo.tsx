import * as React from 'react';
import * as _ from 'lodash';
import * as keymaster from 'keymaster';

import { default as Player } from '../../../components/player/player';
import { editorComponent, containers, uiComponent } from '../../../index';
import { default as IVideoToolbar } from './toolbar';
import { preSaveGeometry } from '../../../model/geometry';
import { EditorConnect, TimeLineConnect } from './presentationConnect';
import { SensitiveScreen } from '../../../components/sensitiveScreen';

export interface IIVideoPropsShared {
    completedSync?: boolean;
    trim?: { start: number, end: number };
}

export interface IIVideoPropsOwn extends IIVideoPropsShared {
    url?: string;
    dbname?: string;

    editorSize?: { width: number; height: number };
    originalSize?: { width: number; height: number };

}

export interface IIVideoDispatch {
    onObjectCreated?: Function;
    onUpdateItem?: Function;
    onRemoveItem?: Function;
    onSelectItem?: Function;
}

export interface IIVideoStore extends IIVideoPropsShared{
    children?: any;
    items?: Array<any>;
}

export interface IIVideoProps extends IIVideoPropsOwn, IIVideoDispatch, IIVideoStore {
}

export interface IIVideoState {
    duration?: number;
    currentTime?: number;
    canPaly?: boolean;

    tool?: any;
    editable?: boolean;
    grid?: IGridProps;
    annotation?: boolean;
    fullscreen?: boolean;
}

export interface IGridProps {
    enabled: boolean;
    size: number;
}

export interface IRefs {
    [name: string]: any;
    player: Player;
    sensitiveScreen: SensitiveScreen;
}

export class IVideo extends React.Component<IIVideoProps, IIVideoState> {
    private timeoutId;
    public refs: IRefs;
    public state: IIVideoState;

    public static get defaultProps(): IIVideoProps {
        return {
            editorSize: editorComponent.Editor.defaultProps.editorSize,
            originalSize: editorComponent.Editor.defaultProps.editorSize,
            trim: null
        };
    }

    public constructor () {
        super();

        this.state = {
            canPaly: false,
            duration: 1,
            currentTime: 0,
            annotation: false,
            editable: false,
            grid: { enabled: false, size: 50 },
            tool: null,
            fullscreen: false
        };

        this.onTimeUpdate = this.onTimeUpdate.bind(this);
        this.onCanPlay = this.onCanPlay.bind(this);
        this.changeCurrentTime = this.changeCurrentTime.bind(this);
        this.changeModeFullScreen = this.changeModeFullScreen.bind(this);
    }

    public componentDidMount() {
        keymaster('left', this.shiftCurrentTime.bind(this, -5));
        keymaster('right', this.shiftCurrentTime.bind(this, 5));
        keymaster('space', this.togglePlay.bind(this));
    }

    public componentWillUnmount() {
        keymaster.unbind('left');
        keymaster.unbind('right');
        keymaster.unbind('space');
    }

    private shiftCurrentTime(value: number) {
        var currentTime = this.state.currentTime + value;
        this.changeCurrentTime(currentTime);
    }

    private togglePlay() {
        const player = this.refs.player;
        player && player.togglePlay();
    }

    public componentDidUpdate() {
        const player = this.refs.player;

        if (!this.state.editable && !player.state.paused) {
            _.filter(this.props.items, (item) => {
                let type = _.get(item, 'type') == 'activeArea';
                let subType = _.includes([ 'stopTimer', 'delayedPlayback' ], _.get(item, 'action.type'));
                let currentTime = _.get(item, 'meta.start') === this.state.currentTime;

                return type && subType && currentTime;
            })
            .forEach((item) => {
                switch (true) {
                    case item.action.type == 'stopTimer':
                        return player && player.pause();

                    case item.action.type == 'delayedPlayback' && !!item.action.data && !this.timeoutId:
                        let delay = 5; // default
                        try {
                            delay = parseInt(item.action.data, 10);
                        } catch(e) { /* ignore */ }

                        setTimeout(() => {
                            player && player.pause();
                        }, 800);
                        this.timeoutId = setTimeout(() => {
                            this.timeoutId = null;
                            player && player.play()
                        }, delay * 1000)
                    break;
                }
        });

        }
    }

    private changeModeFullScreen() {
        const sensitiveScreen = this.refs.sensitiveScreen;
        this.setState({ fullscreen: true });
        sensitiveScreen.requestFullscreen();
    }

    private onObjectCreated (dim) {
        const { tool } = this.state;

        if (!tool || !_.isFunction(this.props.onObjectCreated)) return;

        const item = _.assign({}, tool, dim, { type: tool.type, meta: {
            start: this.state.currentTime,
            end: _.clamp(this.state.currentTime + 10, 0, this.state.duration) }
        });

        this.props.onObjectCreated(preSaveGeometry(item), this.props.dbname);
        this.onSelectTool(undefined);
    }

    private onSelectTool (tool) {
        this.setState({ tool });
    }

    private onRemoveItem(item) {
        if (_.isFunction(this.props.onRemoveItem)) {
            this.props.onRemoveItem(item, this.props.dbname)
        }
    }

    private onUpdateItem(item) {
        if (_.isFunction(this.props.onUpdateItem)) {
            this.props.onUpdateItem(item, this.props.dbname);
        }
    }

    private onChangeEditableStatus (mode: boolean) {
        if (!mode) {
            this.setState({
                editable: false,
                annotation: false,
                canPaly: false,
                grid: { enabled: false, size: this.state.grid.size },
                tool: null
            });

            this.refs.player.load();
            return
        }

        if (this.state.canPaly && this.props.completedSync) {
            this.setState({ editable: mode });
        }
    }

    private onChangeGrid (grid: IGridProps) {
        this.setState({ grid: grid });
    }

    private onTimeUpdate(event: Event, element: HTMLVideoElement) {
        this.setState({ currentTime: Math.floor(element.currentTime) });
    }

    private onCanPlay(event: Event, element: HTMLVideoElement) {
        this.setState({
            duration: Math.floor(element.duration),
            currentTime: Math.floor(element.currentTime),
            canPaly: true
        });
    }

    private bindPlayerEvents() {
        return {
            onTimeUpdate: this.onTimeUpdate,
            onCanPlay: this.onCanPlay
        };
    }

    private onChangeAnnotationStatus () {
        this.setState((prevState: IIVideoState) => ({ annotation: !prevState.annotation }));
    }

    private changeCurrentTime(value: number) {
        this.refs.player.seek(Math.floor(value), true);
    }

    private handleCancelFullscreen() {
        this.state.fullscreen && this.setState({ fullscreen: false });
    }

    private onObjectActiveArea(e: { target: any }) {
        const action = e.target.action;
        const player = this.refs.player;

        switch (action.type) {
            case 'goToSlide':
                this.changeCurrentTime(action.data);
                break;

            case 'stop':
                player.pause();
                break;

            case 'play':
            case 'stopTimer':
            case 'delayedPlayback':
                player.play();
                break;
        }
    }

    private get editorSize() {
        const windowSize = {
            width: _.get(parent.document, 'body.clientWidth', window.innerWidth),
            height: _.get(parent.document, 'body.clientHeight', window.innerHeight)
        };

        return this.state.fullscreen ? windowSize : this.props.editorSize;
    }

    private get trim () {
        const trim = this.state.editable
            ? null
            : { max: _.get(this.props, 'trim.end', this.state.duration), min: _.get(this.props, 'trim.start', 0) };
        return trim
    }

    private renderEditor() {
        if (!this.props.completedSync || !this.state.canPaly) {
            return <uiComponent.CircularSpinner />
        }
        const state: any = _.pick(this.state, [ 'annotation', 'grid', 'currentTime' ]);
        const props: any = _.pick(this.props, [ 'originalSize' ]);

        return (
            <EditorConnect {...state} {...props}
                editorSize={this.editorSize}
                drawingMode={_.get(this.state.tool, 'type') === 'path'}
                selectable={this.state.editable}
                selection={this.state.editable}
                onObjectSelected={this.props.onSelectItem}
                onRemoveObject={this.onRemoveItem.bind(this)}
                onObjectModified={this.onUpdateItem.bind(this)}
                onObjectActiveArea={this.onObjectActiveArea.bind(this)}
                onObjectCreated={this.onObjectCreated.bind(this)} />
        );
    }

    private renderPalyer () {
        return (
            <div style={{ position: 'absolute', height: '100%', width: '100%', top: 0, left: 0 }}>
                <Player
                    controls
                    ref='player'
                    events={this.bindPlayerEvents()}
                    trim={this.trim}
                    wrappingEvents={{ onFullscreen: this.changeModeFullScreen }}
                >
                    { this.props.url && <source src={this.props.url} type={'video/mp4'} /> }
                </Player>
            </div>
        );
    }

    private renderTimeLine() {
        if (!this.state.editable) {
            return null;
        }

        return (
            <TimeLineConnect
                changeCurrentTime={this.changeCurrentTime}
                currentTime={this.state.currentTime}
                onObjectModified={this.onUpdateItem.bind(this)}
                duration={this.state.duration} />
        )
    }


    public render () {
        const state: any = _.pick(this.state, [ 'annotation', 'grid', 'editable', 'tool', 'duration', 'currentTime' ]);

        return (
            <div className='modules--presentation'>
                <IVideoToolbar {...state}
                    onChangeGrid={this.onChangeGrid.bind(this)}
                    dbname={this.props.dbname}
                    onChangeAnnotationStatus={this.onChangeAnnotationStatus.bind(this)}
                    onChangeEditableStatus={this.onChangeEditableStatus.bind(this)}
                    onChangeCurrentTime={this.changeCurrentTime}
                    onSelectTool={this.onSelectTool.bind(this)} />
                <div className='presentation--container' style={{ ...this.editorSize }}>
                    <SensitiveScreen ref='sensitiveScreen' screenSize={this.props.editorSize} fullscreen cancelFullScreen={this.handleCancelFullscreen.bind(this)}>
                        {this.renderPalyer()}
                        {this.renderEditor()}
                    </SensitiveScreen>
                </div>
                {this.renderTimeLine()}
            </div>
        );
    }
}
