import * as React from 'react';
import * as _ from 'lodash';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import { Toolbar, ToolbarGroup } from '../../../components/toolbar/toolbar';
import { SetToolButton, ToolbarNavigation, GridButton, SetToolbarMode, ColorToolButton, ToolbarSeparator, OpacityTool, FontSizeTool, ActiveAreaButton } from '../../../components/toolbarItems';
import { Items } from '../../../model/items';
import { IStore } from '../../../store/storeInterface';
import * as selectors from '../../../selectors';
import * as Actions from '../../../actions';
import { Button } from '../../../components/ui/button';

export interface IToolbarStore {
    currentSlide: Object;
    slides: Array<string>;
    currentSlideId: string;
    selectItem: Array<Object>;
}

export interface IToolbarDispatch {
    onSelectSlide: Function;
    onCreateSlide: Function;
    onRemoveSlide: Function;
    onSetAttachment: Function;
    onRemoveAttachment: Function;
    onUpdateItem: Function;
}

export interface IToolbarOwnProps {
    onSelectTool: Function;
    onChangeEditableStatus: Function;
    onChangeAnnotationStatus: Function;
    onChangeGrid: Function;
    grid: { size: number; enabled: boolean; };
    editable: boolean;
    tool: Object
    dbname: string;
    annotation?: boolean;
}

export interface IPresentationToolbarProps extends IToolbarStore, IToolbarDispatch, IToolbarOwnProps {}
export interface IPresentationToolbarState {}

export class PresentationToolbar extends React.Component<IPresentationToolbarProps, IPresentationToolbarState> {

    private getIndexSlide(slideId = this.props.currentSlideId) {
        return _.indexOf(this.props.slides, slideId);
    }

    private onUpdateItem (items: Array<Object>) {
        this.props.onUpdateItem(items, this.props.dbname);
    }

    private onSelectSlide (diff: number) {
        const currentSlideIndex = this.getIndexSlide();
        const index = currentSlideIndex + diff;
        const id = this.props.slides[index];

        if (_.includes(this.props.slides, id)) {
            this.props.onSelectSlide(id, this.props.dbname);
        }
    }

    private onCreateSlide () {
        this.props.onCreateSlide({}, this.props.dbname);
    }

    private onRemoveSlide () {
        this.props.onRemoveSlide(this.props.currentSlide, this.props.dbname);
    }

    private isActiveItem(types: Array<string>) {
        return !!_.find(this.props.selectItem, (i: any) => _.includes(types, i.type));
    }

    public render () {
        return (
            <Toolbar
                onSelectTool={this.props.onSelectTool}
                selectTool={this.props.tool}
                selectItem={this.props.selectItem}
                onUpdateItem={this.onUpdateItem.bind(this)}
            >
                <ToolbarGroup position='left'>
                    <ToolbarNavigation
                        maxPosition={this.props.slides.length}
                        changePosition={this.onSelectSlide.bind(this)}
                        currentPosition={this.getIndexSlide() + 1} />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode={''} view={this.props.editable}>
                    <SetToolbarMode value='text' icon='input' />
                    <SetToolbarMode value='geometry' icon='rect' />
                    <SetToolbarMode value='gear' icon='gear' />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode='gear' view={this.props.editable} back>
                    <GridButton
                        onChangeGrid={this.props.onChangeGrid}
                        grid={this.props.grid} />
                    <Button className={`btn-default ${this.props.annotation ? 'active' : ''}`}
                        title='Change annotation status'
                        children='Change annotation status'
                        onClick={this.props.onChangeAnnotationStatus} />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode='text' view={this.props.editable} back>
                    <SetToolButton value={Items.Textbox} icon='input' />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode='opacity' view={this.props.editable} back>
                    <OpacityTool />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode='geometry' view={this.props.editable} back>
                    <SetToolButton value={Items.Rect} icon='rect' />
                    <SetToolButton value={Items.Triangle} icon='triangle' />
                    <SetToolButton value={Items.Line} icon='line' />
                    <SetToolButton value={Items.Arrow} icon='arrow' />
                    <SetToolButton value={Items.Ellipse} icon='ellipse' />
                    <SetToolButton value={Items.Path} icon='pencil' />
                    <SetToolButton value={Items.Activearea} children='ActiveArea'/>
                </ToolbarGroup>

                <ToolbarGroup position='left' view={this.props.editable && !_.isEmpty(this.props.selectItem)}>
                    <ToolbarSeparator />
                    <ColorToolButton />
                </ToolbarGroup>

                <ToolbarGroup position='left' mode={''} view={this.props.editable && !_.isEmpty(this.props.selectItem)}>
                    <SetToolbarMode value='opacity' icon='opacity' />
                </ToolbarGroup>

                <ToolbarGroup position='left' view={this.props.editable && this.isActiveItem([ 'text', 'textbox' ])}>
                    <FontSizeTool />
                </ToolbarGroup>

                <ToolbarGroup position='right' view={this.props.editable}>
                    <Button className='btn-default btn-icon image' title='Add background' onFiles={this.props.onSetAttachment.bind(null, this.props.currentSlide, 'background', this.props.dbname)} />
                    <Button className='btn-default btn-icon removeImage' title='Remove background' onClick={this.props.onRemoveAttachment.bind(null, this.props.currentSlide, 'background', this.props.dbname)} />
                    <Button className='btn-default btn-icon plus' title='Add slide' onClick={this.onCreateSlide.bind(this)} />
                    <Button className='btn-default btn-icon delete' title='Remove slide' onClick={this.onRemoveSlide.bind(this)} />
                </ToolbarGroup>

                <ToolbarGroup position='left' view={this.props.editable && this.isActiveItem([ 'activeArea' ])}>
                    <ActiveAreaButton actions={[
                        { typeView: 'option', type: 'goToSlide', label: 'Go to Slide', values: _.map(this.props.slides, (s, i) => ({ value: s, label: `Slide #${i + 1}` }) )}
                    ]} />
                </ToolbarGroup>


                <ToolbarGroup position='right' view={this.props.editable}>
                    <Button className='btn-default btn-icon floppy' title='Save' onClick={this.props.onChangeEditableStatus.bind(this, false)} />
                </ToolbarGroup>
                <ToolbarGroup position='right' view={!this.props.editable}>
                    <Button className='btn-default btn-icon pencil' title='Edit' onClick={this.props.onChangeEditableStatus.bind(this, true)} />
                </ToolbarGroup>
            </Toolbar>
        )
    }
}

function mapStateToProps (store: IStore): IToolbarStore {
    const currentSlide = selectors.presentation.getSlideId(store);
    const selectItemsId = selectors.editorItem.getEditorSelectItemsId(store);
    const selectItem = _.values(_.pick(selectors.editorItem.getEditorItems(store), selectItemsId));

    return {
        currentSlide: selectors.presentation.getSlide(store, currentSlide),
        slides: selectors.presentation.getSlides(store),
        currentSlideId: currentSlide,
        selectItem: selectItem
    };
}

function mapDispatchToProps (dispatch: Dispatch<any>): IToolbarDispatch {
    return {
        onUpdateItem: bindActionCreators(Actions.updateItem, dispatch),
        onSelectSlide: bindActionCreators(Actions.selectSlidePresentation, dispatch),
        onCreateSlide: bindActionCreators(Actions.createSlidePresentation, dispatch),
        onRemoveSlide: bindActionCreators(Actions.removeSlidePresentation, dispatch),
        onSetAttachment: bindActionCreators(Actions.setAttachmentPresentation, dispatch),
        onRemoveAttachment: bindActionCreators(Actions.removeAttachmentPresentation, dispatch),
    };
}

export default connect<IToolbarStore, IToolbarDispatch, IToolbarOwnProps>(mapStateToProps, mapDispatchToProps)(PresentationToolbar);
