import * as React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as _ from 'lodash';

import { editorComponent, containers } from '../../../index';
import PresentationToolbar from './toolbar';
import { IStore } from '../../../store/storeInterface';
import * as Actions from '../../../actions';
import * as selectors from '../../../selectors';
import { preSaveGeometry } from '../../../model/geometry';
import { BasePresentation } from '../basePresentation';

const { Editor } = editorComponent;
const { EditorItem } = containers;

export interface IPresentationStore {
    completedSync?: boolean;
    currentSlideId?: string;
    slides?: Array<string>;
    backgroundURL?: string;
    children?: any;
}

export interface IPresentationDispatch {
    onObjectCreated?: Function;
    onUpdateItem?: Function;
    onRemoveItem?: Function;
    onSelectItem?: Function;
    onSelectSlide?: Function;
}

export interface IPresentationOwnProps {
    completedSync?: boolean;
    dbname: string;
    editorSize?: { width: number; height: number };
    originalSize?: { width: number; height: number };
}

export interface IPresentationProps extends IPresentationOwnProps, IPresentationDispatch, IPresentationStore {}

export interface IGridProps {
    enabled: boolean;
    size: number;
}

export interface IPresentationState {
    tool?: any;
    editable?: boolean;
    grid?: IGridProps;
    annotation?: boolean;
}

export class Presentation extends React.Component<IPresentationProps, IPresentationState> {
    public state: IPresentationState = {
        tool: undefined,
        editable: false,
        annotation: false,
        grid: { enabled: false, size: 50 }
    };

    private onObjectCreated (dim) {
        const { tool } = this.state;

        if (!tool || !_.isFunction(this.props.onObjectCreated)) return;

        const item = _.assign({}, tool, dim, { type: tool.type, meta: this.props.currentSlideId });

        this.props.onObjectCreated(preSaveGeometry(item), this.props.dbname);
        this.onSelectTool(undefined);
    }

    private onSelectTool (tool) {
        this.setState({ tool });
    }

    private onChangeEditableStatus (mode: boolean) {
        this.setState({ editable: mode });
    }

    private onChangeGrid (grid: IGridProps) {
        this.setState({ grid: grid });
    }

    private onChangeAnnotationStatus () {
        this.setState((prevState: IPresentationState) => ({ annotation: !prevState.annotation }));
    }

    private onSelectSlide(slide) {
        if (_.isFunction(this.props.onSelectSlide)) {
            this.props.onSelectSlide(slide, this.props.dbname)
        }
    }

    private onRemoveItem(item) {
        if (_.isFunction(this.props.onRemoveItem)) {
            this.props.onRemoveItem(item, this.props.dbname)
        }
    }

    private onUpdateItem(item) {
        if (_.isFunction(this.props.onUpdateItem)) {
            this.props.onUpdateItem(item, this.props.dbname);
        }
    }

    public render() {
        return (
            <div className='modules--presentation'>
                <PresentationToolbar
                    onChangeGrid={this.onChangeGrid.bind(this)}
                    annotation={this.state.annotation}
                    grid={this.state.grid}
                    editable={this.state.editable}
                    tool={this.state.tool}
                    dbname={this.props.dbname}
                    onChangeAnnotationStatus={this.onChangeAnnotationStatus.bind(this)}
                    onChangeEditableStatus={this.onChangeEditableStatus.bind(this)}
                    onSelectTool={this.onSelectTool.bind(this)} />
                <BasePresentation
                    drawingMode={_.get(this.state.tool, 'type') === 'path'}
                    annotation={this.state.annotation}
                    backgroundURL={this.props.backgroundURL}
                    completedSync={this.props.completedSync}
                    currentSlideId={this.props.currentSlideId}
                    originalSize={this.props.originalSize}
                    editable={this.state.editable}
                    editorSize={this.props.editorSize}
                    grid={this.state.grid}
                    onObjectCreated={this.onObjectCreated.bind(this)}
                    onRemoveItem={this.onRemoveItem.bind(this)}
                    onSelectItem={this.props.onSelectItem}
                    onSelectSlide={this.onSelectSlide.bind(this)}
                    onUpdateItem={this.onUpdateItem.bind(this)}
                    slides={this.props.slides}
                >
                    {this.props.children}
                </BasePresentation>
            </div>
        );
    }
}

function mapStateToProps (store: IStore, ownProps: IPresentationOwnProps): IPresentationStore {
    const currentSlideId = selectors.presentation.getSlideId(store);
    const items = selectors.editorItem.getEditorItemsByMeta(store, currentSlideId);
    const selectItemsId = selectors.editorItem.getEditorSelectItemsId(store);
    const completedSync = _.get(store.syncronizer, [ ownProps.dbname, 'completed' ], ownProps.completedSync);

    return {
        completedSync: completedSync,
        slides: selectors.presentation.getSlides(store),
        currentSlideId,
        children: _.map(items, (id) => (<EditorItem key={id} id={id} select={_.includes(selectItemsId, id)} />)),
        backgroundURL: _.get(selectors.presentation.getSlide(store, currentSlideId), 'background')
    };
}

function mapDispatchToProps (dispatch: Dispatch<any>): IPresentationDispatch {
    return {
        onObjectCreated: bindActionCreators(Actions.createItem, dispatch),
        onUpdateItem: bindActionCreators(Actions.updateItem, dispatch),
        onRemoveItem: bindActionCreators(Actions.removeItem, dispatch),
        onSelectItem: bindActionCreators(Actions.selectItem, dispatch),
        onSelectSlide: bindActionCreators(Actions.selectSlidePresentation, dispatch),
    };
}

export default connect<IPresentationStore, IPresentationDispatch, IPresentationOwnProps>(mapStateToProps, mapDispatchToProps)(Presentation);
