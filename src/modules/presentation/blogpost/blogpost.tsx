import * as React from 'react';
import * as _ from 'lodash';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import { IStore } from '../../../store/storeInterface';
import * as Actions from '../../../actions';
import * as selectors from '../../../selectors';

import { editorComponent, containers } from '../../../index';
import { BlogPostToolbar } from './toolbar';
import { BasePresentation } from '../basePresentation';

const { Editor } = editorComponent;
const { EditorItem } = containers;

export interface IBlogPostStore {
    completedSync?: boolean;
    currentSlideId: string;
    slides: Array<string>;
    children?: any;
    backgroundURL: string;
}

export interface IBlogPostDispatch {
    onSelectSlide: Function;
}

export interface IBlogPostOwnProps {
    completedSync?: boolean;
    dbname?: string;
    editorSize?: any;
    originalSize?: any;
}

export interface IProps extends IBlogPostStore, IBlogPostDispatch, IBlogPostOwnProps {}

export class BlogPost extends React.Component<IProps, any> {
    private onSelectSlide(slide) {
        if (_.isFunction(this.props.onSelectSlide)) {
            this.props.onSelectSlide(slide, this.props.dbname)
        }
    }

    public render() {
        return (
            <div className='modules--presentation'>
                <BasePresentation
                    editable={false}
                    editorSize={this.props.editorSize}
                    originalSize={this.props.originalSize}
                    onSelectSlide={this.props.onSelectSlide}
                    completedSync={this.props.completedSync}
                    currentSlideId={this.props.currentSlideId}
                    backgroundURL={this.props.backgroundURL}
                    slides={this.props.slides}
                >
                    {this.props.children}
                </BasePresentation>
                <BlogPostToolbar
                    onSelectSlide={this.onSelectSlide.bind(this)}
                    slides={this.props.slides}
                    dbname={this.props.dbname}
                    currentSlideId={this.props.currentSlideId} />
            </div>
        );
    }
}

function mapStateToProps (store: IStore, ownProps: IBlogPostOwnProps): IBlogPostStore {
    const currentSlideId = selectors.presentation.getSlideId(store);
    const items = selectors.editorItem.getEditorItemsByMeta(store, currentSlideId);
    const selectItemsId = selectors.editorItem.getEditorSelectItemsId(store);
    const completedSync = _.get(store.syncronizer, [ ownProps.dbname, 'completed' ], ownProps.completedSync);

    return {
        completedSync: completedSync,
        slides: selectors.presentation.getSlides(store),
        currentSlideId,
        children: _.map(items, (id) => (<EditorItem key={id} id={id} select={_.includes(selectItemsId, id)} />)),
        backgroundURL: _.get(selectors.presentation.getSlide(store, currentSlideId), 'background')
    };
}

function mapDispatchToProps (dispatch: Dispatch<any>): IBlogPostDispatch {
    return {
        onSelectSlide: bindActionCreators(Actions.selectSlidePresentation, dispatch),
    };
}

export default connect<IBlogPostStore, IBlogPostDispatch, IBlogPostOwnProps>(mapStateToProps, mapDispatchToProps)(BlogPost);
