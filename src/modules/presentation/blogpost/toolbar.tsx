import * as React from 'react';
import * as _ from 'lodash';

import { Toolbar, ToolbarGroup } from '../../../components/toolbar/toolbar';
import { SetToolButton, ToolbarNavigation, GridButton, SetToolbarMode } from '../../../components/toolbarItems';

export interface IBlogPostToolbarProps {
    onSelectSlide: Function;
    slides: Array<string>;
    currentSlideId: string;
    dbname: string;
}

export class BlogPostToolbar extends React.Component<IBlogPostToolbarProps, {}> {
    private getIndexSlide(slideId = this.props.currentSlideId) {
        return _.indexOf(this.props.slides, slideId);
    }

    private onSelectSlide (diff: number) {
        const currentSlideIndex = this.getIndexSlide();
        const index = currentSlideIndex + diff;
        const id = this.props.slides[index];

        if (_.includes(this.props.slides, id)) {
            this.props.onSelectSlide(id, this.props.dbname);
        }
    }

    public render () {
        return (
            <Toolbar>
                <ToolbarGroup position='center'>
                    <ToolbarNavigation
                        currentPosition={this.getIndexSlide() + 1}
                        maxPosition={this.props.slides.length}
                        changePosition={this.onSelectSlide.bind(this)} />
                </ToolbarGroup>
            </Toolbar>
        );
    }
}
