import * as React from 'react';
import * as _ from 'lodash';

import { editorComponent, containers, uiComponent } from '../../index';

const { Editor } = editorComponent;

export interface IPresentationProps {
    currentSlideId: string;
    completedSync: boolean;
    backgroundURL?: string;
    slides: Array<string>;

    onSelectSlide: Function;
    onUpdateItem?: Function;
    onRemoveItem?: Function;
    onSelectItem?: Function;
    onObjectCreated?: Function;

    drawingMode?: boolean;
    editable?: boolean;
    annotation?: boolean;
    grid?: IGridProps;
    editorSize?: { width: number; height: number };
    originalSize?: { width: number; height: number };
}

export interface IGridProps {
    enabled: boolean;
    size: number;
}

export class BasePresentation extends React.Component<IPresentationProps, {}> {
    public static get defaultProps(): Partial<IPresentationProps> {
        return {
            editorSize: Editor.defaultProps.editorSize,
            originalSize: Editor.defaultProps.editorSize,
            annotation: false,
            editable: false,
            grid: { enabled: false, size: 50 }
        };
    }

    public componentWillReceiveProps (nextProps: IPresentationProps) {
        if (this.props.completedSync != nextProps.completedSync && nextProps.completedSync) {
            if (!_.isEmpty(this.props.slides)) {
                this.props.onSelectSlide(this.props.slides[0]);
            }
        }
    }

    private executeAction(e: { target: any }) {
        const action = e.target.action;

        switch (action.type) {
            case 'goToSlide':
                this.props.onSelectSlide(action.data);
                break;
        }
    }

    private renderBackground () {
        switch (true) {
            case !this.props.completedSync:
                return null;

            case this.props.slides.length === 0:
                return <uiComponent.ErrorComponent desc='No slides' />;

            case !!this.props.backgroundURL:
                return (
                    <div
                        className='presentation--backgroundImage'
                        style={{ backgroundImage: `url(${this.props.backgroundURL})`}}
                    />
                );
        }
    }

    private renderEditor() {
        if (this.props.completedSync) {
            return (
                <Editor
                    drawingMode={this.props.drawingMode}
                    annotation={this.props.annotation}
                    grid={this.props.grid}
                    selectable={this.props.editable}
                    selection={!!this.props.currentSlideId && this.props.editable}
                    onObjectModified={this.props.onUpdateItem}
                    onRemoveObject={this.props.onRemoveItem}
                    onObjectSelected={this.props.onSelectItem}
                    onObjectCreated={this.props.onObjectCreated}
                    editorSize={this.props.editorSize}
                    onObjectActiveArea={this.executeAction.bind(this)}
                    originalSize={this.props.originalSize}
                >
                    {this.props.children}
                </Editor>
            );
        }

        return <uiComponent.CircularSpinner />;
    }

    public render() {
        return (
            <div className='presentation--container' style={{ ...this.props.editorSize }}>
                {this.renderBackground()}
                {this.renderEditor()}
            </div>
        );
    }
}
