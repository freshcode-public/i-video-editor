import { createRequestTypes } from '../model/helpers';

// ###### EDITOR ITEMS
export const CREATE_EDITOR_ITEM = createRequestTypes('CREATE_EDITOR_ITEM');
export const REMOVE_EDITOR_ITEM = createRequestTypes('REMOVE_EDITOR_ITEM');
export const UPDATE_EDITOR_ITEM = createRequestTypes('UPDATE_EDITOR_ITEM');
export const SELECT_EDITOR_ITEM = 'SELECT_EDITOR_ITEM';
// ###### END EDITOR ITEMS

// ###### EDITOR
export const SELECT_SLIDE_EDITOR = createRequestTypes('SELECT_SLIDE_EDITOR');
export const CREATE_SLIDE_EDITOR = createRequestTypes('CREATE_SLIDE_EDITOR');
export const REMOVE_SLIDE_EDITOR = createRequestTypes('REMOVE_SLIDE_EDITOR');
export const SET_ATTACHMENT_SLIDE = createRequestTypes('SET_ATTACHMENT_SLIDE');
export const REMOVE_ATTACHMENT_SLIDE = createRequestTypes('REMOVE_ATTACHMENT_SLIDE');
// ###### END EDITOR

// ###### IVIDEO
export const CHANGE_TRIM = createRequestTypes('CHANGE_TRIM');
// ###### END IVIDEO

// ###### SYNC
export const START_SYNC = createRequestTypes('START_SYNC');
export const STOP_SYNC = 'STOP_SYNC';
export const INITIAL_SYNC = 'INITIAL_SYNC';
export const SYNC_CHANGE = 'SYNC_CHANGE';
export const SYNC_REMOVE = 'SYNC_REMOVE';
export const SYNC_COMPLETE = 'SYNC_COMPLETE';
export const CLEAR_STORE = 'CLEAR_STORE';
// ###### END SYNC
