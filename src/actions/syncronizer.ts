import { Dispatch, ActionCreator } from 'redux';

import * as syncronizerStore from '../model/synchronizerStore';
import * as ActionTypes from '../constants/actionTypes';
import { Action } from '../model/helpers';

function startSyncSuccess (dbname: string): Action {
    return {
        type: ActionTypes.START_SYNC.SUCCESS,
        payload: {
            dbname,
            initialLoading: false,
        }
    };
}

function startSyncRequest (dbname: string, live: boolean): Action {
    return {
        type: ActionTypes.START_SYNC.REQUEST,
        payload: {
            dbname,
            live,
            initialLoading: true
        }
    };
}

export function startSyncDB (dbname: string, live: boolean, remoteHost: string): any {
    return function (dispatch: Dispatch<any>) {
        dispatch(startSyncRequest(dbname, live));

        if (syncronizerStore.isExist(dbname)) {
            return dispatch(startSyncSuccess(dbname));
        } else {
            syncronizerStore.addSynchronizer(dbname, remoteHost, dispatch);
        }

        const syncronizer = syncronizerStore.getSynchronizer(dbname);
        syncronizer.startSync(dbname, { live: !live, initialDump: null, reopen: live })
            .catch((e) => { /* ignore */ })
            .then(() => {
                dispatch(startSyncSuccess(dbname));
            });
    };
}

export function stopSyncDB (dbname): Action {
    if (syncronizerStore.isExist(dbname)) {
        syncronizerStore.getSynchronizer(dbname).stopSync();
        syncronizerStore.removeSynchronizer(dbname);
    }

    return {
        type: ActionTypes.STOP_SYNC,
        payload: {
            dbname
        }
    };
}

export function onClearStore (dbname: string) {
    return {
        type: ActionTypes.CLEAR_STORE,
        payload: {
            dbname
        }
    }
}
