import { Dispatch } from 'redux';
import * as _ from 'lodash';

import { TYPE_ITEM } from '../constants/editorTypes';
import * as syncronizerStore from '../model/synchronizerStore';
import * as ActionTypes from '../constants/actionTypes';
import { Action } from '../model/helpers';
import { IEditorItem } from '../model/editor';

export function selectSlidePresentation (id: string, dbname: string) {
    return function (dispatch: Dispatch<any>) {
        dispatch({
            type: ActionTypes.SELECT_SLIDE_EDITOR.REQUEST,
            payload: { id },
        });

        if (!syncronizerStore.isExist(dbname)) {
            dispatch({
                type: ActionTypes.SELECT_SLIDE_EDITOR.SUCCESS,
                payload: { id },
            });
            return;
        }

        syncronizerStore.getSynchronizer(dbname).localDB.getAttachment(id, 'background')
            .then((background) => {
                dispatch({
                    type: ActionTypes.SELECT_SLIDE_EDITOR.SUCCESS,
                    payload: {
                        id,
                        background: URL.createObjectURL(background)
                    },

                });
            })
            .catch((e) => {
                dispatch({
                    type: ActionTypes.SELECT_SLIDE_EDITOR.SUCCESS,
                    payload: { id }
                });
            })
    }
}

function createSlidePresentationSuccess (slide: IEditorItem): Action {
    return {
        type: ActionTypes.CREATE_SLIDE_EDITOR.SUCCESS,
        payload: {
            slide: slide,
            id: slide._id
        }
    };
}

function createSlidePresentationRequest (slide: IEditorItem): Action {
    return {
        type: ActionTypes.CREATE_SLIDE_EDITOR.REQUEST,
        payload: {
            slide: slide,
            id: slide._id
        }
    };
}

export function createSlidePresentation (slide: IEditorItem, dbname: string) {
    const id = (_.isNil(slide._id) ? Date.now() : slide._id).toString();
    slide._id = id;

    return function (dispatch: Dispatch<any>) {
        dispatch(createSlidePresentationRequest(slide));

        if (!syncronizerStore.isExist(dbname)) {
            return dispatch(createSlidePresentationSuccess(slide));
        }

        slide = _.assign({}, slide, {
            doc_type: TYPE_ITEM.SLIDE
        });

        syncronizerStore.getSynchronizer(dbname).localDB.put(slide)
            .then((res) => {
                slide._rev = res.rev;
                return dispatch(createSlidePresentationSuccess(slide));
            });
    }
}

function removeSlidePresentationSuccess (id: string): Action {
    return {
        type: ActionTypes.REMOVE_SLIDE_EDITOR.SUCCESS,
        payload: id,
        skipHistory: false,
    };
}

function removeSlidePresentationRequest (id: string): Action {
    return {
        type: ActionTypes.REMOVE_SLIDE_EDITOR.REQUEST,
        payload: id,
        skipHistory: false,
    };
}

export function removeSlidePresentation (slide: IEditorItem, dbname: string) {
    return function (dispatch: Dispatch<any>) {
        dispatch(removeSlidePresentationRequest(slide._id));

        if (!syncronizerStore.isExist(dbname)) {
            return dispatch(removeSlidePresentationSuccess(slide._id));
        }

        syncronizerStore.getSynchronizer(dbname).localDB.remove(slide as any)
            .then(() => {
                dispatch(removeSlidePresentationSuccess(slide._id));
            });
    }
}

export function setAttachmentPresentation(slide: IEditorItem, name: string, dbname: string, attachment: FileList) {
    return function (dispatch: Dispatch<any>) {
        dispatch({
            type: ActionTypes.SET_ATTACHMENT_SLIDE.REQUEST,
            payload: slide
        });

        if (!syncronizerStore.isExist(dbname)) {
            return;
        }

        syncronizerStore.getSynchronizer(dbname).localDB.putAttachment(slide._id, name, slide._rev, attachment[0], attachment[0].type)
            .then((t) => {
                dispatch({
                    type: ActionTypes.SET_ATTACHMENT_SLIDE.SUCCESS,
                    payload: {
                        slide,
                        id: slide._id,
                        attachment: attachment[0],
                        name
                    },
                });
            })
            .catch((e) => {
                dispatch({
                    type: ActionTypes.SET_ATTACHMENT_SLIDE.FAILURE,
                    payload: {
                        slide,
                        id: slide._id,
                        name
                    }
                });
            });
    }
}

export function removeAttachmentPresentation(slide: IEditorItem, name: string, dbname: string) {
    return function (dispatch: Dispatch<any>) {
        dispatch({
            type: ActionTypes.REMOVE_ATTACHMENT_SLIDE.REQUEST,
            payload: slide
        });

        if (!syncronizerStore.isExist(dbname)) {
            return;
        }

        syncronizerStore.getSynchronizer(dbname).localDB.removeAttachment(slide._id, name, slide._rev)
            .then((t) => {
                dispatch({
                    type: ActionTypes.REMOVE_ATTACHMENT_SLIDE.SUCCESS,
                    payload: {
                        slide,
                        id: slide._id,
                        name: name
                    }
                });
            })
            .catch((e) => {
                dispatch({
                    type: ActionTypes.REMOVE_ATTACHMENT_SLIDE.FAILURE,
                    payload: {
                        slide,
                        id: slide._id,
                        name
                    }
                });
            });
    }
}
