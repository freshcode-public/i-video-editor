import { Dispatch } from 'redux';
import * as _ from 'lodash';

import * as syncronizerStore from '../model/synchronizerStore';
import { TYPE_ITEM } from '../constants/editorTypes';
import * as ActionTypes from '../constants/actionTypes';
import { Action } from '../model/helpers';
import { BaseItem } from '../model/abstractItems';

function changeTrimRequest(trim: any): Action {
    return {
        type: ActionTypes.CHANGE_TRIM.REQUEST,
        payload: trim
    };
}

function changeTrimSuccess (trim: any): Action {
    return {
        type: ActionTypes.CHANGE_TRIM.SUCCESS,
        payload: trim
    };
}

export function changeTrim(trim: any, dbname: string) {
    return function (dispatch: Dispatch<any>) {
        dispatch(changeTrimRequest(trim));

        if (!syncronizerStore.isExist(dbname)) {
            return dispatch(changeTrimSuccess(trim));
        }

        syncronizerStore.getSynchronizer(dbname).localDB
            .query((doc: BaseItem, emit: any) => {
                if (doc.doc_type === TYPE_ITEM.TRIM)
                    emit(doc.doc_type);
            }, { include_docs : true })
            .then((docs) => {
                let doc = _.get<BaseItem>(docs.rows, [ 0, 'doc' ], { doc_type: TYPE_ITEM.TRIM, _id: Date.now().toString() } as any);
                doc = _.assign({}, doc, { ...trim });

                return syncronizerStore.getSynchronizer(dbname).localDB.put(doc);
            })
            .then((res) => dispatch(changeTrimSuccess(trim)))
            .catch(console.log.bind(console, 'changeTrim error:'));
    }
}
