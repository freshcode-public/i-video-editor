import { Dispatch } from 'redux';
import * as _ from 'lodash';

import * as ActionTypes from '../constants/actionTypes';
import { Action } from '../model/helpers';
import * as syncronizerStore from '../model/synchronizerStore';
import { BaseItem } from '../model/abstractItems';

function createItemRequest (item: BaseItem, id: string): Action {
    return {
        type: ActionTypes.CREATE_EDITOR_ITEM.REQUEST,
        payload: null
    };
}

function createItemSuccess (item: BaseItem, id: string): Action {
    return {
        type: ActionTypes.CREATE_EDITOR_ITEM.SUCCESS,
        payload: { item: item, id: id }
    };
}

export function createItem (item: BaseItem, dbname: string) {
    const id = (_.isNil(item._id) ? Date.now() : item._id).toString();
    item._id = id;

    return function (dispatch: Dispatch<any>) {
        dispatch(createItemRequest(item, id));

        if (!syncronizerStore.isExist(dbname)) {
            return dispatch(createItemSuccess(item, id));
        }

        syncronizerStore.getSynchronizer(dbname).localDB.put(item)
            .then((res) => {
                dispatch(createItemSuccess(item, id));
            })
            .catch(console.log.bind(console, 'createItem error:'));
    }
}

function removeItemRequest(ids: Array<string>): Action {
    return {
        type: ActionTypes.REMOVE_EDITOR_ITEM.REQUEST,
        payload: ids
    };
}

function removeItemSuccess (ids: Array<string>): Action {
    return {
        type: ActionTypes.REMOVE_EDITOR_ITEM.SUCCESS,
        payload: ids
    };
}

export function removeItem (item: Array<Object> | Object, dbname: string) {
    const items = _.isArray(item) ? item : [ item ];
    const ids: Array<string> = _.map(items, '_id');

    return function (dispatch: Dispatch<any>) {
        dispatch(removeItemRequest(ids));

        if (!syncronizerStore.isExist(dbname)) {
            return dispatch(removeItemSuccess(ids));
        }
        const _items = _.map(items, (i: any) => { i._deleted = true; return i });

        syncronizerStore.getSynchronizer(dbname).localDB.bulkDocs(_items)
            .then((res) => {
                dispatch(removeItemSuccess(ids));
            })
            .catch(console.log.bind(console, 'removeItem error:'));
    }
}

function updateItemRequest (items: Array<BaseItem>): Action {
    return {
        type: ActionTypes.UPDATE_EDITOR_ITEM.REQUEST,
        payload: items,
        pending: true
    };
}

function updateItemSuccess (items: Array<BaseItem>): Action {
    return {
        type: ActionTypes.UPDATE_EDITOR_ITEM.SUCCESS,
        payload: items,
        pending: false
    };
}

export function updateItem (item: Array<BaseItem> | BaseItem, dbname: string) {
    const items = _.cloneDeep(_.isArray(item) ? item : [ item ]);
    const updateAt = Date.now();

    items.forEach((item) => _.set(item, 'info.updateAt', updateAt));

    return function (dispatch: Dispatch<any>) {
        dispatch(updateItemRequest(items));

        if (!syncronizerStore.isExist(dbname)) {
            return dispatch(updateItemSuccess(items));
        }

        syncronizerStore.getSynchronizer(dbname).localDB.bulkDocs(items)
            .then((result: any) => {
                const errorDocs = result.filter((res) => res.error);

                dispatch(updateItemSuccess(_.filter(items, (i: any) => !_.includes(errorDocs, i._id))));
            })
            .catch(console.log.bind(console, 'updateItem error:'));
    }
}

export function selectItem (id: string): Action {
    return {
        type: ActionTypes.SELECT_EDITOR_ITEM,
        skipHistory: true,
        payload: id
    };
}
