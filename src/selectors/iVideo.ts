import * as _ from 'lodash';

import { IStore } from '../store/storeInterface';
import { BaseItem } from '../model/abstractItems';
import { Map } from '../model/helpers';
import { IIVideoTrimRange } from '../reducers/iVideo';

export function getTrim (store: IStore) {
    return store.iVideo.trim;
}
