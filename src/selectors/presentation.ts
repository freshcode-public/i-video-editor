import { IStore } from '../store/storeInterface';
import { IEditorItem } from '../model/editor';

export function getSlides (store: IStore): string[] {
    return store.presentation.slides;
}

export function getSlide (store: IStore, id: string): IEditorItem {
    return store.presentation.slidesById[id];
}

export function getSlideId (store: IStore): string {
    return store.presentation.selectId;
}
