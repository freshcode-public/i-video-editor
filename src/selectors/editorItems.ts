import * as _ from 'lodash';

import { IStore } from '../store/storeInterface';
import { BaseItem } from '../model/abstractItems';
import { Map } from '../model/helpers';

export function getEditorSelectItemsId (store: IStore): Array<string> {
    return store.editorItems.selectsId;
}

export function getEditorItemsId (store: IStore): Array<string> {
    return store.editorItems.items;
}

export function getEditorItems (store: IStore): Map<BaseItem> {
    return store.editorItems.itemsById;
}

export function getEditorItem (store: IStore, id: string): BaseItem {
    return store.editorItems.itemsById[id];
}

export function getEditorItemsByMeta (store: IStore, selector: string | Function): Array<string> {
    let obj;

    return _.filter(store.editorItems.items, (id: string) => {
        obj = getEditorItem(store, id);

        if (_.isFunction(selector)) {
            return selector(obj)
        }

        return _.get(obj, [ 'meta' ], '') === selector;
    });
}
