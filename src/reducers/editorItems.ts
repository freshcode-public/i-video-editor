import * as _ from 'lodash';
import { Action, Map } from '../model/helpers';
import * as ActionTypes from '../constants/actionTypes';
import { TYPE_ITEM } from '../constants/editorTypes';
import { Item } from '../model/abstractItems';

export interface IEditorItemsStore {
    itemsById: Map<Item>;
    selectsId: Array<string>;
    items: Array<string>;
}

export const INITIAL_EDITOR_ITEMS_STORE: IEditorItemsStore = {
    items: [],
    itemsById: {},
    selectsId: []
};

function createEditorItemSuccess (state: IEditorItemsStore, item: Item, itemId: string, select: boolean = true): IEditorItemsStore {
    // Duplicate key or invalid base props
    if (_.includes(state.items, item._id) || _.isNil(item.type) || _.isNil(item._id)) return state;

    item = _.assign({}, item, { _id: itemId });

    return {
        items: state.items.concat(itemId),
        selectsId: select ? [ item._id ] : state.selectsId,
        itemsById: _.assign({}, state.itemsById, { [item._id]: item })
    };
}

function selectEditorItem (state: IEditorItemsStore, ids: Array<string>): IEditorItemsStore {
    return {
        items: state.items,
        selectsId: ids,
        itemsById: state.itemsById
    };
}

function removeEditorItem (state: IEditorItemsStore, ids: Array<string>): IEditorItemsStore {
    return {
        items: _.without(state.items, ...ids),
        selectsId: [],
        itemsById: _.omit(state.itemsById, ids)
    };
}

function updateEditorItem (state: IEditorItemsStore, items: Array<Item>): IEditorItemsStore {
    const itemsById = _.assign({}, state.itemsById);

    items
        .filter((item) => _.has(item, '_id') && _.includes(state.items, item._id))
        .forEach((item) => {
            itemsById[item._id] = item;
        });

    return {
        items: state.items,
        selectsId: state.selectsId,
        itemsById: itemsById
    };
}

function createBatchEditorItem (state: IEditorItemsStore, items: Array<Item>): IEditorItemsStore {
    items = items
        .filter((item) => !_.isNil(item._id) && !_.includes(state.items, item._id));

    return {
        items: state.items.concat(_.map(items, '_id')),
        itemsById: _.reduce(items, (map, obj: Item) => { map[obj._id] = obj; return map }, state.itemsById),
        selectsId: []
    };
}

export function editorItemsReducer (state: IEditorItemsStore = INITIAL_EDITOR_ITEMS_STORE, action: Action): IEditorItemsStore {
    const { payload } = action;

    switch (action.type) {
        case ActionTypes.SELECT_EDITOR_ITEM:
            return selectEditorItem(state, payload);

        case ActionTypes.UPDATE_EDITOR_ITEM.SUCCESS:
            return updateEditorItem(state, payload);

        case ActionTypes.REMOVE_EDITOR_ITEM.SUCCESS:
            return removeEditorItem(state, payload);

        case ActionTypes.CREATE_EDITOR_ITEM.SUCCESS:
            return createEditorItemSuccess(state, payload.item, payload.id);

        case ActionTypes.INITIAL_SYNC:
            const items = _.filter(payload.items, (item: Item) => item.doc_type === TYPE_ITEM.GEOMERTIC );
            return createBatchEditorItem(state, items);

        case ActionTypes.SYNC_CHANGE:
            if ((payload.doc as Item).doc_type !== TYPE_ITEM.GEOMERTIC)
                return state;

            const isOld = _.includes(state.items, payload.id);
            if (isOld) {
                return updateEditorItem(state, [ payload.doc ]);
            }
            return createEditorItemSuccess(state, payload.doc, payload.id, false);

        case ActionTypes.SYNC_REMOVE:
            return removeEditorItem(state, payload.id);

        case ActionTypes.CLEAR_STORE:
            // TODO
            return {
                items: [],
                itemsById: {},
                selectsId: []
            };

        default:
            return state;
    }
}
