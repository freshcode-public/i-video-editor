import * as _ from 'lodash';
import { Action } from '../model/helpers';
import * as ActionTypes from '../constants/actionTypes';
import { TYPE_ITEM } from '../constants/editorTypes';
import { BaseItem } from '../model/abstractItems';

export interface IIVideoTrimRange {
    start: number;
    end: number;
}

export interface IIVideoStore {
    trim: IIVideoTrimRange;
}

export const INITIAL_IVIDEO_STORE: IIVideoStore = {
    trim: { start: 0, end: undefined }
};

function initialSync(state: IIVideoStore, items: Array<any>): IIVideoStore {
    const trim = _.find<BaseItem & IIVideoTrimRange>(items, { doc_type: TYPE_ITEM.TRIM });

    if (trim)
        return {
            trim: { start: trim.start, end: trim.end }
        };

    return state;
}

function syncChange(state: IIVideoStore, item: BaseItem): IIVideoStore {
    switch (item.doc_type) {
        case TYPE_ITEM.TRIM:
            let trim: BaseItem & IIVideoTrimRange = item as any;
            return {
                trim: { start: trim.start, end: trim.end }
            };

        default:
            return state;
    }
}

function changeTrim(state: IIVideoStore, trim: IIVideoTrimRange ): IIVideoStore {
    return {
        trim: trim
    };
}

export function iVideoReducer (state: IIVideoStore = INITIAL_IVIDEO_STORE, action: Action): IIVideoStore {
    const { payload } = action;

    switch (action.type) {
        case ActionTypes.INITIAL_SYNC:
            return initialSync(state, payload.items);

        case ActionTypes.SYNC_CHANGE:
            return syncChange(state, payload.doc);

        case ActionTypes.CHANGE_TRIM.REQUEST:
        case ActionTypes.CHANGE_TRIM.SUCCESS:
            return changeTrim(state, action.payload);

        default:
            return state;
    }
}
