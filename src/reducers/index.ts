import { combineReducers, Reducer } from 'redux';
import { editorItemsReducer, IEditorItemsStore, INITIAL_EDITOR_ITEMS_STORE } from './editorItems';
import { presentationReducer, IPresentationStore, INITIAL_PRESENTATIIN_STORE  } from './presentation';
import { syncronizerReducer, ISyncronizerStore, INITIAL_SYNCRONIZER_STORE } from './syncronizer';
import { iVideoReducer, IIVideoStore, INITIAL_IVIDEO_STORE } from './iVideo';
import { IStore } from '../store/storeInterface';

export const reducersMap = combineReducers<IStore>({
    editorItems: editorItemsReducer,
    presentation: presentationReducer,
    syncronizer: syncronizerReducer,
    iVideo: iVideoReducer
});

export {
    presentationReducer,
    IPresentationStore,
    INITIAL_PRESENTATIIN_STORE,

    editorItemsReducer,
    IEditorItemsStore,
    INITIAL_EDITOR_ITEMS_STORE,

    syncronizerReducer,
    ISyncronizerStore,
    INITIAL_SYNCRONIZER_STORE,

    iVideoReducer,
    IIVideoStore,
    INITIAL_IVIDEO_STORE
};
