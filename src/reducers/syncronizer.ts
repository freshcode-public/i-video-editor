import * as _ from 'lodash';
import { Action, Map } from '../model/helpers';
import * as ActionTypes from '../constants/actionTypes';

export interface IDB {
    initialLoading: boolean;
    completed: false;
    dbname: string;
}

export type ISyncronizerStore = Map<IDB>;

export const INITIAL_SYNCRONIZER_STORE: ISyncronizerStore = {}

function startSyncRequest (state: ISyncronizerStore, payload: IDB): ISyncronizerStore {
    return _.assign({}, state, { [payload.dbname]: payload });
}

function startSyncSuccess (state: ISyncronizerStore, payload: IDB): ISyncronizerStore {
    const db: any = _.cloneDeep(state[payload.dbname]);
    db.initialLoading = false;
    db.completed = false;

    return _.assign({}, state, { [payload.dbname]: db });
}

function syncComplete (state: ISyncronizerStore, payload: IDB): ISyncronizerStore {
    const db: any = _.cloneDeep(state[payload.dbname]);
    db.completed = true;

    return _.assign({}, state, { [payload.dbname]: db });
}

export function syncronizerReducer(state: ISyncronizerStore = INITIAL_SYNCRONIZER_STORE, action: Action): ISyncronizerStore {
    switch (action.type) {
        case ActionTypes.START_SYNC.REQUEST:
            return startSyncRequest(state, action.payload);

        case ActionTypes.START_SYNC.SUCCESS:
            return startSyncSuccess(state, action.payload);

        case ActionTypes.SYNC_COMPLETE:
            return syncComplete(state, action.payload);

        case ActionTypes.STOP_SYNC:
            return _.omit(state, [ action.payload.dbname ])

        default:
            return state;
    }
}
