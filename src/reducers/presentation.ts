import * as _ from 'lodash';
import { Action } from '../model/helpers';
import * as ActionTypes from '../constants/actionTypes';
import { TYPE_ITEM } from '../constants/editorTypes';
import { IEditorItem } from '../model/editor';

export interface IPresentationStore {
    slides: Array<string>;
    slidesById: Map<string, IEditorItem>;
    selectId: string;
}

export const INITIAL_PRESENTATIIN_STORE: IPresentationStore = {
    slides: [],
    slidesById: {},
    selectId: undefined
};

function selectSlidePresentation(state: IPresentationStore, id: string, background: string): IPresentationStore {
    _.set(state.slidesById[id], 'background', background);

    return {
        selectId: id,
        slides: state.slides,
        slidesById: state.slidesById
    };
}

function createSlidePresentation(state: IPresentationStore, slide: Object, id: string): IPresentationStore {
    let selectId = state.selectId;
    slide = _.assign({}, slide, {
        _id: id,
        name: id
    });

    if (_.isEmpty(state.slides)) {
        selectId = id;
    }

    return {
        slides: state.slides.concat(id),
        selectId: selectId,
        slidesById: _.assign({}, state.slidesById, { [ id ]: slide })
    };
}

function createBatchSlidePresentation (state: IPresentationStore, slides: Array<Object>): IPresentationStore {
    slides = slides
        .filter((slide: any) => !_.isNil(slide._id) && !_.includes(state.slides, slide._id));
    const _slides = state.slides.concat(_.map(slides, '_id'));

    return {
        slides: _slides,
        slidesById: _.reduce(slides, (map, obj: any) => { map[obj._id] = obj; return map }, state.slidesById),
        selectId: state.selectId
    };
}

function removeSlidePresentation(state: IPresentationStore, id: string): IPresentationStore {
    let selectId = state.selectId;

    if (state.selectId == id) {
        const index = _.indexOf(state.slides, id);

        switch (true) {
            case index == 0:
                selectId = state.slides[1];
            break;
            case index == state.slides.length - 1:
                selectId = state.slides[state.slides.length - 2];
            break;
            default:
                selectId = state.slides[index + 1];
        }
    }

    return {
        slides: _.without(state.slides, id),
        selectId: selectId,
        slidesById: _.omit(state.slidesById, [ id ])
    };
}

function setAttachmentPresentation (state: IPresentationStore, id: string, attachmentId: string, attachment: any): IPresentationStore {
    return {
        selectId: state.selectId,
        slides: state.slides,
        slidesById: _.assign({}, state.slidesById, { [id]: _.assign({}, state.slidesById[id], { [attachmentId]: attachment })})
    };
}

function syncChange (state: IPresentationStore, doc: any, id: string): IPresentationStore {
    if (doc.doc_type !== TYPE_ITEM.SLIDE)
        return state;

    const isOld = _.includes(state.slides, id);
    if (isOld) {
        return {
            selectId: state.selectId,
            slides: state.slides,
            slidesById: _.assign({}, state.slidesById, { [ id ]: _.assign({}, state.slidesById[id], doc) })
        };
    }
    return createSlidePresentation(state, doc, id);
}

export function presentationReducer (state: IPresentationStore = INITIAL_PRESENTATIIN_STORE, action: Action): IPresentationStore {
    const { payload } = action;

    switch (action.type) {
        case ActionTypes.SELECT_SLIDE_EDITOR.SUCCESS:
            return selectSlidePresentation(state, payload.id, payload.background);

        case ActionTypes.CREATE_SLIDE_EDITOR.SUCCESS:
            return createSlidePresentation(state, payload.slide, payload.id);

        case ActionTypes.REMOVE_SLIDE_EDITOR.SUCCESS:
            return removeSlidePresentation(state, payload);

        case ActionTypes.INITIAL_SYNC:
            const slides = _.filter(payload.items, (item: any) => item.doc_type === TYPE_ITEM.SLIDE );
            return createBatchSlidePresentation(state, slides);

        case ActionTypes.SET_ATTACHMENT_SLIDE.SUCCESS:
            return setAttachmentPresentation(state, payload.id, action.payload.name, URL.createObjectURL(payload.attachment));

        case ActionTypes.REMOVE_ATTACHMENT_SLIDE.SUCCESS:
            return setAttachmentPresentation(state, payload.id, action.payload.name, null);

        case ActionTypes.SYNC_CHANGE:
            return syncChange(state, payload.doc, payload.id);

        case ActionTypes.SYNC_REMOVE:
            return removeSlidePresentation(state, payload.id);

        case ActionTypes.CLEAR_STORE:
            // TODO
            return INITIAL_PRESENTATIIN_STORE;

        default:
            return state;
    }
}
