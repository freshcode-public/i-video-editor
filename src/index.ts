import './model/canvasElements';

import * as canvas from './components/canvas';
import * as canvasItems from './components/canvasItems';
import * as editor from './components/editor';
import * as ui from './components/ui';
export { Palyer as PlayerComponent } from './components/player';

import * as editorType from './constants/editorTypes';
import * as actionType from './constants/actionTypes';
import * as action from './actions';
import * as reducers from './reducers';
import * as _containers from './containers'
import * as _modals from './components/modals';

// https://github.com/Microsoft/TypeScript/issues/4529#issuecomment-281028204
// fake namespace

export const baseCanvasComponents = canvas;
export const baseCanvasItems = canvasItems;
export const editorComponent = editor;
export const utils = {
    action,
    reducers,
    constants: {
        editorType,
        actionType
    }
};
export const modals = _modals;
export const uiComponent = ui;
export const containers = _containers;

export { configureStore } from './store/store';

// modules
import { default as PresentationEdtior } from './modules/presentation/editor/presentation';
import { default as PresentationBlogPost } from './modules/presentation/blogpost/blogpost';
import { IVideoConnect } from './modules/iVideo/editor/presentationConnect';
export const appModules = {
    presentation: {
        Editor: (PresentationEdtior as any),
        Blogpost: (PresentationBlogPost as any)
    },
    iVideo: {
        Editor: (IVideoConnect as any)
    }
}
