import * as React from 'react';
import * as _ from 'lodash';

export interface Coords {
    width: number;
    height: number;
}

export interface IProps extends React.Props<any> {
    screenSize: Coords;
    className?: string;
    cancelFullScreen?: Function;
    fullscreen?: boolean;
    onResize?: Function;
    closeFullscreen?: boolean;
}

export interface IState {
    fullscreen?: boolean;
}

export class SensitiveScreen extends React.Component<IProps, IState> {
    private eventsResize = [ 'resize' ];
    private funcRequestFullscreen = [ 'requestFullscreen', 'msRequestFullscreen', 'mozRequestFullScreen', 'webkitRequestFullscreen' ];
    private eventsFullscreenchange = [ 'fullscreenchange', 'mozfullscreenchange', 'webkitfullscreenchange' ];
    private objFullScreenElement = [ 'fullscreenElement', 'mozFullScreenElement', 'webkitFullscreenElement'];
    private funcCancelFullScreen = [ 'cancelFullScreen', 'mozCancelFullScreen', 'webkitCancelFullScreen' ];

    public state: IState = {
        fullscreen: false
    };

    public refs: {
        [name: string]: any;
        sensitiveScreen: HTMLDivElement;
    };

    private onChangeFullScreen() {
        let status = false;

        _.forEach(this.objFullScreenElement, (element) => {
            if (status) return;

            status = !!window.document[element];
        });

        if (!status) {
            this.cancelFullscreen()
        }

        this.setState({ fullscreen: status });
    }

    constructor() {
        super();

        this.onChangeFullScreen = this.onChangeFullScreen.bind(this);
        this.cancelFullscreen = this.cancelFullscreen.bind(this);
    }

    private configuration(eventListener: string) {
        if (this.props.fullscreen)
            _.map(this.eventsFullscreenchange, (eventName) => document[eventListener](eventName, this.onChangeFullScreen, false));

        if (_.isFunction(this.props.onResize))
            _.map(this.eventsResize, (eventName) => document[eventListener](eventName, this.props.onResize, false));
    }

    public componentDidMount() {
        this.configuration('addEventListener');
    }

    public componentWillUnmount() {
        this.configuration('removeEventListener');
    }

    private getProportionalSize() {
        const proportion = 645 / 1140;
        let proportionalSize = { width: window.innerWidth, height: 0 }
        proportionalSize.height = proportionalSize.width * proportion;
        if (proportionalSize.height > window.innerHeight) {
            proportionalSize.height = window.innerHeight;
            proportionalSize.width = proportionalSize.height / proportion;
        }
        return proportionalSize;
    }

    private get styleComponent(): React.CSSProperties {
        if (this.state.fullscreen) {
            let zoom = window.innerWidth / this.props.screenSize.width;
            let margin = (window.innerHeight - this.props.screenSize.height * zoom) / 2;
            var size = this.getProportionalSize();
            return {
                zoom: 1,
                width: size.width,
                height: size.height,
                backgroundPosition: 'center',
                marginTop: margin,
                marginBottom: margin
            };
        }

        return {
            width: this.props.screenSize.width,
            height: this.props.screenSize.height
        };
    }

    public requestFullscreen() {
        const sensitiveScreen = this.refs.sensitiveScreen;

        _.forEach(this.funcRequestFullscreen, (requestFullscreen) => {
            if (sensitiveScreen[requestFullscreen])
                sensitiveScreen[requestFullscreen]();
        });
    }

    public cancelFullscreen() {
        let _cancelFullScreen;

        _.forEach(this.funcCancelFullScreen, (cancelFullScreen) => {
            // _cancelFullScreen = _.get(parent, [ 'document', cancelFullScreen ], window.document[cancelFullScreen]);
            _cancelFullScreen = document.documentElement[cancelFullScreen];

            if (_cancelFullScreen)
                _cancelFullScreen();
        });
        this.props.cancelFullScreen();
    }

    private renderCancelFullscreen(): JSX.Element {
        if (!this.props.fullscreen || !this.state.fullscreen ||!this.props.closeFullscreen) {
            // this.props.cancelFullScreen();
            return null;
        }

        return (
            <div className='only-fullscreen'>
                <div className='close only-fullscreen' onClick={this.cancelFullscreen}>X</div>
            </div>
        );
    }

    public render() {
        return (
            <div ref='sensitiveScreen' className={this.props.className} style={this.styleComponent}>
                {this.renderCancelFullscreen()}
                {this.props.children}
            </div>
        );
    }
}
