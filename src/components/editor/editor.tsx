import * as React from 'react';
import * as f from 'fabric';
import * as _ from 'lodash';
import * as keymaster from 'keymaster';

import { getCenterGroup, getActiveObjects, ICanvasSize, ICanvasConfig } from '../canvas/utils';
import { Canvas, RelativeCanvas } from '../canvas';
import { Grid } from './grid';
import { clearItemProps } from '../../model/geometry';
import { snapMoveToGrid, snapScaleToGrid } from '../../model/canvasUtils/grid';

export interface IEditorActionProps {
    onRemoveObject?: Function;
    onObjectModified?: Function;
    onObjectSelected?: Function;
    onObjectCreated?: Function;
    onObjectActiveArea?: Function;
}

export interface IEditorProps extends React.Props<Editor>, IEditorActionProps, ICanvasConfig {
    grid?: { enabled: boolean; size: number; };
    annotation?: boolean;
    selectItemsId?: Array<string>;
    originalSize?: ICanvasSize;
    drawingMode?: boolean;
}

interface IBounds {
    x1?: number, y1?: number, x2?: number, y2?: number
};

export class Editor extends React.PureComponent<IEditorProps, any> {
    public static get defaultProps(): IEditorProps {
        return {
            drawingMode: false,
            annotation: false,
            selection: true,
            selectable: true,
            editorSize: Canvas.defaultProps.editorSize,
            originalSize: Canvas.defaultProps.editorSize,
            grid: { enabled: false, size: 50 }
        };
    }

    public constructor () {
        super();

        this.onRemoveObject = this.onRemoveObject.bind(this);
        this.onObjectSelected = this.onObjectSelected.bind(this);
        this.onObjectDeselected = this.onObjectDeselected.bind(this);
        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
        this.onObjectModified = this.onObjectModified.bind(this);
        this.removeActiveObject = this.removeActiveObject.bind(this);
        this.onObjectMoving = this.onObjectMoving.bind(this);
        this.onObjectScaling = this.onObjectScaling.bind(this);
        this.onPathCreated = this.onPathCreated.bind(this);
        this.onObjectActiveArea = this.onObjectActiveArea.bind(this);
    }

    public refs: {
        [anyKey: string]: any;
        canvas: Canvas;
    }

    public componentDidMount () {
        keymaster('delete, backspace', this.removeActiveObject);
    }

    public componentWillUnmount () {
        keymaster.unbind('delete');
        keymaster.unbind('backspace');
    }

    private removeActiveObject () {
        const group: any = { _objects: getActiveObjects(this.getCanvas()) };
        this.onRemoveObject({ target: group, e: null });
    }

    private bounds: IBounds = {};

    private getCanvas (): f.Canvas {
        const canvasComponent = this.refs.canvas;
        return canvasComponent && canvasComponent.canvas;
    }

    private onRemoveObject(options: f.IEvent) {
        if (!_.isFunction(this.props.onRemoveObject)) return;

        const ids = _.get(options.target, '_objects', [ options.target ])
            .filter((target) => target && !target.get('_unmount') && _.has(target, '_id'))
            .map((item) => clearItemProps(item));

        if (_.isEmpty(ids)) return;

        this.props.onRemoveObject(ids);
    }

    private onObjectModified(e: f.IEvent) {
        if (!_.isFunction(this.props.onObjectModified)) return;

        const updateAt = Date.now();
        const isGroup = _.has(e, 'target._objects');
        let items = _.get(e.target, '_objects', [ e.target ])
            .map((item) => clearItemProps(_.cloneDeep(item)))
            .filter((target) => !_.isNil(target));

        if (isGroup) {
            items = items.map((target) => {
                const centerGroup = getCenterGroup(e.target);
                return _.assign({}, target, {
                    top: target.top + centerGroup.top,
                    left: target.left + centerGroup.left,
                    scaleX: target.scaleX + centerGroup.scaleX,
                    scaleY: target.scaleY + centerGroup.scaleY,
                });
            });
        }

        this.props.onObjectModified(items);
    }

    private onObjectMoving (options: f.IEvent) {
        if (this.props.grid.enabled)
            snapMoveToGrid(this.props.grid.size, options.target);
    }

    private onObjectScaling (options: f.IEvent) {
        if (this.props.grid.enabled)
            snapScaleToGrid(this.props.grid.size, options.target);
    }

    private lockSelectGroup(target: f.Object) {
        if (!_.has(target, '_objects')) return;

        target.lockRotation = true;
        target.lockScalingFlip = true;
        target.lockScalingX = target.lockScalingY = true;
    }

    private onObjectSelected(e: f.IEvent) {
        this.lockSelectGroup(e.target);

        if (!_.isFunction(this.props.onObjectSelected)) return;

        const ids: Array<string> = _.get(e.target, '_objects', [ e.target ])
            .map((target) => target.get('_id'));

        // if one
        if (!_.includes(this.props.selectItemsId, ids[0])) {
            this.props.onObjectSelected(ids);
        }
    }

    protected onMouseDown(options: f.IEvent) {
        const canvas = this.getCanvas();
        // if not selected the item
        if (!options.target && canvas) {
            const pt = canvas.getPointer(options.e);
            this.bounds.x1 = pt.x;
            this.bounds.y1 = pt.y;
        }
    }

    protected onMouseUp(options: f.IEvent) {
        const canvas = this.getCanvas();
        if (!_.isFunction(this.props.onObjectCreated) || !canvas || this.props.drawingMode) return;

        const pt = canvas.getPointer(options.e);
        this.bounds.x2 = pt.x;
        this.bounds.y2 = pt.y;

        const x = Math.min(this.bounds.x1, this.bounds.x2),
            y = Math.min(this.bounds.y1, this.bounds.y2),
            w = Math.abs(this.bounds.x1 - this.bounds.x2),
            h = Math.abs(this.bounds.y1 - this.bounds.y2);

        if (w > 0 && h > 0 && this.props.selectable) {
            this.props.onObjectCreated({ left: x, width: w, top: y, height: h });
        }

        this.bounds = {};
    }

    private onObjectDeselected(e) {
        if (!_.isFunction(this.props.onObjectSelected)) return;

        this.props.onObjectSelected([]);
    }

    private onPathCreated(e: { path: f.IPath }) {
        const canvas = this.getCanvas();
        if (!_.isFunction(this.props.onObjectCreated) || !canvas) return;

        if (e.path.originX === 'center')
            e.path.left -= e.path.width / 2;

        if (e.path.originX === 'center')
            e.path.top -= e.path.height / 2;

        const item = clearItemProps(e.path.toObject());

        if (this.props.selectable) {
            this.props.onObjectCreated(item);
        }

        e.path.remove();
    }

    private onObjectActiveArea(e) {
        if (_.isFunction(this.props.onObjectActiveArea)) {
            this.props.onObjectActiveArea(e)
        }
    }

    private renderChildren(): Array<JSX.Element> {
        return React.Children.map(this.props.children, (child) => {
            if (!child) return;

            return React.cloneElement(child as any);
        });
    }

    public render () {
        // TODO: add separator between RelativeCanvas and Canvas
        return (
            <div>
                {this.props.grid.enabled && <Grid grid={this.props.grid.size} {...this.props.editorSize} />}
                <RelativeCanvas
                    originalSize={this.props.originalSize}
                    editorSize={this.props.editorSize}
                    drawingMode={this.props.drawingMode}
                    ref='canvas'
                    annotation={this.props.annotation}
                    selectable={this.props.selectable}
                    selection={this.props.selection && this.props.selectable}
                    onObjectActiveArea={this.onObjectActiveArea}
                    onObjectRemoved={this.onRemoveObject}
                    onObjectSelected={this.onObjectSelected}
                    onSelectionCleared={this.onObjectDeselected}
                    onObjectScaling={this.onObjectScaling}
                    onObjectMoving={this.onObjectMoving}
                    onMouseDown={this.onMouseDown}
                    onPathCreated={this.onPathCreated}
                    onMouseUp={this.onMouseUp}
                    onObjectModified={this.onObjectModified}>
                    {this.renderChildren()}
                </RelativeCanvas>
            </div>
        );
    }
}
