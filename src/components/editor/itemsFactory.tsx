import * as React from 'react';
import * as _ from 'lodash';

import { Shape, Line, ActiveArea, Ellipse, Image, Photo, Text, Path } from '../canvasItems';
import { GeometryItem } from '../../model/abstractItems';

const mapTypeToComponent = {
    Line: Line,
    Arrow: Line,
    Activearea: ActiveArea,
    Ellipse: Ellipse,
    Image: Image,
    Photo: Photo,
    Text: Text,
    Textbox: Text,
    Path: Path
};

export function getComponentsForType (type: string) {
    type = _.capitalize(type);

    const defaultComponent = Shape;
    const component = mapTypeToComponent[type];

    if (_.isUndefined(component)) {
        return defaultComponent;
    }

    return component;
}

export interface IEditorItemProps extends React.Props<EditorItem> {
    id: string;
    target?: GeometryItem;
    select?: boolean;
    index?: number;
}

export class EditorItem extends React.PureComponent<IEditorItemProps, any> {
    public render () {
        const { target } = this.props;

        if (_.isNil(target)) {
            console.warn('Items factory:' + 'target is undefined');
            return <div data-id={this.props.id} data-type='error' data-mess='target is undefined' />;
        }

        const propsOwnConnect = _.pick(this.props, [ 'selectable', 'select', 'index', 'groupSelect' ]);
        const props = _.assign({}, target, propsOwnConnect);
        const component = getComponentsForType(target.type);

        return React.createElement(component, props);
    }
}
