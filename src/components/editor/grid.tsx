import * as React from 'react';
import * as _ from 'lodash';

export interface IGridProps {
    grid: number;
    height: number;
    width: number;
}

export class Grid extends React.PureComponent<IGridProps, any> {
    private getRow(height: number) {
        const { grid, width } = this.props;
        const row = { cols: [], height: height };
        let currentWidth = grid

        while (currentWidth <= width) {
            row.cols.push({ width: grid });
            currentWidth += grid;
        }

        if (width % grid != 0) {
            row.cols.push({ width: width % grid });
        }

        return row;
    }

    private getRows() {
        const rows = [];
        const { grid, height } = this.props;
        let currentHeight = grid;

        while (currentHeight <= height) {
            rows.push(this.getRow(grid));
            currentHeight += grid;
        }

        if (height % grid != 0)
            rows.push(this.getRow(height % grid));

        return rows;
    }

    private renderTableCell(height: number, width: number, i: number) {
        return <td className='snap-cell snap' key={i} style={{ height: height, width: width }} />;
    }

    private renderTableRow(row, id: number) {
        return (
            <tr className='snap-row snap' key={id}>
                {_.map(row.cols, (c, i) => this.renderTableCell(row.height, row.cols[i].width, i))}
            </tr>
        );
    }

    private renderTableBody() {
        const rows = this.getRows();

        return <tbody>{_.map(rows, (r, i) =>  this.renderTableRow(rows[i], i))}</tbody>;
    }

    public render () {
        return (
            <div className='editor--grid'>
                <table>
                    {this.renderTableBody()}
                </table>
            </div>
        );
    }
}
