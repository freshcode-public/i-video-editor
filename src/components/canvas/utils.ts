import * as f from 'fabric';
import * as _ from 'lodash';

export interface ICanvasSize {
    width: number;
    height: number;
}

export interface ICanvasConfig {
    /** Indicates whether group selection should be enabled  */
    selection?: boolean;
    /** When set to `false`, an object can not be selected for modification */
    selectable?: boolean;
    editorSize?: ICanvasSize;
    annotation?: boolean;
}

export interface ICanvasStore {
    canvas: f.Canvas;
    canvasConfig: ICanvasConfig;
}

export interface IEventMap<T> {
    onObjectAdded?: T;
    onObjectModified?: T;
    onObjectRotating?: T;
    onObjectScaling?: T;
    onObjectMoving?: T;
    onObjectRemoved?: T;
    onObjectSelected?: T;
    onBeforeSelectionCleared?: T;
    onSelectionCleared?: T;
    onPathCreated?: T;
    onMouseDown?: T;
    onMouseMove?: T;
    onMouseUp?: T;
    onMouseOver?: T;
    onMouseOut?: T;
    onMouseDblclick?: T;


    onObjectActiveArea?: T;
    onObjectPlay?: T;
}

const CANVAS_EVENTS: IEventMap<string> = {
    onObjectAdded: 'object:added',
    onObjectModified: 'object:modified',
    onObjectRotating: 'object:rotating',
    onObjectScaling: 'object:scaling',
    onObjectMoving: 'object:moving',
    onObjectRemoved: 'object:removed',
    onObjectSelected: 'object:selected',
    onBeforeSelectionCleared: 'before:selection:cleared',
    onSelectionCleared: 'selection:cleared',
    onPathCreated: 'path:created',
    onMouseDown: 'mouse:down',
    onMouseMove: 'mouse:move',
    onMouseUp: 'mouse:up',
    onMouseOver: 'mouse:over',
    onMouseOut: 'mouse:out',
    onMouseDblclick: 'mouse:dblclick',


    onObjectActiveArea: 'object:activearea',
    onObjectPlay: 'object:play'
};

export interface ICanvasEventProps extends IEventMap<Function> {}

export function subscribeToEvent(canvas: f.Canvas, props: ICanvasEventProps) {
    _.keys(CANVAS_EVENTS)
        .filter((event) => _.isFunction(props[event]))
        .forEach((event) => {
            canvas.on(CANVAS_EVENTS[event], props[event]);
        });
}

export function getCenterGroup(group: f.Group | f.Object): { top: number; left: number; scaleY: number; scaleX: number; } {
    if (group)
        return {
            top: group.top + group.height / 2,
            left: group.left + group.width / 2,
            scaleY: group.scaleY - 1,
            scaleX: group.scaleX - 1
        };

    return { top: 0, left: 0, scaleY: 0, scaleX: 0 };
}

export function getActiveObjects(canvas: fabric.Canvas): Array<fabric.Object> {
    const target = canvas.getActiveObject();
    const targetGroup = _.get(canvas.getActiveGroup(), '_objects', [ target ]);

    return _.filter(targetGroup, (obj) => _.has(obj, '_id') && _.has(obj, 'type'));
}

export function deselectActiveObjects(canvas: fabric.Canvas) {
    canvas.discardActiveGroup();
    canvas.discardActiveObject();
}

// TODO: rewrite this. use: x = R*cos(angle), y = R*sin(angle)
export function isClickZone(canvas: f.Canvas, event: f.IEvent, obj: { left: number, top: number, width: number, height: number }) {
    const groupCoordinates = getCenterGroup(event.target);
    const position = canvas.getPointer(event.e);

    let activeZone = {
        maxY: obj.top + obj.height + groupCoordinates.top,
                    minY: obj.top + groupCoordinates.top,
                    maxX: obj.left + obj.width + groupCoordinates.left,
                    minX: obj.left + groupCoordinates.left
                };

    if (this.angle > 45 && this.angle < 220)
        activeZone.maxX = activeZone.maxX - (2 * obj.width);

    if (this.angle > 135 && this.angle < 300)
        activeZone.maxY = activeZone.maxY - (2 * obj.height);

    return _.inRange(position.x, activeZone.minX, activeZone.maxX) && _.inRange(position.y, activeZone.minY, activeZone.maxY);
}

export function getStringSize(text: string, fontFamily: string, fontSize: number): { width: number; height: number; } {
    var str = document.createTextNode(text);
    var obj = document.createElement('A');

    obj.style.fontSize = fontSize + 'px';
    obj.style.fontFamily = fontFamily;
    obj.style.margin = 0 + 'px';
    obj.style.padding = 0 + 'px';

    obj.appendChild(str);
    if (!document.body || !document.body.appendChild) return { width: 0, height: 0 };

    document.body.appendChild(obj);
    var str_size = { width: obj.offsetWidth, height: obj.offsetHeight };
    document.body.removeChild(obj);

    return str_size;
}
