import * as React from 'react';
import * as PropTypes from 'prop-types';
import { ICanvasSize, ICanvasEventProps, subscribeToEvent, ICanvasStore, ICanvasConfig } from './utils';
import Broadcast from '../../model/broadcast';

import * as f from 'fabric';
const fabric = (require("fabric") as any).fabric;

export interface ICanvasProps extends React.Props<Canvas>, ICanvasEventProps, ICanvasConfig {
    drawingMode?: boolean;
}

export interface ICanvasState {}

export interface ICanvasContext {
    canvasStore: Broadcast<ICanvasStore>;
}

export class Canvas<P extends ICanvasProps = ICanvasProps>
    extends React.Component<P, ICanvasState>
    implements React.ChildContextProvider<ICanvasContext> {

    private canvasStore: Broadcast<ICanvasStore>;

    public static get childContextTypes() {
        return {
            canvasStore: PropTypes.object
        };
    }

    public static get defaultProps (): ICanvasProps {
        return {
            editorSize: { height: 645, width: 1140 },
            selection: true,
            selectable: true,
            drawingMode: false
        };
    }

    private canvasId: string = Date.now().toString();
    public canvas: f.Canvas;

    public getChildContext(): ICanvasContext {
        return {
            canvasStore: this.canvasStore
        };
    }

    protected getCanvasConfigStore (props: P): ICanvasStore {
        return {
            canvas: this.canvas,
            canvasConfig: {
                editorSize: props.editorSize,
                selection: props.selection,
                selectable: props.selectable,
                annotation: props.annotation
            }
        };
    }

    public componentDidMount() {
        this.initialize();
        this.configuration(this.props);

        this.canvasStore = new Broadcast<ICanvasStore>(this.getCanvasConfigStore(this.props));
        this.forceUpdate();
    }

    public componentWillReceiveProps(nextProps: P) {
        this.configuration(nextProps);
        this.canvasStore.setStore(this.getCanvasConfigStore(nextProps));

        this.canvas.renderAll();
    }

    private initialize() {
        this.canvas = new fabric.Canvas(this.canvasId);
        subscribeToEvent(this.canvas, this.props);
    }

    protected calcOffset(props: P) {
        this.canvas.setHeight(props.editorSize.height);
        this.canvas.setWidth(props.editorSize.width);

        this.canvas.calcOffset();
    }

    protected configuration(props: P) {
        this.canvas.selection = props.selection;
        this.canvas.isDrawingMode = props.drawingMode;

        this.calcOffset(props);
    }

    private renderChildren(): Array<JSX.Element> {
        if (!this.canvas) return;

        return React.Children.map(this.props.children, (child) => {
            if (!child) return;

            return React.cloneElement(child as any);
        });
    }

    public render() {
        return (
            <div>
                <canvas id={this.canvasId} />
                <div data-info='items'>
                    {this.renderChildren()}
                </div>
            </div>
        );
    }
}
