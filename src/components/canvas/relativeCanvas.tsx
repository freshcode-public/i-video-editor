import * as React from 'react';
import { Canvas, ICanvasProps } from './canvas';
import { ICanvasSize } from './utils';
import * as _ from 'lodash';

export interface IRelativeCanvasProps extends ICanvasProps {
    originalSize: ICanvasSize;
}

export class RelativeCanvas extends Canvas<IRelativeCanvasProps> {
    /**
     * @override
     */
    public static get defaultProps (): ICanvasProps {
        return _.assign({}, Canvas.defaultProps, {
            originalSize: Canvas.defaultProps.editorSize
        });
    }

    /**
     * @override
     */
    protected calcOffset(props: IRelativeCanvasProps) {
        const canvasSize = this.canvasSize(props);

        this.canvas.setZoom(canvasSize.zoom);
        this.canvas.setDimensions({
            width: canvasSize.width,
            height: canvasSize.height
        });

        this.canvas.calcOffset();
    }

    private canvasSize(props: IRelativeCanvasProps) {
        const zoom = props.editorSize.width / props.originalSize.width;
        return {
            width: props.originalSize.width * zoom,
            height: props.originalSize.height * zoom,
            zoom: zoom
        };
    }
}
