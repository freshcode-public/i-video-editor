import * as React from 'react';
import * as _ from 'lodash';

export interface IEvents {
    onAbort?: Function;
    onCanPlay?: Function;
    onCanPlayThrough?: Function;
    onDurationChange?: Function;
    onEmptied?: Function;
    onEncrypted?: Function;
    onEnded?: Function;
    onError?: Function;
    onLoadedData?: Function;
    onLoadedMetadata?: Function;
    onLoadStart?: Function;
    onPause?: Function;
    onPlay?: Function;
    onPlaying?: Function;
    onProgress?: Function;
    onRateChange?: Function;
    onSeeked?: Function;
    onSeeking?: Function;
    onStalled?: Function;
    onSuspend?: Function;
    onTimeUpdate?: Function;
    onVolumeChange?: Function;
    onWaiting?: Function;
}

export interface IWrappingEvents {
    onFullscreen?: Function;
}

export interface IState {
    networkState?: number;
    paused?: boolean;
    muted?: boolean;
    volume?: number;
    error?: boolean;
    loading?: boolean;
    duration?: number;
    currentTime?: number;
    buffered?: TimeRanges;
    readyState?: number;
    percentageBuffered?: number;
    percentagePlayed?: number;
}

export interface IProps extends React.Props<any> {
    wrappingEvents?: IWrappingEvents;
    events?: IEvents;
    type?: string;
    trim?: {
        min?: number;
        max?: number;
    };

    // HTML5 Video standard attributes
    autoPlay?: boolean;
    muted?: boolean;
    controls?: boolean;
}

var EVENTS = [
    'onAbort',
    'onCanPlay',
    'onCanPlayThrough',
    'onDurationChange',
    'onEmptied',
    'onEncrypted',
    'onEnded',
    'onError',
    'onLoadedData',
    'onLoadedMetadata',
    'onLoadStart',
    'onPause',
    'onPlay',
    'onPlaying',
    'onProgress',
    'onRateChange',
    'onSeeked',
    'onSeeking',
    'onStalled',
    'onSuspend',
    'onTimeUpdate',
    'onVolumeChange',
    'onWaiting'
];

export abstract class GenericPlayer<P extends IProps, S extends IState> extends React.Component<P, S> {
    private _mediaEl: HTMLMediaElement;
    private _updateStateFromVideo;
    private mediaEventProps;

    protected abstract localEvents(): IEvents;

    protected privateEvents(): IEvents {
        return {
            onCanPlay: (ev) => {
                if (!!this.props.trim) {
                    var startTime = _.get(this.props, 'trim.min', 0);
                    if (this._mediaEl.currentTime === 0 && startTime != 0)
                        this._mediaEl.currentTime = startTime;
                }
            },
            onTimeUpdate: (ev) => {
                const { currentTime, duration } = ev.target
                const min = _.get(this.props, 'trim.min', 0);
                const _duration = _.get(this.props, 'trim.max', duration) - min;

                if (!!this.props.trim && currentTime > min + _duration) {
                    this.load();
                }
            }
        }
    }

    public static defaultProps: IProps = {
        events: null,
        type: 'video/mp4',
    }

    protected get mediaEl(): HTMLMediaElement {
        return this._mediaEl;
    }

    protected set mediaEl(element: HTMLMediaElement) {
        this._mediaEl = element;
    }

    protected defaultState: IState = {
        networkState: 0,
        paused: !this.props.autoPlay,
        muted: !!this.props.muted,
        volume: 1,
        error: false,
        loading: false,
    }

    protected checkEvents(events: IEvents, eventName: string) {
        return !!events
            && !!events[eventName]
            && typeof events[eventName] === 'function';
    }

    protected get playerProps() {
        const duration = _.get(this.mediaEl, 'duration', 0);
        const currentTime = _.get(this.mediaEl, 'currentTime', 0);

        let _currentTime = currentTime - _.get(this.props, 'trim.min', 0);
        let _duration = _.get(this.props, 'trim.max', duration) - _.get(this.props, 'trim.min', 0);

        if (_currentTime < 0) _currentTime = 0;

        return { duration: _duration, currentTime: _currentTime };
    }

    protected updateStateFromVideo() {
        const { duration, currentTime } = this.playerProps;

        const state = {
            // Standard video properties
            duration: duration,
            currentTime: currentTime,
            buffered: this.mediaEl.buffered,
            paused: this.mediaEl.paused,
            muted: this.mediaEl.muted,
            volume: this.mediaEl.volume,
            readyState: this.mediaEl.readyState,

            // Non-standard state computed from properties
            percentageBuffered: this.mediaEl.buffered.length && this.mediaEl.buffered.end(this.mediaEl.buffered.length - 1) / _.get(this.mediaEl, 'duration', 0) * 100,
            percentagePlayed: currentTime / duration * 100,
            error: this.mediaEl.networkState === this.mediaEl.NETWORK_NO_SOURCE,
            loading: this.mediaEl.readyState < this.mediaEl.HAVE_ENOUGH_DATA
        } as IState;

        this.setState(state as any) ;
    }

    // This function should be called in the class of heir
    public componentDidUpdate() {
        let children = this.mediaEl && this.mediaEl.children && this.mediaEl.children.length;

        if (!!children)
            this.mediaEl.children[this.mediaEl.children.length - 1]
                .addEventListener('error', this._updateStateFromVideo);
    }

    // This function should be called in the class of heir
    public componentWillUnmount() {
        let children = this.mediaEl && this.mediaEl.children && this.mediaEl.children.length;

        // Remove event listener from video.
        if (!!children)
            this.mediaEl.children[this.mediaEl.children.length - 1]
                .removeEventListener('error', this._updateStateFromVideo);

        this._updateStateFromVideo.cancel();
    }

    // This function should be called in the class of heir
    public componentWillMount() {
        var localEvents = this.localEvents();
        var globalEvents = this.props.events;
        var privateEvents = this.privateEvents();

        this._updateStateFromVideo = _.throttle(this.updateStateFromVideo, 100);
        this.mediaEventProps = EVENTS.reduce((p, c) => {
            p[c] = (e) => {
                // A prop exists for this mediaEvent, call it.
                if (this.checkEvents(globalEvents, c))
                    globalEvents[c](e, this._mediaEl);

                if (this.checkEvents(localEvents, c))
                    localEvents[c](e, this._mediaEl);

                if (this.checkEvents(privateEvents, c))
                    privateEvents[c](e, this._mediaEl);

                this._updateStateFromVideo();
            };
            return p;
        }, {})
    }

    public play() {
       this.mediaEl.play();
    }

    public pause() {
        this.mediaEl.pause();
    }

    public load() {
        this.mediaEl.load();
        this.updateStateFromVideo();
    }

    public unmute() {
        this.mediaEl.muted = false;
    }

    public mute() {
        this.mediaEl.muted = true;
    }

    public togglePlay() {
        if (this.state.paused) {
            this.play();
        } else {
            this.pause();
        }
    }

    public toggleMute() {
        if (this.state.muted) {
            this.unmute();
        } else {
            this.mute();
        }
    }

    public seek(time: number, forceUpdate: boolean) {
        const { trim } = this.props;
        this.mediaEl.currentTime = time + _.get(trim, 'min', 0);

        if (forceUpdate) {
            this.updateStateFromVideo();
        }
    }

    public fullscreen() {
        var func = _.get(this.props, 'wrappingEvents.onFullscreen') as Function;

        if (!!func &&  typeof func == 'function')
            return func();

        var mediaEl = this.mediaEl as any;

        if (mediaEl.requestFullscreen) {
            mediaEl.requestFullscreen();
        } else if (mediaEl.msRequestFullscreen) {
            mediaEl.msRequestFullscreen();
        } else if (mediaEl.mozRequestFullScreen) {
            mediaEl.mozRequestFullScreen();
        } else if (mediaEl.webkitRequestFullscreen) {
            mediaEl.webkitRequestFullscreen();
        }
    }

    public setVolume(volume: number, forceUpdate: boolean) {
        this.mediaEl.volume = volume;

        if (forceUpdate) {
            this.updateStateFromVideo();
        }
    }

    protected renderSources(): JSX.Element | JSX.Element[] {
        return React.Children.map(this.props.children, (child: JSX.Element) => {
            if (child.type !== 'source') {
                return void 0;
            }
            return child;
        });
    }

    protected renderMediaEl(): JSX.Element {
        const type = this.props.type;
        const mime = _.get(/^(\w+)/i.exec(type), 1);
        const video = <video>{this.renderSources()}</video>;
        const audio = <audio>{this.renderSources()}</audio>;
        var el;

        switch (mime) {
            case 'video': el = video; break;
            case 'audio': el = audio; break;
            default: el = video;
        }

        const props = _.assign({
                ref: (el) => { this.mediaEl = el; } ,
                className: `${mime}__el`
            },
            _.omit(this.props, [
                'controls',
                'events',
                'trim',
                'title',
                'autoResizePanelPosition',
                'styleControls',
                'styleOverlay',
                'styleRootInfo',
                'rootClassName',
                'wrappingEvents'
            ]),
            this.mediaEventProps,
            { controls: false }
        )

        return React.cloneElement(el, props)
    }

    protected rootClassName(): string {
        var classString = 'palyer--component';

        if (this.state.error) {
            classString += ' video--error';
        } else if (this.state.loading) {
            classString += ' video--loading';
        } else if (this.state.paused) {
            classString += ' video--paused';
        } else {
            classString += ' video--playing';
        }

        return classString;
    }
}
