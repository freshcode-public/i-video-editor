import * as React from 'react';
import * as _ from 'lodash';

export interface ITimeProps {
    currentTime?: number;
    duration?: number;
}

export interface ISeekProps {
    keys?: any;
    seek?: Function;
    percentageBuffered?: number;
    percentagePlayed?: number;
    duration?: number;
}

export interface ISeekState {
    focused: boolean;
}

export interface IPlayProps {
    keys?: any;
    togglePlay?: Function;
    paused?: boolean;
}

export interface IProgressBarProps {
    orientation?: string;
    step?: number;
    progress?: number;
    onChange?: Function;
    onFocus?: Function;
    onBlur?: Function;
}

export interface IFullscreenProps {
    type?: string;
    fullscreen?: Function;
    keys?: any;
}

export interface IMuteProps {
    keys?: any;
    volume?: number;
    unmute?: Function;
    setVolume?: Function;
    toggleMute?: Function;
    muted?: boolean;
}

export interface IIconProps {
    name?: string;
    style?: any;
}

export class Time extends React.Component<ITimeProps, any> {
    public shouldComponentUpdate(nextProps: ITimeProps): boolean {
        return this.props.currentTime !== nextProps.currentTime ||
               this.props.duration !== nextProps.duration;
    }

    private formatTime(seconds: number): string {
        seconds = isNaN(seconds) ? 0 : Math.floor(seconds);
        return seconds.toString();

        // var date = new Date(null);
        // date.setSeconds(seconds);
        // return date.toISOString().substr(11, 8);
    }

    render() {
        return (
            <div className="video-time video__control">
                <span className="video-time__current">
                    {this.formatTime(this.props.currentTime)}
                </span>
                /
                <span className="video-time__duration">
                    {this.formatTime(this.props.duration)}
                </span>
                <span> sec </span>

            </div>
        );
    }
}

export class Seek extends React.Component<ISeekProps, ISeekState> {
    public state: ISeekState = {
        focused: false
    }

    public shouldComponentUpdate(nextProps: ISeekProps) {
        return this.props.seek !== nextProps.seek ||
               this.props.percentageBuffered !== nextProps.percentageBuffered ||
               this.props.percentagePlayed !== nextProps.percentagePlayed ||
               this.props.duration !== nextProps.duration;
    }

    private seek(e) {
        this.props.seek(e.target.value * this.props.duration / 100, true);
    }

    private onFocus() {
        this.setState({ focused: true });
    }

    private onBlur() {
        this.setState({ focused: false });
    }

    public render() {
        return (
            <div className={'video-seek video__control' + (this.state.focused
                    ? ' video__control--focused' : '')}
                    aria-label={this.props.keys.seek}>
                <div className="video-seek__container">
                    <div className="video-seek__buffer-bar" style={{ width: this.props.percentageBuffered + '%' }} />
                    <ProgressBar
                        orientation='horizontal'
                        onBlur={this.onBlur.bind(this)}
                        onFocus={this.onFocus.bind(this)}
                        onChange={this.seek.bind(this)}
                        progress={this.props.percentagePlayed} />
                </div>
            </div>
        );
    }
}

export class Play extends React.Component<IPlayProps, any> {
    public shouldComponentUpdate(nextProps) {
        return this.props.paused !== nextProps.paused ||
               this.props.togglePlay !== nextProps.togglePlay;
    }

    public render() {
        return (
            <button
                className="video-play video__control"
                onClick={this.props.togglePlay as any}
                aria-label={this.props.paused
                        ? this.props.keys.play : this.props.keys.pause}>
                {this.props.paused ? <Icon name="play" /> : <Icon name="pause" />}
            </button>
        );
    }
}

export class ProgressBar extends React.Component<IProgressBarProps, any> {
    componentDidMount() {
        const input = this.refs['input'] as HTMLInputElement;
        input.setAttribute('orient', this.props.orientation);
    }

    render() {
        return (
            <div className={'video-progress-bar ' + (this.props.orientation === 'horizontal'
                ? 'video-progress-bar--horizontal' : 'video-progress-bar--vertical')}>
                <div className="video-progress-bar__fill" style={{
                    [this.props.orientation === 'horizontal' ? 'width' : 'height']: this.props.progress + '%'
                }} />
                <input className="video-progress-bar__input"
                    onBlur={this.props.onBlur as any}
                    onFocus={this.props.onFocus as any}
                    ref="input"
                    onChange={this.props.onChange as any}
                    type="range"
                    min="0"
                    max="100"
                    value={this.props.progress || 0}
                    step={this.props.step} />
            </div>
        );
    }
}

export class Fullscreen extends React.PureComponent<IFullscreenProps, any> {
    private eventsFullscreenchange = [ 'fullscreenchange', 'mozfullscreenchange', 'webkitfullscreenchange' ];
    private objFullScreenElement = [ 'fullscreenElement', 'mozFullScreenElement', 'webkitFullscreenElement'];

    constructor() {
        super();
        this.onChangeFullScreen = this.onChangeFullScreen.bind(this);
        this.state = { fullscreen: false };
    }

    private onChangeFullScreen() {
        let status = false;

        _.forEach(this.objFullScreenElement, (element) => {
            if (status) return;

            status = !!_.get(parent, [ 'window.document', element ], window.document[element]);
        });

        this.setState({ fullscreen: status });
    }

    public componentDidMount() {
        this.configuration('addEventListener');
    }

    public componentWillUnmount() {
        this.configuration('removeEventListener');
    }

    private configuration(eventListener: string) {
        if (this.props.fullscreen)
            _.map(this.eventsFullscreenchange, (eventName) => document[eventListener](eventName, this.onChangeFullScreen, false));
    }

    render() {
        return !this.state.fullscreen
            ? (
                <button
                    onClick={this.props.fullscreen as any}
                    className="video-fullscreen video__control"
                    aria-label={this.props.keys.fullscreen}>
                    {<Icon name="fullscreen" />}
                </button>
            ) : null;
    }
}

export class Mute extends React.Component<IMuteProps, any> {
    shouldComponentUpdate(nextProps) {
        return this.props.muted !== nextProps.muted ||
               this.props.toggleMute !== nextProps.toggleMute ||
               this.props.volume !== nextProps.volume ||
               this.props.setVolume !== nextProps.setVolume ||
               this.props.unmute !== nextProps.unmute;
    }

    private changeVolume(e) {
        this.props.setVolume(e.target.value / 100, true);
        this.props.unmute();

        return e.stopPropagation() || e.defaultPrevented;
    }

    private toggleMute(e: Event) {
        // If we volume has been dragged to 0, assume it is in
        // a muted state and then toggle to full volume.
        if (this.props.volume <= 0) {
            this.props.setVolume(1);
        } else {
            this.props.toggleMute();
        }

        return e.stopPropagation() || e.defaultPrevented
    }

    public render() {
        return (
            <div className="video-mute video__control" >
                <button
                    className="video-mute__inner"
                    onClick={this.toggleMute.bind(this)}
                    aria-label={this.props.muted || this.props.volume <= 0
                       ? this.props.keys.unmute : this.props.keys.mute}>
                    {this.props.muted || this.props.volume <= 0
                        ? <Icon name="mute" />
                        : <Icon name="unmute" />}
                </button>
                <div className="video-mute__volume">
                    <div className="video-mute__track">
                        <ProgressBar
                            orientation="vertical"
                            onChange={this.changeVolume.bind(this)}
                            progress={this.props.muted ? 0 : this.props.volume * 100} />
                    </div>
                </div>
            </div>
        );
    }
}

export class Icon extends React.Component<IIconProps, any> {
    private get className() {
        var className = [ 'video-icon fa' ];

        switch(this.props.name) {
            case 'fullscreen': className.push('fa-arrows-alt'); break;
            case 'unmute': className.push('fa-volume-up'); break;
            case 'mute': className.push('fa-volume-off'); break;
            case 'pause': className.push('fa-pause'); break;
            case 'play': className.push('fa-play'); break;

            default: className.push(this.props.name);
        }

        return className.join(' ').trim();
    }
    render() {
        return (
            <span className={this.className} style={this.props.style || {}}></span>
        );
    }
}
