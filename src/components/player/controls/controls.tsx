import * as React from 'react';
import * as _ from 'lodash';

import { Time, Play, Seek, Mute, Fullscreen } from './items';

export interface IProps extends React.Props<Controls> {
    fullscreen?: Function;
    togglePlay?: Function;
    toggleMute?: Function;
    setVolume?: Function;
    unmute?: Function;
    pause?: Function;
    play?: Function;
    mute?: Function;
    seek?: Function;
    error?: boolean;
    type?: string;
    style?: any;
    trim?: { min: number; max: number; };
    keys?: any;
}

export default class Controls extends React.Component<IProps, any> {
    public static defaultProps: IProps = {
        children: [ <Play />, <Seek />, <Time />, <Mute />, <Fullscreen /> ],
        style: {}
    }

    renderChildren() {
        const props = _.omit(this.props, [ 'style' ]);

        return React.Children.map(this.props.children, (child: any) => {
            return React.cloneElement(child, props);
        });
    }

    public render() {
        return (
            !this.props.error ? (
                <div className="video-controls video__controls" style={this.props.style}>
                    {this.renderChildren()}
                </div>
            ) : null
        );
    }
}
