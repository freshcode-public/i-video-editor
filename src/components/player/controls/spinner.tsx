import * as React from 'react';

export default class Spinner extends React.Component<any, any> {
    render() {
        return (
            <div className='video-spinner'>
                <div className="video-spinner__rect1" />
                <div className="video-spinner__rect2" />
                <div className="video-spinner__rect3" />
                <div className="video-spinner__rect4" />
                <div className="video-spinner__rect5" />
            </div>
        );
    }
}
