import * as React from 'react';
import * as _ from 'lodash';

import Overlay, { keys } from './overlay';
import Controls from './controls/controls';
import { IEvents, IState as IStateBase, GenericPlayer, IProps as IPropsBase, IWrappingEvents } from './genericPlayer';

export interface IProps extends IPropsBase{
    title?: string;
    rootClassName?: string;
    className?: string;
    styleOverlay?: any;
    styleControls?: any;
    styleRootInfo?: any;
    autoResizePanelPosition?: boolean;
    wrappingEvents?: IWrappingEvents;
}

enum OrientationPlayer {
    vertical,
    horisontal
}

export interface IState extends IStateBase {
    focused?: boolean;
}

export default class Player extends GenericPlayer<IProps, IState> {
    private style = {
        controlPanel: {},
        rootInfo: {}
    }

    public state: IState = this.defaultState;

    public static defaultProps: IProps = {
        events: null,
        rootClassName: '',
        type: 'video/mp4',
        title: '',
        styleOverlay: {},
        styleControls: {},
        styleRootInfo: {},
        autoResizePanelPosition: false
    }

    protected localEvents(): IEvents {
        return {
            onCanPlay: this.handlerOnCanPlay.bind(this)
        }
    };

    public componentWillMount() {
        // It is important that this function was called
        super.componentWillMount();
    }

    public componentDidUpdate() {
        // It is important that this function was called
        super.componentDidUpdate();
    }

    public componentWillUpdate() {
        if (!this.state.loading)
            this.autoResizePanelPosition();
    }

    public componentWillUnmount() {
        // It is important that this function was called
        super.componentWillUnmount();
    }

    private autoResizePanelPosition() {
        const { type, autoResizePanelPosition } = this.props;

        if (!autoResizePanelPosition || _.get(/^(\w+)/i.exec(type), 1) != 'video')
            return;

        const container = this.refs['container'] as HTMLElement;
        const videoEl = this.mediaEl as HTMLVideoElement;
        const orientation = container.clientWidth - 100 >= container.clientHeight
            ? OrientationPlayer.vertical
            : OrientationPlayer.horisontal;
        var style;

        var vertical_style = ((container.clientWidth - (container.clientHeight * videoEl.videoWidth) / videoEl.videoHeight)) / 2 || 0;
        var horisontal_style = ((container.clientHeight - (container.clientWidth * videoEl.videoHeight) / videoEl.videoWidth)) / 2 || 0;

        vertical_style = vertical_style < 0 ? 0 : vertical_style;
        horisontal_style = horisontal_style < 0 ? 0 : horisontal_style;

        switch(orientation) {
            case OrientationPlayer.vertical:
                style = {
                    controlPanel: { left: vertical_style - 1, right: vertical_style - 1 },
                    rootInfo: { left: vertical_style + 10, right: vertical_style + 10 }
                }
                break;
            case OrientationPlayer.horisontal:
                style = {
                    controlPanel: { bottom: horisontal_style },
                    rootInfo: { top: horisontal_style }
                }
                break;
        }

        this.style = style;
    }

    private handlerOnCanPlay() {
        this.autoResizePanelPosition();
    }

    private onBlur() {
        this.setState({ focused: false });
    }

    private onFocus() {
        this.setState({ focused: true });
    }

    // override methods in generic class
    public rootClassName(): string {
        var className = super.rootClassName();

        className = [ className, this.props.rootClassName ].join(' ').trim();

        if (this.state.focused) {
           className += ' video--focused';
        }

        return className;
    }

    private renderControls() {
        const min = _.get(this.props, 'trim.min', 0);

        var styleControls = _.assign({}, this.props.styleControls, this.style.controlPanel);
        var extendedProps = _.assign({
            // The public methods that all controls should be able to use.
            togglePlay: this.togglePlay.bind(this),
            toggleMute: this.toggleMute.bind(this),
            play:       this.play.bind(this),
            pause:      this.pause.bind(this),
            mute:       this.mute.bind(this),
            unmute:     this.unmute.bind(this),
            seek:       this.seek.bind(this),
            fullscreen: this.fullscreen.bind(this),
            setVolume:  this.setVolume.bind(this),
            type:       this.props.type,
            trim:       this.props.trim
        }, this.state, { keys: keys });

        var controls = React.Children.map(this.props.children, (child: JSX.Element) => {
            if (child.type === 'source') {
                return void 0;
            }
            return React.cloneElement(child, extendedProps);
        }) as any;

        if (!controls || !controls.length) {
            controls = (
                <div>
                    <Overlay {...extendedProps} style={this.props.styleOverlay} />
                    <Controls {...extendedProps  as any} style={styleControls} />
                </div>
            );
        }

        return controls;
    }

    private renderRootInfo() {
        const style = _.assign({}, this.props.styleRootInfo, this.style.rootInfo);

        return (
            <div className='root__info' style={style}>
                <p>{this.props.title}</p>
            </div>
        )
    }

    public render() {
        const { controls } = this.props;

        return (
            <div ref='container'
                className={this.rootClassName()}
                tabIndex={0}
                onFocus={this.onFocus.bind(this)}
                onBlur={this.onBlur.bind(this)}>

                {this.renderMediaEl()}
                {controls ? this.renderControls() : null}
                {this.renderRootInfo()}
            </div>
        )
    }
}
