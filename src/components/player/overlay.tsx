import * as React from 'react';
import * as _ from 'lodash';

import Spinner from './controls/spinner';
import { Icon } from './controls/items'

export interface IProps {
    error?: Boolean;
    togglePlay?: Function;
    paused?: Boolean;
    keys?: any;
    loading?: Boolean;
    style?: any;
}

export const keys = {
    sourceError: 'Video cannot be played in this browser.',
    play: 'Play video',
    pause: 'Pause video',
    mute: 'Mute video',
    unmute: 'Unmute video',
    fullscreen: 'View video fullscreen',
    seek: 'Seek video'
}

export default class Overlay extends React.Component<IProps, any> {
    private renderContent() {
        var content;
        if (this.props.error) {
            content = (
                <div className="video-overlay__error">
                    <p className="video-overlay__error-text">{this.props.keys.sourceError}</p>
                </div>
            );
        } else if (this.props.loading) {
            content = (
                <div className="video-overlay__loader">
                    <Spinner />
                </div>
            );
        } else {
            content = (
                <div className="video-overlay__play" onClick={this.props.togglePlay as any}>
                    {this.props.paused ? <Icon name="play" style={this.props.style} /> : null}
                </div>
            );
        }
        return content;
    }

    public render() {
        return (
            <div className='video-overlay'>
                {this.renderContent()}
            </div>
        );
    }
}
