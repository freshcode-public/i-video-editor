import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';

import { Button } from '../ui/button';
import { ResetToolbarMode } from '../toolbarItems';

export interface IContextToolbar {
    selectItem: Array<Object>;
    selectItemId: string;
    onUpdateItem: Function;

    selectTool: Object;
    onSelectTool: Function;
    changeToolbarMode: Function;
}

export interface IToolbarState {
    mode?: string;
}
export interface IToolbarProps extends React.Props<Toolbar> {
    mode?: string;
    selectItem?: Array<Object>;
    selectitemId?: string;
    onUpdateItem?: Function;
    selectTool?: Object;
    onSelectTool?: Function;
}


export class Toolbar extends React.Component<IToolbarProps, IToolbarState> implements React.ChildContextProvider<IContextToolbar> {
    public static get childContextTypes() {
        return {
            selectItem: PropTypes.array,
            selectItemId: PropTypes.oneOf([ PropTypes.string, PropTypes.number ]),
            onUpdateItem: PropTypes.func.isRequired,
            onSelectTool: PropTypes.func.isRequired,
            selectTool: PropTypes.object,
            changeToolbarMode: PropTypes.func.isRequired
        };
    }

    public state: IToolbarState = { mode: '' };
    public context: IContextToolbar;

    public getChildContext(): IContextToolbar {
        return {
            selectItem: this.props.selectItem,
            selectItemId: this.props.selectitemId,
            onUpdateItem: this.onUpdateItemHandler.bind(this),
            onSelectTool: this.onSelectToolHandler.bind(this),
            selectTool: this.props.selectTool,
            changeToolbarMode: this.changeToolbarMode.bind(this)
        };
    }

    private changeToolbarMode (mode: string) {
        this.setState({ mode: mode });
    }

    protected onSelectToolHandler(object) {
        if (!_.isFunction(this.props.onSelectTool)) return;

        this.props.onSelectTool(object);
    }

    protected onUpdateItemHandler(object) {
        if (!_.isFunction(this.props.onUpdateItem)) return;

        this.props.onUpdateItem(object);
    }

    protected isRenderSection(child: any, sectionName: string): boolean {
        if (child.type !== ToolbarGroup) return false;

        const mode = _.isString(child.props.mode)
            ? child.props.mode === this.state.mode
            : true;

        return mode && child.props.view && child.props.position == sectionName;
    }

    private renderChildSection(side: string) {
        const childrens = React.Children.map(this.props.children, (child: React.ReactElement<any>, i: number) => {
            if (this.isRenderSection(child, side))
                return React.cloneElement(child, {});

            return void 0;
        });

        return (
            <div className={`section ${side}`}>
                {childrens}
            </div>
        );
    }

    public render() {
        return (
            <div className='editor--toolbar'>
                {this.renderChildSection('left')}
                {this.renderChildSection('center')}
                {this.renderChildSection('right')}
            </div>
        );
    }
}

export interface IToolbarGroupProps extends React.Props<ToolbarGroup> {
    position: 'left' | 'right' | 'center';
    view?: boolean;
    mode?: string;
    back?: boolean;
}

export class ToolbarGroup extends React.Component<IToolbarGroupProps, any> {
    public static get defaultProps (): IToolbarGroupProps {
        return {
            position: 'left',
            view: true,
            back: false
        }
    }

    private renderBackButton () {
        return (
            <ResetToolbarMode />
        );
    }

    public render () {
        return (
            <div className='toolbar--group'>
                {this.props.back && this.renderBackButton()}
                {this.props.children}
            </div>
        );
    }
}
