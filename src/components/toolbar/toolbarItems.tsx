import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { Button } from '../ui/button';
import { IContextToolbar } from './toolbar';

const style: { toolbarNavigation: string; separator: string } = null;

export interface IColorToolButtonProps extends React.Props<ColorToolButton> {
    propsName?: string;
}

export class ColorToolButton extends React.Component<IColorToolButtonProps, any> {
    public static get defaultProps(): IColorToolButtonProps {
        return {
            propsName: 'fill'
        }
    }

    public static get contextTypes() {
        return {
            selectItem: PropTypes.object,
            onUpdateItem: PropTypes.func.isRequired
        };
    }

    public context: {
        selectItem: Object,
        onUpdateItem: Function
    };

    public constructor(props: IColorToolButtonProps) {
        super(props);

        this.onChangeHandler = this.onChangeHandler.bind(this, props.propsName);
    }

    private onChangeHandler (propsName: string, e) {
        const item = _.assign({}, this.context.selectItem, { [propsName]: e.target.value });
        this.context.onUpdateItem(item);
    }

    public render () {
        const value = _.get(this.context, 'selectItem.fill', '#000000');

        return (
            <div>
                <select value={value} onChange={this.onChangeHandler as any}>
                    <option value='#ff0000'>Red</option>
                    <option value='#000000'>Black</option>
                </select>
            </div>
        );
    }
}
