import * as React from 'react';
import * as f from 'fabric';

import { AbstractShape, IAbstractShapeProps } from './abstractShape';
import { newGeometry } from '../../model/geometry';
import { ImageItem } from '../../model/abstractItems';

import { PicsModal } from '../modals/pics';

export interface IPhotoProps extends IAbstractShapeProps, Partial<ImageItem> {}

export class Photo extends AbstractShape<f.Object, IPhotoProps>  {
    protected draw (object = null, callback: Function) {
        newGeometry(this.props as any, (object: f.Line) => {
            object.on('image:loaded', () => super.draw(object, callback));
        }, { type: 'photo' });
    }

    protected subscribeEvents () {
        super.subscribeEvents();

        if (!this.isCreated) return;

        this.object.on('object:play', () => {
            // TODO: remove this
            (this.refs['pics'] as PicsModal).open({ url: this.props.url });
        });
    }

    public render() {
        return (
            <div>
                {super.render()}
                <PicsModal ref='pics' />
            </div>
        )
    }
}
