import * as React from 'react';
import * as f from 'fabric';

import { AbstractShape, IAbstractShapeProps } from './abstractShape';
import { newGeometry } from '../../model/geometry';
import { ImageItem } from '../../model/abstractItems';

export interface ILinkProps extends IAbstractShapeProps, Partial<ImageItem> {}

export class Link extends AbstractShape<f.Object, ILinkProps>  {
    protected draw (object = null, callback: Function) {
        newGeometry(this.props as any, (object: f.Line) => {
            object.on('image:loaded', () => super.draw(object, callback));
        }, { type: 'link' });
    }

    protected subscribeEvents () {
        super.subscribeEvents();

        if (!this.isCreated) return;

        this.object.on('object:play', () => {
            this.redirectToLink(this.props['to']);
        });
    }

    private redirectToLink (url: string) {
        if (!(/^(https?|s?ftp):\/\/[a-z].*$/i).test(url))
            url = ['http://', url ].join('');

        window.open(url);
    }
}
