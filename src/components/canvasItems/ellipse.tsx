import * as React from 'react';
import * as f from 'fabric';

import { AbstractShape, IAbstractShapeProps } from './abstractShape';
import { newGeometry } from '../../model/geometry';
import { EllipseItem } from '../../model/abstractItems';

export interface IEllipseProps extends IAbstractShapeProps, Partial<EllipseItem> {}

export class Ellipse extends AbstractShape<f.Object, IEllipseProps>  {
    protected draw (object = null, callback: Function) {
        newGeometry(this.props as any, (object: f.Line) => {
            super.draw(object, callback);
        }, { type: 'ellipse' });
    }
}
