import * as React from 'react';
import * as f from 'fabric';

import { AbstractShape, IAbstractShapeProps } from './abstractShape';
import { newGeometry } from '../../model/geometry';
import { Items } from '../../model/items';

export class ActiveArea extends AbstractShape {
    constructor (props: IAbstractShapeProps, context) {
        super(props, context);
        this.onMousedownHandler = this.onMousedownHandler.bind(this);
    }

    protected draw (object = null, callback: Function) {
        const override: f.IObjectOptions = {
            type: Items.Activearea.type,
            fill: this.getSelectable() ? Items.Activearea.fill : 'transparent',
            stroke: Items.Activearea.stroke
        };

        newGeometry(this.props as any, (object: f.Line) => {
            super.draw(object, callback);
        }, override);
    }

    protected configuration (props: IAbstractShapeProps, context: any) {
        this.updateObjectProps('fill', this.getSelectable() ? Items.Activearea.fill : 'transparent');
        super.configuration(props, context);
    }

    protected onMousedownHandler () {
        if (!this.getSelectable()) {
            this.fire('object:activearea');
        }
    }

    protected subscribeEvents () {
        if (!this.isCreated) return false;

        this.object.on('mouseup', this.onMousedownHandler);
    }
}
