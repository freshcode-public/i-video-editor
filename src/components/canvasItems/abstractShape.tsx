import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as f from 'fabric';
import * as _ from 'lodash';

import { renderAnnotation } from '../../model/canvasUtils/annotation';
import { GeometryItem } from '../../model/abstractItems';
import { getCenterGroup, getActiveObjects, deselectActiveObjects, ICanvasStore } from '../canvas/utils';
import { ICanvasContext } from '../canvas/canvas';

const DeepDiff = (require('deep-diff') as any).default;
const fabric = (require('fabric') as any).fabric;

export interface IAbstractShapeProps extends React.Props<AbstractShape>, Partial<GeometryItem> {
    selectable?: boolean;
    select?: boolean;
    groupSelect?: boolean;
}

export abstract class AbstractShape<I extends f.Object = f.Object, P extends IAbstractShapeProps = IAbstractShapeProps>
    extends React.PureComponent<P, any> {

    public static get contextTypes() {
        return {
            canvasStore: PropTypes.object
        };
    }

    public static get defaultProps(): IAbstractShapeProps {
        return {
            select: false,
            groupSelect: false
        };
    }

    public context: ICanvasContext;
    protected object: I = new fabric.Object();
    protected isCreated = false;

    public constructor (props: P, context: ICanvasContext) {
        super(props, context);

        this.onUpdateCanvasStore = this.onUpdateCanvasStore.bind(this);
    }

    private onUpdateCanvasStore () {
        this.forceUpdate();
    }

    public componentDidMount() {
        this.context.canvasStore.subscribe(this.onUpdateCanvasStore);
    }

    public getSelectable (): boolean {
        if (_.isNil(this.props.selectable))
            return this.getCanvasStore().canvasConfig.selectable;

        return this.props.selectable;
    }

    public componentWillMount() {
        this.draw(null, (object: I) => {
            this.isCreated = true;

            this.renderAnnotation(object);
            this.getCanvasStore().canvas.add(object);
            this.configuration(this.props, this.context);
            this.subscribeEvents();
            this.select(this.props);
        });
    }

    public componentWillUnmount () {
        this.remove(true);

        if (localStorage.getItem('logger'))
            console.log(`Unmount item. type: %o id: %o`, this.props.type, this.props._id);

        this.context.canvasStore.unsubscribe(this.onUpdateCanvasStore);
    }

    public componentWillReceiveProps (nextProps: P, nextContext: ICanvasContext) {
        if (!this.isCreated) return;

        if (localStorage.getItem('logger'))
            this.logger(this.props, nextProps);

        this.select(nextProps);
        this.updateItem(this.props, nextProps);
    }

    protected getCanvasStore () {
        return this.context.canvasStore.getStore();
    }

    protected draw(object: I, save?: Function) {
        if (object instanceof fabric.Object) {
            this.object = object;

            if (_.isFunction(save)) {
                save(this.object);
            }
        }
    }

    private renderAnnotation (geometry: f.Object) {
        const self = this;
        const originalRender: Function = geometry['render'];

        geometry['render'] = function(ctx: CanvasRenderingContext2D, noTransform: boolean) {
            if (self.getCanvasStore().canvasConfig.annotation)
                renderAnnotation.call(geometry, ctx);

            originalRender.call(geometry, ctx, noTransform);
        }
    }

    public componentDidUpdate () {
        this.configuration(this.props, this.context);
    }

    protected subscribeEvents () { }

    protected fire (event: string) {
        if (!this.isCreated) return;

        this.object.trigger(event);
        this.getCanvasStore().canvas.trigger(event, { e: null, target: this.object });
    }

    protected select (props: P) {
        if (!props.select) return;

        const canvas = this.getCanvasStore().canvas;
        const activeObject = canvas.getActiveObject();
        const activeId = activeObject && activeObject.get('_id');

        if (activeId != props._id && !props.groupSelect) {
            this.getCanvasStore().canvas.setActiveObject(this.object);
        }
    }

    protected configuration (props: P, context: ICanvasContext) {
        const selectable = this.getSelectable();

        this.updateObjectProps('selectable', selectable);
        this.updateObjectProps('hoverCursor', selectable ? 'move' : 'default');
        this.getCanvasStore().canvas.renderAll();
    }

    protected updateItem(oldItem: P, newItem: P) {
        const difference = DeepDiff.diff(oldItem, newItem);
        const comparisons = difference;

        if (_.isEmpty(comparisons)) {
            return;
        }

        let activeGroup = this.containsItemInGroup(this.object);
        let groupCoordinates = getCenterGroup(activeGroup);
        let propsName, value;

        comparisons.forEach((diff) => {
            propsName = diff.path[0];
            value = newItem[propsName];

            // shift within the group
            if (_.includes([ 'left', 'top' ], propsName))
                value = diff.rhs - groupCoordinates[propsName];

            this.updateObjectProps(propsName, value);
        });

        this.object.setCoords();
        this.getCanvasStore().canvas.renderAll();

        return comparisons;
    }

    protected logger(oldItem: P, newItem: P) {
        const difference = DeepDiff.diff(oldItem, newItem);
        const comparisons = difference;

        if (_.isEmpty(comparisons)) {
            return;
        }

        let propsName;
        console.group(`Update item. type: %o id: %o`, newItem.type, newItem._id);
        comparisons.forEach((diff) => {
            console.log('Name: %o, old: %o new: %o', diff.path.join('.'), _.get(oldItem, diff.path), diff.rhs);
        });

        console.groupEnd();
        return comparisons;
    }

    /**
     * Removing from canvas
     */
    protected remove (unmount: boolean = false) {
        if (!this.isCreated) return;

        const canvas = this.getCanvasStore().canvas;
        const isActive = _.find(getActiveObjects(canvas), (obj) => obj['_id'] == this.props._id);

        if (isActive)
            deselectActiveObjects(canvas);

        this.updateObjectProps('selectable', false);
        this.updateObjectProps('_unmount', unmount);
        this.object.off();
        this.object.remove();
    }

    protected containsItemInGroup(item: f.Object): fabric.Group {
        let activeGroup = this.getCanvasStore().canvas.getActiveGroup();
        let isContainsItemInGroup = activeGroup && activeGroup.contains(item);

        if (isContainsItemInGroup)
            return activeGroup;
    }

    protected updateObjectProps<V>(key: string, value: V) {
        if (this.isCreated) {
            this.object.set(key, value);
        }
    }

    public render() {
        return (
            <div
                data-id={this.props._id}
                data-type={this.object.type}
                data-isCreated={this.isCreated} />
        );
    }
}
