import * as React from 'react';
import * as f from 'fabric';
import * as _ from 'lodash';

import { AbstractShape } from './abstractShape';
import { newGeometry } from '../../model/geometry';

export class Line extends AbstractShape {
    protected draw (object = null, callback: Function) {
        const override: f.IObjectOptions = {
            padding: 7,
            width: 0
        };

        const accessTypes = [ 'line', 'arrow' ]
        if (!_.includes(accessTypes, this.props.type)) {
            console.warn(`This component (Line) can not support this type (${this.props.type}). You can use the type (${accessTypes.join(', ')})`);
            return;
        }

        newGeometry(this.props as any, (object: f.Line) => {
            object.setControlsVisibility({ bl: false, br: false, mb: true, ml: false, mr: false, mt: true, tl: false, tr: false });

            super.draw(object, callback);
        }, override);
    }
}
