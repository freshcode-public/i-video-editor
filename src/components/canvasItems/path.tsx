import * as React from 'react';
import * as f from 'fabric';

import { AbstractShape, IAbstractShapeProps } from './abstractShape';
import { newGeometry } from '../../model/geometry';
import { PathItem } from '../../model/abstractItems';

export interface IPathProps extends IAbstractShapeProps, Partial<PathItem> {}

export class Path extends AbstractShape<f.IPath, IPathProps>  {
    protected draw (object = null, callback: Function) {
        newGeometry(this.props as any, (object: f.IPath) => {
            super.draw(object, callback);
        }, { type: 'path', padding: 3 });
    }
}
