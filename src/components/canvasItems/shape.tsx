import * as React from 'react';
import * as f from 'fabric';

import { AbstractShape } from './abstractShape';
import { newGeometry } from '../../model/geometry';

export class Shape extends AbstractShape {
    protected draw (object = null, callback: Function) {
        newGeometry(this.props as any, (object: f.Object) => {
            super.draw(object, callback);
        });
    }
}
