import * as React from 'react';
import * as f from 'fabric';

import { AbstractShape, IAbstractShapeProps } from './abstractShape';
import { newGeometry } from '../../model/geometry';
import { ImageItem } from '../../model/abstractItems';

export interface IEllipseProps extends IAbstractShapeProps, Partial<ImageItem> {}

export class Image extends AbstractShape<f.Object, IEllipseProps>  {
    protected draw (object = null, callback: Function) {
        newGeometry(this.props as any, (object: f.Line) => {
            super.draw(object, callback);
        }, { type: 'image' });
    }
}
