import * as React from 'react';
import * as f from 'fabric';
import * as _ from 'lodash';

import { NoteItem } from '../../model/abstractItems';
import { AbstractShape, IAbstractShapeProps,  } from './abstractShape';
import { ICanvasContext } from '../canvas/canvas';
import { newGeometry } from '../../model/geometry';

export interface ITextProps extends IAbstractShapeProps, Partial<NoteItem> {}

export class Text extends AbstractShape<f.IText, ITextProps> {
    protected draw (object = null, callback: Function) {
        const override: f.IITextOptions = {
            editable: this.props.selectable
        };

        const accessTypes = [ 'text', 'textbox' ];
        if (!_.includes(accessTypes, this.props.type)) {
            console.warn(`This component (Text) can not support this type (${this.props.type}). You can use the type (${accessTypes.join(', ')})`);
            return;
        }

        newGeometry(this.props as any, (object: f.IText) => {
            super.draw(object, callback);
        }, override);
    }

    protected configuration (props: ITextProps, context: ICanvasContext) {
        super.configuration(props, context);

        this.updateObjectProps('editable', this.getSelectable());
    }
}
