import * as React from 'react';

export default class ErrorComponent extends React.PureComponent<any, any> {
    public render () {
        return (
            <div className='error-awsnap'>
                <div className='img' />
                <div className='desc'>{this.props.desc}</div>
            </div>
        )
    }
}
