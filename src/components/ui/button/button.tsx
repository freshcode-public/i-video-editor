import * as React from 'react';
import * as _ from 'lodash';
import { File } from './file';

export interface IButtonProps extends React.Props<Button> {
    className?: string;
    onClick?: Function;
    title?: string;
    onFiles?: Function;
}

export class Button extends React.PureComponent<IButtonProps, any> {
    public constructor() {
        super();
        this.onClickHandler = this.onClickHandler.bind(this);
    }

    private get className(): string {
        const className = [ 'button', this.props.className ];

        if (_.isFunction(this.props.onFiles)) {
            className.push('button--file');
        }

        return className.join(' ').trim();
    }

    private onClickHandler (e) {
        if (!_.isFunction(this.props.onClick)) return;

        this.props.onClick(e);
    }

    public render () {
        const props = _.pick(this.props, [ 'title' ]);

        return (
            <button
                type='button'
                className={this.className}
                onClick={this.onClickHandler}
                {...props}
            >
                {_.isFunction(this.props.onFiles) ? <File onFiles={this.props.onFiles} /> : null}
                {this.props.children}
            </button>
        );
    }
}
