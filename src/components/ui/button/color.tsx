import * as React from 'react';
import * as _ from 'lodash';
import { SketchPicker } from 'react-color';

import { Button } from './button';

const  btnClassName: string = 'btn btn-sm btn-default';

export interface IColProps extends React.Props<ColorButton> {
    color?: string;
    onChange?: any;
    className?: string;
    disabled?: boolean;
    style?: {
        colorContainer?: Object;
        btnColor?: Object;
    }
}

export interface IColState {
    show?: boolean;
    color?: string;
}

export class ColorButton extends React.Component<IColProps, IColState> {
    public state: IColState = {
        show: false,
        color: this.props.color || 'rgba(0,0,0,1)'
    };

    private configure(props: IColProps, state: IColState, updateState: boolean = true) {
        if (updateState)
            this.setState({ color: props.color });
    }

    public componentWillReceiveProps(props: IColProps) {
        this.configure(props, this.state);
    }

    public componentWillMount() {
        this.configure(this.props, this.state);
    }

    public componentWillUpdate(props: IColProps, state: IColState) {
        this.configure(props, state, false);
    }

    private show() {
        !this.props.disabled && this.setState({ show: !this.state.show });
    }

    private hide() {
        this.setState({ show: false });
    }

    private change(color) {
        const components = ['r','g','b','a'].map((c) => color.rgb[c]);
        const rgb = `rgba(${components.join(',')})`;

        this.setState({ color: rgb });

        if (typeof this.props.onChange === 'function')
            this.props.onChange(rgb);
    }

    public render() {
        const { style } = this.props;
        const btnColor = _.get(style, 'btnColor', {});
        const colorContainer = _.get(style, 'colorContainer', {});
        const colorStyle = _.assign({}, btnColor, { backgroundColor: this.state.color });

        return (
            <span className={'button--color--container'} style={colorContainer}>
                <Button onClick={this.show.bind(this)} className='btn-default'>
                    <div className={'button--color'} style={colorStyle} />
                </Button>
                <div className='sketch--picker'>
                    {this.state.show && <SketchPicker
                        color={this.state.color}
                        onChangeComplete={this.change.bind(this)}
                        onClose={this.hide.bind(this)} />}
                </div>
            </span>
        );
    }
}
