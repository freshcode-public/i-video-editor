import * as React from 'react';
import * as _ from 'lodash';

export interface IFileProps {
    onFiles?: Function;
}

export class File extends React.Component<IFileProps, any> {
    public constructor () {
        super();
        this.onChange = this.onChange.bind(this);
    }

    private onChange (e) {
        const result = e.target.files;

        if (_.isFunction(this.props.onFiles)) {
            this.props.onFiles(result);
        }

        return result;
    }

    public render () {
        return (
            <input
                type='file'
                ref='file'
                multiple
                onChange={this.onChange}
            />
        )
    }
}
