import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';

export interface IPropsModal {
    title?: string;
    onClose?: Function;
    openAtCreate?: boolean;
}

export interface IStateModal {
    show?: boolean;
}

export class Modal extends React.PureComponent<IPropsModal, IStateModal> {
    public static get defaultProps (): IPropsModal {
        return {
            title: '',
            openAtCreate: false
        };
    }

    public constructor (props: IPropsModal) {
        super(props);
        this.onCloseHandler = this.onCloseHandler.bind(this);

        this.state = {
           show: props.openAtCreate
       };
    }

    public state: IStateModal;

    private onCloseHandler(e) {
        this.setState({ show: false });

        if (_.isFunction(this.props.onClose)) {
            this.props.onClose(e);
        }
    }

    public changeShowStatus (status: boolean = true) {
        this.setState({ show: status });
    }

    public get className() {
        const className = [ 'modal--backdrop' ];

        if (this.state.show)
            className.push('modal--backdrop-show');

        return className.join(' ').trim();
    }

    public render () {
        return (
            <div className={this.className}>
                <div className='modal-dialog'>
                    <div className='modal-header'>
                        <button type='button' className='close' onClick={this.onCloseHandler}>×</button>
                        <div className='modal-title'>{this.props.title}</div>
                    </div>
                    <div className='modal-body'>
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}
