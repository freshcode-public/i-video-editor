import * as React from 'react';

export interface ICircularSpinnerProps {
    style?: React.CSSProperties;
}

export class CircularSpinner extends React.PureComponent<ICircularSpinnerProps> {
    public static get defaultProps (): ICircularSpinnerProps {
        return {
            style: {}
        };
    }

    public render () {
        return (
            <div className='spinner--circular' style={this.props.style}>
               <ul className='spinner'>
                   <li />
                   <li />
                   <li />
                   <li />
               </ul>
           </div>
       );
    }
}
