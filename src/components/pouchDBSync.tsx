import * as React from 'react';
import { bindActionCreators, Action, Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as _ from 'lodash';

export interface IPouchDBSyncProps {
    dbname: string;
    onStartSync?: Function;
    onStopSync?: Function;
    onClearStore?: Function;
    live?: boolean;
    remoteHost?: string;
}

export class PouchDBSync extends React.PureComponent<IPouchDBSyncProps, any> {
    public static get defaultProps (): IPouchDBSyncProps {
        return {
            remoteHost: 'http://localhost:5984',
            dbname: undefined,
            live: false,
            onStartSync: undefined,
            onStopSync: undefined
        };
    }

    public componentDidMount () {
        this.startSync(this.props.dbname, this.props.live);
    }

    public componentWillReceiveProps (nextProps: IPouchDBSyncProps) {
        const { dbname, onStopSync, onStartSync } = this.props;

        if (this.isShouldReconnect(nextProps.dbname, nextProps.live)) {
            this.stopSync(dbname);
            this.startSync(nextProps.dbname, nextProps.live);
        }
    }

    public componentWillUnmount() {
        const { onClearStore } = this.props;

        this.stopSync(this.props.dbname);

        if (_.isFunction(onClearStore)) {
            onClearStore(this.props.dbname);
        }
    }

    private isShouldReconnect (dbname: string, live: boolean): boolean {
        if (!_.isString(dbname)) return false;

        return dbname !== this.props.dbname;
    }

    private stopSync (dbname: string) {
        const { onStopSync } = this.props;

        if (_.isString(dbname) && _.isFunction(onStopSync)) {
            onStopSync(dbname);
        }
    }

    private startSync (dbname: string, live: boolean) {
        const { onStartSync } = this.props;

        if (_.isString(dbname) && _.isFunction(onStartSync)) {
            onStartSync(dbname, live, this.props.remoteHost);
        }
    }

    public render() {
        return <div />;
    }
}
