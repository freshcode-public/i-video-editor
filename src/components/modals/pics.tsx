import * as React from 'react';
import { Modal } from '../ui';

export interface IPicsModalProps { }
export interface IPicsModalState {
    url?: string;
    title?: string;
}

export class PicsModal extends React.Component<IPicsModalProps, IPicsModalState> {
    public refs: {
        [name: string]: any;
        modal: Modal;
    }

    public state: IPicsModalState = {};

    public open (option: { url: string; title?: string }) {
        this.setState({ ...option }, () => {
            this.refs.modal.changeShowStatus(true);
        });
    }

    public close() {
        this.refs.modal.changeShowStatus(false);
    }

    public render () {
        return (
            <Modal ref='modal' onClose={this.close.bind(this)} title={this.state.title}>
                <img src={this.state.url} style={{ width: '100%' }} />
            </Modal>
        );
    }
}
