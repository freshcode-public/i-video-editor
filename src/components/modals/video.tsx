import * as React from 'react';
import * as _ from 'lodash';

import { Modal } from '../ui';
import  { default as Player } from '../player/player';

export interface IState {
    blob?: Blob;
    url?: string;
    type?: string;
    title?: string;
}

export class VideoModal extends React.Component<{}, IState> {
    public constructor () {
        super();
        this.onKeyHandler = this.onKeyHandler.bind(this);
    }
    public state: IState = {
        blob: null,
        url: null,
        type: null,
        title: ''
    };

    public refs: {
        [name: string]: any;
        modal: Modal;
        player: Player;
    }

    public componentDidMount() {
        document.addEventListener("keyup", this.onKeyHandler);
    }

    public componentWillUnmount() {
        document.removeEventListener('keyup', this.onKeyHandler);
    }

    private onKeyHandler(ev: KeyboardEvent) {
        const player = this.refs.player;
        var prevent = true;

        switch(true) {
            case ev.keyCode == 113: // f2
                player && player.fullscreen();
                break;
            case ev.keyCode == 32: // space
                player && player.togglePlay();
                break;
            default:
                prevent = false;
                break;
        }

        if (prevent) {
            ev.stopPropagation();
            ev.preventDefault();
        }
    }

    public stop() {
        const modal = this.refs.modal;
        modal && modal.changeShowStatus(false);

        return !this.open();
    }

    private realod() {
        const player = this.refs.player;
        player && player.load();
    }

    private get source() {
        const { url, type } = this.state;

        return url
            ? <source src={url} type={type} />
            : null;
    }

    private onEndedHandler() {
        this.stop();
    }

    public open (media?: any, type?: string, title?: string) {
        const { blob, url } = this.state

        const modal = this.refs.modal;
        const _blob = typeof media === 'object' ? media : null;
        const _url = _blob ? URL.createObjectURL(_blob) : media;
        const _type = _.get(media, 'type', type);

        if (blob && url)
            URL.revokeObjectURL(url);

        this.setState({
            blob: _blob,
            url: _url,
            type: _type,
            title: title,
        }, this.realod );

        modal && modal.changeShowStatus(!!_url);

        return !!_url;
    }

    public render() {
        return (
            <Modal ref='modal' title={this.state.title} onClose={this.stop.bind(this)}>
                <Player
                    ref='player'
                    type={this.state.type}
                    autoPlay
                    controls
                    events={{ onEnded: this.onEndedHandler.bind(this) }}
                >
                    {this.source}
                </Player>
            </Modal>
        )
    }
}
