import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Button } from '../ui/button';
import { ToolbarSeparator } from './separator';

export class ResetToolbarMode extends React.PureComponent {
    public static get contextTypes () {
        return {
            changeToolbarMode: PropTypes.func.isRequired
        };
    };

    public context: {
        changeToolbarMode: Function;
    };

    public constructor () {
        super();
        this.onHandlerClick = this.onHandlerClick.bind(this);
    }

    private onHandlerClick(e) {
        this.context.changeToolbarMode('');
    }

    public render() {
        return (
            <div className='toolbar--reset'>
                <ToolbarSeparator />
                <Button
                    className='btn-transparent btn-icon back'
                    onClick={this.onHandlerClick}
                    children={this.props.children} />
            </div>
        );
    }
}
