import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { Button } from '../ui/button';

export interface ISetToolButtonProps extends React.Props<SetToolButton> {
    value: { type: string };
    icon?: string;
    onClick?: Function;
}

export class SetToolButton extends React.Component<ISetToolButtonProps, any> {
    public static get contextTypes() {
        return {
            selectTool: PropTypes.object,
            onSelectTool: PropTypes.func.isRequired
        };
    }

    public static get propTypes () {
        return {
            value: PropTypes.shape({ type: PropTypes.string.isRequired }).isRequired
        };
    };

    public constructor (props: ISetToolButtonProps) {
        super(props);
        this.onHandlerClick = this.onHandlerClick.bind(this);
    }

    public context: {
        selectTool: Object,
        onSelectTool: Function
    };

    private get className(): string {
        const className: Array<string> = [ 'btn-default' ];

        if (this.props.icon)
            className.push('btn-icon', this.props.icon);

        if (this.isActiveTool) {
            className.push('active');
        }

        return className.join(' ').trim();
    }

    private get isActiveTool (): boolean {
        return _.get(this.context, 'selectTool.type', '') == this.props.value.type;
    }

    private onHandlerClick (e) {
        if (_.isFunction(this.props.onClick))
            this.props.onClick(e);

        this.context.onSelectTool(this.isActiveTool ? null : this.props.value);
    }

    public render () {
        return (
            <Button
                className={this.className}
                children={this.props.children}
                onClick={this.onHandlerClick} />
        );
    }
}
