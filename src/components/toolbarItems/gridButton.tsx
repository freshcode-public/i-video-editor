import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';

export interface IGridButtonProps {
    grid?: { size: number; enabled: boolean }
    onChangeGrid?: Function;
}

export interface IGridButtonState {
    size?: number;
}

export class GridButton extends React.PureComponent<IGridButtonProps, IGridButtonState> {
    public static get defaultProps (): IGridButtonProps {
        return {
            grid: { enabled: false, size: 50 }
        };
    }

    public state: IGridButtonState;

    public constructor(props: IGridButtonProps) {
        super(props);
        this.state = { size: this.props.grid.size };

        this.onChangeSize = this.onChangeSize.bind(this);
        this.onChangeStatus = this.onChangeStatus.bind(this);
        this.onLocalChangeSize = this.onLocalChangeSize.bind(this);
    }

    public componentWillReceiveProps(nextProps: IGridButtonProps) {
        if (nextProps.grid.size != this.props.grid.size) {
            this.setState({ size: nextProps.grid.size });
        }
    }

    private onLocalChangeSize (e) {
        this.setState({ size: parseInt(e.target.value) });
    }

    private onChangeSize (e) {
        this.onChangeGrid({ size: parseInt(e.target.value) });
    }

    private onChangeGrid (grid: { size?: number, enabled?: boolean }) {
        const { onChangeGrid } = this.props;
        let _grid = _.assign({}, this.props.grid, grid);

        if (_.isFunction(onChangeGrid)) {
            onChangeGrid(_grid);
        }
    }

    private onChangeStatus (e) {
        this.onChangeGrid({ enabled: e.target.checked });
    }

    public render () {
        return (
            <div className='grid--button'>
                <label htmlFor='grid-btn' children={'Grid'} />
                <input
                    id='grid-btn'
                    type='checkbox'
                    checked={this.props.grid.enabled}
                    onChange={this.onChangeStatus} />
                <input
                    type='range'
                    value={this.state.size}
                    disabled={!this.props.grid.enabled}
                    min={50} max={200} step={5}
                    onMouseUp={this.onChangeSize}
                    onChange={this.onLocalChangeSize} />
                <span style={{ color: '#000' }}>{this.state.size}</span>
            </div>
        );
    }
}
