import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { ColorButton } from '../ui/button';

export class ColorToolButton extends React.Component {
    public static contextTypes = {
        selectItem: PropTypes.array,
        onUpdateItem: PropTypes.func.isRequired,
    };

    public context: {
        selectItem: Array<Object>;
        onUpdateItem: Function;
    };

    private onChange(color: string) {
        const items = _.cloneDeep(this.context.selectItem);
        let path;

        _.forEach(items, (obj: any) => {
            path = this.getPathToObject(obj);

            if (path) {
                obj[path] = color;
            }
        });

        this.context.onUpdateItem(items);
    }

    private getPathToObject (item): string {
        switch (item.type) {
            case 'rect':
            case 'ellipse':
            case 'triangle':
            case 'arrow':
            case 'line':
            case 'path':
                return 'stroke';

            case 'text':
            case 'textbox':
                return 'fill';
        }
    }

    public render() {
        const item: any = this.context.selectItem[0] || {};
        const color = item[this.getPathToObject(item)];

        if (!color)
            return null;

        return (
            <div className='action-tool'>
                <ColorButton color={color} onChange={this.onChange.bind(this)} />
            </div>
        );
    }
}
