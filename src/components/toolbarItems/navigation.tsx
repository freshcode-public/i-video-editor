import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { Button } from '../ui/button';

export interface INavigatorButtonProps extends React.Props<ToolbarNavigation> {
    currentPosition: number;
    maxPosition: number;
    changePosition?: Function;
    clickNumber?: Function;
}

export class ToolbarNavigation extends React.PureComponent<INavigatorButtonProps, any> {
    public static propTypes: PropTypes.ValidationMap<any> = {
        currentPosition: PropTypes.number,
        maxPosition: PropTypes.number.isRequired
    };

    public static defaultProps: INavigatorButtonProps = {
        currentPosition: 0,
        maxPosition: 0,
        changePosition: undefined
    };

    private onChangePosition(delta: number, e: Event) {
        if (_.isFunction(this.props.changePosition))
            this.props.changePosition(delta);
    }

    private onClickNumber(e: Event) {
        if (_.isFunction(this.props.clickNumber))
            this.props.clickNumber();
    }

    public render() {
        return (
            <div className='toolbar--navigation'>
                <Button className='btn-icon-arrow prev' onClick={this.onChangePosition.bind(this, -1) } />
                <Button className='btn btn-primary number' onClick={this.onClickNumber.bind(this)}>
                    {this.props.currentPosition}/{this.props.maxPosition}
                </Button>
                <Button className='btn-icon-arrow next' onClick={this.onChangePosition.bind(this, 1)} />
            </div>
        );
    }
}
