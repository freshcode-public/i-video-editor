import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';

import { Items } from '../../model/items';
import { ActiveAreaItem } from '../../model/abstractItems'

export interface Action {
    typeView: string; // number OR option
    type: string;
    label?: string;

    // enable if typeView is option
    values?: { value: any; label: string; }[];

    // enable if typeView is number
    maxValue?: number;
    minValue?: number;
}


export interface IActiveAreaButtonProps {
    actions: Action[];
}

export class ActiveAreaButton extends React.Component<IActiveAreaButtonProps, {}> {
    public static contextTypes = {
        selectItem: PropTypes.array,
        onUpdateItem: PropTypes.func.isRequired,
    };

    public context: {
        selectItem: Array<ActiveAreaItem>;
        onUpdateItem: Function;
    };

    public static defaultProps: IActiveAreaButtonProps = {
        actions: []
    };

    public constructor () {
        super();

        this.changeActionValue = this.changeActionValue.bind(this);
        this.changeAction = this.changeAction.bind(this);
    }

    private getSelectItem() {
        return _.filter(this.context.selectItem, (i: ActiveAreaItem) => i.type === Items.Activearea.type);
    }

    private changeActionValue(value) {
        let selectItem = this.getSelectItem();
        let _selectItem = _.cloneDeep(selectItem);

        _.forEach(_selectItem, (i) => {
            if (_.has(i, [ 'action', 'type' ]) && i.action.type == selectItem[0].action.type) {
                i.action.data = value;
            };
        });
        this.context.onUpdateItem(_selectItem);
    }

    private changeAction(event: any) {
        let selectItem = this.getSelectItem();
        selectItem = _.cloneDeep(selectItem);

        _.forEach(selectItem, (i) => {
            i.action = {
                type: event.target.value,
                data: null
            };
        });
        this.context.onUpdateItem(selectItem);
    }

    private renderActionOptionValue(action: Action) {
        const actionItem = this.getSelectItem()[0]
        var itemAction = _.get(actionItem, 'action');
        var itemActionData = _.get<string>(itemAction, 'data');

        return (
            <div className='input-group input-group-sm'>
                <DelayedAction value={itemActionData || ''} onChange={this.changeActionValue}>
                    <select className='form-control'>
                        <option value={''} disabled children='Select ...' />
                        {_.map(action.values, (a, i) => <option key={i} value={a.value} children={a.label} />)}
                    </select>
                </DelayedAction>
            </div>
        );
    }

    private renderActionNumberValue(action: Action) {
        const actionItem = this.getSelectItem()[0]
        var itemAction = _.get(actionItem, 'action');
        var itemActionData = _.get<number>(itemAction, 'data');

        return (
            <div className='input-group input-group-sm'>
                <DelayedAction onChange={this.changeActionValue} value={itemActionData || 0}>
                    <input className='form-control input-sm'
                        type='number'
                        min={action.minValue || 0}
                        max={action.maxValue || 1} />
                </DelayedAction>
            </div>
        );
    }

    private renderActionValues(): JSX.Element {
        const actionItem = _.find(this.context.selectItem, (i: ActiveAreaItem) => i.type === Items.Activearea.type);
        var action = _.get(actionItem, 'action', { type: null, data: null });
        let propsAction = _.find(this.props.actions, (a) => a.type == _.get(action, 'type'));

        if (!propsAction) return null;

        switch (propsAction.typeView) {
            case 'number':
                return this.renderActionNumberValue(propsAction);

            case 'option':
                return this.renderActionOptionValue(propsAction);
        }

        return null;
    }

    private renderActions() {
        const actionItem = _.find(this.context.selectItem, (i: ActiveAreaItem) => i.type === Items.Activearea.type);
        var action = _.get<string>(actionItem, 'action');
        var type = _.get(action, 'type', '');

        return (
            <div className='input-group input-group-sm'>
                <select className='form-control' value={type} onChange={this.changeAction.bind(this) }>
                    <option value={''} disabled children='Select ...' />
                    {_.map(this.props.actions, (action, i) => <option key={i} value={action.type} children={action.label} />)}
                </select>
            </div>
        );
    }

    public render() {
        return (
            <div className='action-tool'>
                {this.renderActions()}
                {this.renderActionValues()}
            </div>
        );
    }
}

class DelayedAction extends React.Component<any, any> {
    public state: { value: any };

    public constructor(props) {
        super(props);

        this.state = { value: props.value };
        this.upd = _.debounce(this.props.onChange.bind(this), 300);
    }

    public update(event: any) {
        var value = event.target.value;

        this.setState({ value: value });
        this.upd(value);
    }

    private upd: any;

    public render() {
        const props = {
            onChange: this.update.bind(this),
            value: this.state.value
        };

        return React.cloneElement(this.props.children, props);
    }
}
