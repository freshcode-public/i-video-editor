import * as React from 'react';

export function ToolbarSeparator () {
    return (
        <span className='toolbar--separator' />
    );
}
