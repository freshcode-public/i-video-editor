import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';

export interface IOpacityToolState {
    value: number;
}

export class OpacityTool extends React.Component<{}, IOpacityToolState> {
    public static get contextTypes () {
        return {
            selectItem: PropTypes.array,
            onUpdateItem: PropTypes.func.isRequired,
        };
    };

    public context: {
        selectItem: Array<Object>;
        onUpdateItem: Function;
    };

    public state: IOpacityToolState = {
        value: _.get(this.context.selectItem, [ 0, 'opacity' ], 1)
    }

    private changeOpacity(ev) {
        let value = parseFloat(ev.target.value);
        this.setState({ value: value });
    }

    private onChange(ev) {
        let value = parseFloat(ev.target.value);

        const items = _.cloneDeep(this.context.selectItem);
        _.forEach(items, (obj: any) => obj.opacity = value);

        this.context.onUpdateItem(items);
    }


    public render() {
        return (
            <div className='opacity--button'>
                <input type='range' max={1} min={0} step={0.05}
                    onChange={this.changeOpacity.bind(this)}
                    value={this.state.value.toString()}
                    onMouseUp={this.onChange.bind(this)} />
                <label>{this.state.value}</label>
            </div>
        );
    }


}
