import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';

const FONT_SIZES = [ 12, 14, 16, 18, 20, 22, 24, 28, 32, 36, 40, 48 ];

export class FontSizeTool extends React.Component {
    public static get contextTypes () {
        return {
            selectItem: PropTypes.array,
            onUpdateItem: PropTypes.func.isRequired,
        };
    };

    public context: {
        selectItem: Array<Object>;
        onUpdateItem: Function;
    };

    private onChange(ev) {
        let value = parseInt(ev.target.value, 10);

        let items = _.cloneDeep(this.context.selectItem);
        items = _.filter(items, (item: any) =>  _.includes([ 'text', 'textbox' ], item.type));
        items.forEach((item: any) => item.fontSize = value);

        this.context.onUpdateItem(items);
    }

    public render() {
        const noteItem = _.find(this.context.selectItem, (i: any) => _.includes([ 'text', 'textbox' ], i.type));
        const fontSize = _.get(noteItem, 'fontSize', 10);

        return (
            <div className='font-size--button'>
                <select value={fontSize.toString()} onChange={this.onChange.bind(this)}>
                    {FONT_SIZES.map((size, i) => (
                        <option key={i} value={size.toString()}>{size}px</option>
                    ))}
                </select>
            </div>
        );
    }


}
