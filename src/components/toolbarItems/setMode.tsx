import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { Button } from '../ui/button';

export interface ISetToolbarModeProps {
    className?: string;
    icon?: string;
    transparent?: boolean;
    value: string;
    onClick?: Function;
}

export class SetToolbarMode extends React.PureComponent<ISetToolbarModeProps, any> {
    public static get propTypes () {
        return {
            value: PropTypes.string
        };
    };

    public static get contextTypes () {
        return {
            changeToolbarMode: PropTypes.func.isRequired
        };
    };

    public context: {
        changeToolbarMode: Function;
    };

    public constructor () {
        super();
        this.onHandlerClick = this.onHandlerClick.bind(this);
    }

    private get className(): string {
        const className = [ this.props.className, 'btn-default' ];

        if (this.props.icon)
            className.push('btn-icon', this.props.icon);

        if (this.props.transparent)
            className.push('btn-transparent');

        return className.join(' ').trim();
    }

    private onHandlerClick(e) {
        this.context.changeToolbarMode(this.props.value);

        if (_.isFunction(this.props.onClick))
            this.props.onClick(e);
    }

    public render() {
        return (
            <Button
                className={this.className}
                onClick={this.onHandlerClick}>
                    {this.props.children}
            </Button>
        );
    }
}
