import {
    reducersMap,
    IEditorItemsStore, INITIAL_EDITOR_ITEMS_STORE,
    IPresentationStore, INITIAL_PRESENTATIIN_STORE,
    ISyncronizerStore, INITIAL_SYNCRONIZER_STORE,
    IIVideoStore, INITIAL_IVIDEO_STORE
} from '../reducers';

export interface IStore {
    editorItems: IEditorItemsStore;
    presentation: IPresentationStore;
    syncronizer: ISyncronizerStore;
    iVideo: IIVideoStore;
}

export const PRELOADED_STORE: IStore = {
    editorItems: INITIAL_EDITOR_ITEMS_STORE,
    presentation: INITIAL_PRESENTATIIN_STORE,
    syncronizer: INITIAL_SYNCRONIZER_STORE,
    iVideo: INITIAL_IVIDEO_STORE
};
