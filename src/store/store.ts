import { compose, createStore, applyMiddleware, Store, Middleware } from 'redux';
import { createLogger } from 'redux-logger';
import * as _ from 'lodash';

import { asyncMiddleware } from '../model/middleware';
import { IStore, PRELOADED_STORE } from './storeInterface';
import { reducersMap } from '../reducers';

export function configureStore(initialStore: Partial<IStore> = PRELOADED_STORE): Store<IStore> {
    const _initialStore: IStore = _.assign({}, PRELOADED_STORE, initialStore);
    let composeEnhancers: any = compose;
    const enhancers: Array<any> = [];
    const middleware: Array<Middleware> = [
        asyncMiddleware(),
        createLogger()
    ];

    if (_.isFunction(window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__)) {
        composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    }

    const store = createStore<IStore>(
        reducersMap,
        _initialStore,
        composeEnhancers(
            applyMiddleware(...middleware),
            ...enhancers
        )
    );

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require<any>('../reducers').reducersMap;
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}

declare var module: {
    hot: any,
}

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}
