import * as React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect, MapStateToProps } from 'react-redux';

import * as Actions from '../actions/editorItems';
import { IStore } from '../store/storeInterface';
import { Toolbar } from '../components/toolbar/toolbar';

export interface IEditorContainerMapState {
    selectItemsId: Array<string>;
    selectItems: Array<Object>
}

function mapStateToProps(store: IStore): IEditorContainerMapState {
    const selectsId = store.editorItems.selectsId;

    return {
        selectItemsId: selectsId,
        selectItems: null // store.editorItems.itemsById[selectId]
    };
}

export interface IEditorContainerMapDispatch {
    onUpdateItem: Function;
}

function mapDispatchToProps(dispatch: Dispatch<any>): IEditorContainerMapDispatch {
    return {
        onUpdateItem: bindActionCreators(Actions.updateItem, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Toolbar as any);
