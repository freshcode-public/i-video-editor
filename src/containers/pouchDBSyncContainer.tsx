import * as React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../actions/syncronizer';
import { PouchDBSync, IPouchDBSyncProps } from '../components/pouchDBSync';

export interface IPouchDBSyncContainerMapDispatch {
    onStartSync?: Function;
    onStopSync?: Function;
    onClearStore?: Function
}

function mapDispatchToProps(dispatch: Dispatch<any>): IPouchDBSyncContainerMapDispatch {
    return {
        onStartSync: bindActionCreators(Actions.startSyncDB, dispatch),
        onStopSync: bindActionCreators(Actions.stopSyncDB, dispatch),
        onClearStore: bindActionCreators(Actions.onClearStore, dispatch)
    };
}

export default connect<any, IPouchDBSyncContainerMapDispatch, IPouchDBSyncProps>(null, mapDispatchToProps)(PouchDBSync);
