import * as React from 'react';
import * as _ from 'lodash';
import { bindActionCreators, Action } from 'redux';
import { connect } from 'react-redux';
import { EditorItem, IEditorItemProps } from '../components/editor/itemsFactory';
import * as editorItemsSelectors from '../selectors/editorItems';

import { IStore } from '../store/storeInterface';

interface IMapStateToProps {
    target?: Object;
    select?: boolean;
    groupSelect?: boolean;
}

function mapStateToProps (state: IStore, ownProps: IEditorItemProps): IMapStateToProps {
    const selectItems = editorItemsSelectors.getEditorSelectItemsId(state);
    const select = _.includes(selectItems, ownProps.id);

    return {
        target: editorItemsSelectors.getEditorItem(state, ownProps.id),
        select: select,
        groupSelect: selectItems.length >= 2 && select
    };
}

export default connect<IMapStateToProps, any, IEditorItemProps>(mapStateToProps)(EditorItem);
