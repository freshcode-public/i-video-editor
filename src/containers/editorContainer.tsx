import * as React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as _ from 'lodash';

import { Editor, IEditorProps, IEditorActionProps } from '../components/editor/editor';
import * as Actions from '../actions/editorItems';
import EditorItem from './editorItem';
import { IStore } from '../store/storeInterface';
import * as editorItemsSelectors from '../selectors/editorItems';

export interface IEditorContainerMapState {
    items: Array<string>;
    selectItemsId: Array<string>;
}

function mapStateToProps (store: IStore): IEditorContainerMapState {
    return {
        items: editorItemsSelectors.getEditorItemsId(store),
        selectItemsId: editorItemsSelectors.getEditorSelectItemsId(store)
    };
}

export interface IEditorContainerMapDispatch {
    onUpdateItem: Function;
    onRemoveItem: Function;
    onSelectItem: Function;
    onObjectCreated?: Function;
}

function mapDispatchToProps (dispatch: Dispatch<any>): IEditorContainerMapDispatch {
    return {
        onUpdateItem: bindActionCreators(Actions.updateItem, dispatch),
        onRemoveItem: bindActionCreators(Actions.removeItem, dispatch),
        onSelectItem: bindActionCreators(Actions.selectItem, dispatch)
    };
}

export interface IEditorContainerProps extends React.Props<EditorContainer>, IEditorContainerMapDispatch, IEditorContainerMapState {
}

export class EditorContainer extends React.PureComponent<IEditorContainerProps, any> {
    private renderChild (id) {
        return <EditorItem key={id} id={id} />;
    }

    public render () {
        const props = _.omit(this.props, [ 'onObjectCreated', 'onUpdateItem', 'onSelectItem', 'onRemoveItem' ])
        return (
            <Editor
                {...props}
                ref='editor'
                onObjectCreated={this.props.onObjectCreated}
                onObjectModified={this.props.onUpdateItem}
                onObjectSelected={this.props.onSelectItem}
                onRemoveObject={this.props.onRemoveItem}>
                {_.map(this.props.items, this.renderChild)}
            </Editor>
        );
    }
}

export default connect<IEditorContainerMapState, IEditorContainerMapDispatch, IEditorProps>
    (mapStateToProps, mapDispatchToProps, null, { withRef: true })(EditorContainer);
