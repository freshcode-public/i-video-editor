import * as React from 'react';

export function Container (props) {
    return (
        <div style={{ margin: 'auto', border: '1px solid #edf0f3', ...props }}>
            {props.children}
        </div>
    );
}
