import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { Provider } from 'react-redux';
import * as _ from 'lodash';

import { configureStore, containers } from '../src/index';
import { Container } from './containers';
import { size } from './helpers';

const { PouchDBSyncContainer, EditorContainer } = containers;

class DBChange extends React.Component<any, any> {
    state = {
        dbname: 'one'
    };

    onChangeDB () {
        const dbname = prompt('dbname', this.state.dbname);
        this.setState({ dbname: dbname || 'one' });
    }

    render () {
        return (
            <div>
                <div>
                    <button children='On change dbName' onClick={this.onChangeDB.bind(this)} />
                    <div>DBname: {this.state.dbname}</div>
                </div>

                <PouchDBSyncContainer dbname={this.state.dbname} live />
            </div>
        )
    }
}

storiesOf('Editor (in pouchdb)', module)
    .addDecorator((story) => {
        return (
            <Container minHeight={size.height} width={size.width}>
                {story()}
            </Container>
        );
    })
    .add('no name', () => {
        const store = configureStore();
        return (
            <Provider store={store}>
                <div>
                    <DBChange />
                    <EditorContainer />
                </div>
            </Provider>
        );
    });
