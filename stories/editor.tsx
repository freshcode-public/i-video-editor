import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { Provider } from 'react-redux';
import * as _ from 'lodash';

import { containers, configureStore, } from '../src/index';
import { Container } from './containers';
import { size, getRandomItems } from './helpers';

const { EditorContainer } = containers;
const grid = { enabled: true, size: 50 };
function renderStory (store, props) {
    return (
        <Provider store={store}>
            <EditorContainer {...props} />
        </Provider>
    );
}
function decoratorContainer (story) {
    return (
        <Container minHeight={size.height} width={size.width}>
            {story()}
        </Container>
    );
}

storiesOf('Editor', module)
    .addDecorator(decoratorContainer)
    .add('10 items', () => {
        const store = configureStore({ editorItems: getRandomItems(10) });
        return renderStory(store, {});
    })
    .add('100 items', () => {
        const store = configureStore({ editorItems: getRandomItems(100) });
        return renderStory(store, {});
    })
    .add('300 items', () => {
        const store = configureStore({ editorItems: getRandomItems(300) });
        return renderStory(store, {});
    });

storiesOf('Editor (grid)', module)
    .addDecorator(decoratorContainer)
    .add('10 items', () => {
        const store = configureStore({ editorItems: getRandomItems(10) });
        return renderStory(store, { grid });
    })
    .add('100 items', () => {
        const store = configureStore({ editorItems: getRandomItems(100) });
        return renderStory(store, { grid });
    })
    .add('300 items', () => {
        const store = configureStore({ editorItems: getRandomItems(300) });
        return renderStory(store, { grid });
    });
