import '../styles';

import './modals';
import './player';

import './baseCanvas';
import './editor';
import './presentation';
import './iVideo';
