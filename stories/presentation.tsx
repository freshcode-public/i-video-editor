import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { Provider } from 'react-redux';

import '../styles/modules';
import { appModules, configureStore, containers } from '../src/index';
import { Container } from './containers';
import { size, getCouchdbHost } from './helpers';

const { PouchDBSyncContainer } = containers;
const { presentation: { Editor, Blogpost } } = appModules;

const presentation = {
    slides: [ '1', '2' ],
    selectId: '1',
    slidesById: {
        '1': { name: 'One', _id: '1' },
        '2': { name: 'Two', _id: '2' },
    }
};

storiesOf('Presentation', module)
    .addDecorator((story) => {
        return (
            <Container minHeight={size.height} width={size.width}>{story()}</Container>
        );
    })
    .add('editor (in memory)', () => {
        const store = configureStore({ presentation: presentation });

        return (
            <Provider store={store}>
                <Editor completedSync dbname={null} />
            </Provider>
        );
    })
    .add('editor (in pouchdb)', () => {
        const store = configureStore();

        return (
            <Provider store={store}>
                <div>
                    <PouchDBSyncContainer dbname={'one'} live  remoteHost={getCouchdbHost()} />
                    <Editor dbname='one' />
                </div>
            </Provider>
        );
    })
    .add('blogpost (in pouchdb)', () => {
        const store = configureStore();

        return (
            <Provider store={store}>
                <div>
                    <PouchDBSyncContainer dbname={'one'} live remoteHost={getCouchdbHost()} />
                    <Blogpost dbname='one' />
                </div>
            </Provider>
        );
    })
