import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { Provider } from 'react-redux';

import '../styles/modules';
import { appModules, configureStore, containers } from '../src/index';
import { Container } from './containers';
import { size, getCouchdbHost } from './helpers';

const { PouchDBSyncContainer } = containers;
const { iVideo: { Editor } } = appModules;


storiesOf('iVideo', module)
    .addDecorator((story) => {
        return (
            <Container minHeight={size.height} width={size.width}>{story()}</Container>
        );
    })
    .add('test (in memory)', () => {
        const store = configureStore({ });

        return (
            <Provider store={store}>
                <div>
                    <Editor completedSync url={'http://clips.vorwaerts-gmbh.de/VfE_html5.mp4'} />
                </div>
            </Provider>
        );
    })
    .add('test (in pouchdb)', () => {
        const store = configureStore({ });
        const dbName = 'two'

        return (
            <Provider store={store}>
                <div>
                    <PouchDBSyncContainer dbname={dbName} live remoteHost={getCouchdbHost()} />
                    <Editor dbname={dbName} url={'http://clips.vorwaerts-gmbh.de/VfE_html5.mp4'} />
                </div>
            </Provider>
        );
    });
