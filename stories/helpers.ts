import { baseCanvasComponents } from '../src/index';
import * as _ from 'lodash';

export const size = baseCanvasComponents.Canvas.defaultProps.editorSize as { width: number; height: number };

export function getRandomItems (count: number, selectCount: number = 0) {
    const items = [];
    const itemsById = {};

    _.forEach(new Array(count), (n, i) => {
        items.push(i.toString());
        itemsById[i] = {
            _id: i.toString(),
            stroke: '#000',
            fill: `rgba(${_.random(255)}, ${_.random(255)}, ${_.random(255)}, .3)`,
            type: 'rect',
            width: 50,
            height: 50,
            left: _.random(0, size.width - 50),
            top: _.random(0, size.height - 50)
        };
    });

    return {
        items: items,
        itemsById: itemsById,
        selectsId: _.take(items, selectCount)
    };
}

export function getCouchdbHost(): string {
    return process.env.COUCHDB_HOST;
}

export interface IProcess {
    env: {
        NODE_ENV: string;
        __DEV__: boolean;
        COUCHDB_HOST: string;
    }
}

declare var process: IProcess;
