import * as React from 'react';
import { storiesOf } from '@storybook/react';

import { uiComponent, modals } from '../src/index';

class WrapPics extends React.Component {
    openModal() {
        (this.refs['modal'] as any).open({ url: 'https://upload.wikimedia.org/wikipedia/commons/b/b4/JPEG_example_JPG_RIP_100.jpg' });
    }

    render () {
        return (
            <div>
                <button onClick={this.openModal.bind(this)}>Open img</button>
                <modals.PicsModal ref='modal' />
            </div>
        );
    }
}

class WrapVideo extends React.Component {
    openModal() {
        (this.refs['modal'] as any).open('http://clips.vorwaerts-gmbh.de/VfE_html5.mp4', 'video/mp4', 'any name');
    }

    render () {
        return (
            <div>
                <button onClick={this.openModal.bind(this)}>Open video</button>
                <modals.VideoModal ref='modal' />
            </div>
        );
    }
}

storiesOf('Modals', module)
    .add('Pure', () => {
        return (
            <uiComponent.Modal openAtCreate title='any title'>
                <h2>any content</h2>
            </uiComponent.Modal>
        );
    })
    .add('large content', () => {
        function renderContent () {
            const child = [];

            for (let i = 0; i<50; i++) {
                child.push(<h2 key={i} children={`${i} any content`} />);
            }
            return child;
        }

        return (
            <uiComponent.Modal openAtCreate title='any title'>
                {renderContent()}
            </uiComponent.Modal>
        );
    })
    .add('pics', () =>  <WrapPics />)
    .add('video', () => <WrapVideo />);
