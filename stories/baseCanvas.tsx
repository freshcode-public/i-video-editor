import * as React from 'react';
import { storiesOf } from '@storybook/react';
const fabric = (require("fabric") as any).fabric;

import { Container } from './containers';
import { baseCanvasComponents, baseCanvasItems } from '../src/index';

import { size, getRandomItems } from './helpers';

const { Canvas } = baseCanvasComponents;
const { Shape, Ellipse, Line, ActiveArea, Image, Photo, Link, Text, Path } = baseCanvasItems;

storiesOf('Base Canvas', module)
    .addDecorator((story) => {
        return (
            <Container minHeight={size.height} width={size.width}>{story()}</Container>
        );
    })
    .add('Empty', () => {
        return (
            <Canvas editorSize={size} />
        );
    })
    .add('Empty (drawingMode)', () => {
        return (
            <Canvas editorSize={size} drawingMode />
        );
    })
    .add('Paht', () => {
        const path =[
            ["M",372,348],["Q",372,348,372.5,348],["Q",373,348,372.25,347.5],["Q",371.5,347,372.5,345],["Q",373.5,343,374.5,341],["Q",375.5,339,376.5,336],["Q",377.5,333,378.5,330],["Q",379.5,327,381,324],["Q",382.5,321,384,317],["Q",385.5,313,388,308.5],["Q",390.5,304,391.5,300.5],["Q",392.5,297,394.5,293],["Q",396.5,289,398.5,284.5],["Q",400.5,280,402,275.5],["Q",403.5,271,406,266],["Q",408.5,261,410,256],["Q",411.5,251,413,247.5],["Q",414.5,244,415.5,239.5],["Q",416.5,235,417.5,231.5],["Q",418.5,228,420,224],["Q",421.5,220,422.5,216.5],["Q",423.5,213,424,209],["Q",424.5,205,426,201.5],["Q",427.5,198,430,194],["Q",432.5,190,433.5,186.5],["Q",434.5,183,435.5,180.5],["Q",436.5,178,438,174.5],["Q",439.5,171,441,167.5],["Q",442.5,164,444,161.5],["Q",445.5,159,446.5,156],["Q",447.5,153,449,149.5],["Q",450.5,146,451.5,143.5],["Q",452.5,141,453.5,139],["Q",454.5,137,454.5,135.5],["Q",454.5,134,455.5,132],["Q",456.5,130,457,128.5],["Q",457.5,127,458.5,125],["Q",459.5,123,460,121],["Q",460.5,119,460.5,118.5],["Q",460.5,118,461,117],["Q",461.5,116,462,116.5],["Q",462.5,117,464,120.5],["Q",465.5,124,466,127.5],["Q",466.5,131,468,135.5],["Q",469.5,140,470.5,145],["Q",471.5,150,472,155],["Q",472.5,160,474,165.5],["Q",475.5,171,476.5,176],["Q",477.5,181,479,187],["Q",480.5,193,482,198.5],["Q",483.5,204,485,209.5],["Q",486.5,215,488,220.5],["Q",489.5,226,491,231.5],["Q",492.5,237,494,243],["Q",495.5,249,497.5,254.5],["Q",499.5,260,501,265],["Q",502.5,270,504,275.5],["Q",505.5,281,507,285.5],["Q",508.5,290,510,294.5],["Q",511.5,299,513,302.5],["Q",514.5,306,516,309.5],["Q",517.5,313,518.5,315],["Q",519.5,317,520,318.5],["Q",520.5,320,522,322],["Q",523.5,324,524.5,326],["Q",525.5,328,526,329],["Q",526.5,330,527,330.5],["Q",527.5,331,528,331.5],["Q",528.5,332,527.5,332.5],["Q",526.5,333,525,333],["Q",523.5,333,521.5,332],["Q",519.5,331,517.5,329.5],["Q",515.5,328,513,325.5],["Q",510.5,323,507.5,321],["Q",504.5,319,500.5,315.5],["Q",496.5,312,492.5,308.5],["Q",488.5,305,484,302],["Q",479.5,299,474.5,295],["Q",469.5,291,464,286.5],["Q",458.5,282,453.5,278.5],["Q",448.5,275,442.5,269.5],["Q",436.5,264,430,259],["Q",423.5,254,416.5,249.5],["Q",409.5,245,403,240.5],["Q",396.5,236,390,231.5],["Q",383.5,227,377,223],["Q",370.5,219,364.5,216],["Q",358.5,213,354,210],["Q",349.5,207,345,204],["Q",340.5,201,338,199.5],["Q",335.5,198,332.5,195],["Q",329.5,192,329,191.5],["Q",328.5,191,328,190.5],["Q",327.5,190,331,190],["Q",334.5,190,340,191.5],["Q",345.5,193,353.5,194],["Q",361.5,195,371.5,195.5],["Q",381.5,196,394,197],["Q",406.5,198,421,198],["Q",435.5,198,449,198],["Q",462.5,198,473,198],["Q",483.5,198,494,198],["Q",504.5,198,513.5,198],["Q",522.5,198,530,197.5],["Q",537.5,197,543.5,196],["Q",549.5,195,554,194.5],["Q",558.5,194,561.5,193],["Q",564.5,192,567,191.5],["Q",569.5,191,571,190],["Q",572.5,189,573,189],["Q",573.5,189,571.5,189.5],["Q",569.5,190,565,191.5],["Q",560.5,193,555.5,195.5],["Q",550.5,198,543.5,200.5],["Q",536.5,203,530.5,205],["Q",524.5,207,518,210],["Q",511.5,213,504.5,216],["Q",497.5,219,491.5,221.5],["Q",485.5,224,479.5,226.5],["Q",473.5,229,467.5,232.5],["Q",461.5,236,456.5,239.5],["Q",451.5,243,447,245],["Q",442.5,247,439,249.5],["Q",435.5,252,431,254],["Q",426.5,256,422.5,258.5],["Q",418.5,261,415,262.5],["Q",411.5,264,408.5,265],["Q",405.5,266,403,267],["Q",400.5,268,398.5,269.5],["Q",396.5,271,394,272.5],["Q",391.5,274,390,275],["Q",388.5,276,388,276.5],["Q",387.5,277,386,278],["Q",384.5,279,384,279.5],["Q",383.5,280,383.5,280.5],["Q",383.5,281,383.5,281.5],["Q",383.5,282,382.5,283],["Q",381.5,284,381.5,284.5],["Q",381.5,285,381,286],["Q",380.5,287,380,288],["Q",379.5,289,379,289.5],["Q",378.5,290,378,291],["Q",377.5,292,377.5,292.5],["Q",377.5,293,377.5,293.5],["Q",377.5,294,377,295.5],["Q",376.5,297,376,297.5],["Q",375.5,298,375,298.5],["Q",374.5,299,373.5,300.5],["Q",372.5,302,372,302.5],["Q",371.5,303,371,304.5],["Q",370.5,306,370,306.5],["Q",369.5,307,369,308],["Q",368.5,309,368.5,310],["Q",368.5,311,367.5,312],["Q",366.5,313,366.5,313.5],["Q",366.5,314,366.5,314.5],["Q",366.5,315,366.5,316],["Q",366.5,317,366,318.5],["Q",365.5,320,365.5,320.5],["Q",365.5,321,365.5,322],["Q",365.5,323,365.5,323.5],["Q",365.5,324,365.5,325],["Q",365.5,326,365.5,326.5],["Q",365.5,327,365.5,328],["Q",365.5,329,365.5,329.5],["Q",365.5,330,365.5,331],["Q",365.5,332,365.5,333.5],["Q",365.5,335,365.5,335.5],["Q",365.5,336,365.5,337.5],["Q",365.5,339,365.5,340],["Q",365.5,341,365.5,341.5],["Q",365.5,342,365.5,343],["Q",365.5,344,365.5,344.5],["L",365.5,345]
        ];

        return (
            <Canvas editorSize={size}>
                <Path top={10} left={100} width={200} height={200} fill='transparent' stroke='red' path={path} />
            </Canvas>
        );
    })
    .add('Rect', () => {
        return (
            <Canvas editorSize={size}>
                <Shape type='rect' top={10} left={100} width={200} height={200} fill='transparent' stroke='red' />
            </Canvas>
        );
    })
    .add('Triangle', () => {
        return (
            <Canvas editorSize={size}>
                <Shape type='triangle' top={10} left={100} width={200} height={200} fill='transparent' stroke='blue' />
            </Canvas>
        );
    })
    .add('Ellipse', () => {
        return (
            <Canvas editorSize={size}>
                <Ellipse type='ellipse' top={250} rx={100} ry={100} left={250} fill='transparent' stroke='yellow' />
            </Canvas>
        );
    })
    .add('Line', () => {
        return (
            <Canvas editorSize={size}>
                <Line type='line' top={10} left={100} height={200} fill='transparent' stroke='red' />
            </Canvas>
        );
    })
    .add('Arrow', () => {
        return (
            <Canvas editorSize={size}>
                <Line type='arrow' top={10} left={100} height={200} fill='transparent' stroke='red' />
            </Canvas>
        );
    })
    .add('ActiveArea', () => {
        return (
            <Canvas editorSize={size}>
                <ActiveArea top={10} left={100} width={200} height={200} />
            </Canvas>
        );
    })
    .add('ActiveArea (invisible)', () => {
        const props = {
            text: 'click me'
        }
        return (
            <Canvas editorSize={size} onObjectActiveArea={(e) => alert('action')}>
                <Shape type='text' {...props} top={50} left={150} width={200} height={200} fill='black' selectable={false} />
                <ActiveArea top={10} left={100} width={200} height={200} selectable={false} />
            </Canvas>
        );
    })
    .add('Textbox', () => {
        const props = {
            text: 'two cows are standing in a field'
        };
        return (
            <Canvas editorSize={size}>
                <Text type='textbox' top={0} left={100} width={200} height={200} fill='transparent' stroke='black' {...props} />
                <Text type='textbox' top={200} left={100} width={200} height={200} fill='black' stroke='transparent' {...props} />
                <Text type='text' top={400} left={100} width={200} height={200} fill='black' selectable={false} {...props} />
            </Canvas>
        );
    })
    .add('Image', () => {
        const url = 'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg';
        return (
            <Canvas editorSize={size}>
                <Image top={10} left={100} width={200} height={200} url={url} />
            </Canvas>
        );
    })
    .add('Image (video)', () => {
        return (
            <ImageVideWrap />
        )
    })
    .add('Photo', () => {
        const url1 = 'http://i.imgur.com/5tMNlyw.jpg';
        const url2 = 'https://static.pexels.com/photos/46710/pexels-photo-46710.jpeg';
        const url3 = 'https://static.pexels.com/photos/465445/pexels-photo-465445.jpeg';

        return (
            <Canvas editorSize={size}>
                <Photo top={10} left={100} width={200} height={200} url={url1} />
                <Photo top={220} left={100} width={200} height={200} url={url2} />
                <Photo top={10} left={320} width={200} height={200} url={url3} />
            </Canvas>
        );
    })
    .add('Link', () => {
        const url1 = 'http://i.imgur.com/5tMNlyw.jpg';
        const url2 = 'https://www.packet.net/media/pages/images/335f5352088d7d9bf74191e006d8e24c/GIwV-rancher.jpg'

        return (
            <Canvas editorSize={size}>
                <Link top={10} left={100} width={200} height={200} url={url1} {...{ to: url1 }} />
                <Link top={250} left={100} width={200} height={200} url={url2} {...{ to: 'freshcode1.ddns.net:9090'}} />
            </Canvas>
        );
    })
    .add('Shapes', () => {
        return (
            <Canvas editorSize={size}>
                <Shape type='rect' top={10} left={100} width={200} height={200} fill='transparent' stroke='red' />
                <Shape type='triangle' top={250} left={10} width={200} height={200} fill='transparent' stroke='blue' />
            </Canvas>
        );
    });

window['cancelRequestAnimFrame'] = (function(){
    return  window.cancelAnimationFrame ||
            window['webkitCancelRequestAnimationFrame'] ||
            window['mozCancelRequestAnimationFrame'] ||
            window['oCancelRequestAnimationFrame'] ||
            window['msCancelRequestAnimationFrame'] ||
            clearTimeout
})();

class ImageVideWrap extends React.Component {
    state = { canPlay: false };
    request = null;

    constructor() {
        super();
        this.renderCanvas = this.renderCanvas.bind(this);
    }

    renderCanvas() {
        const canvasComp: any = this.refs['canvas'];
        if (canvasComp) {
            canvasComp.canvas.renderAll();
            fabric.util.requestAnimFrame(this.renderCanvas);
        }
    }

    componentDidMount () {
        this.request = fabric.util.requestAnimFrame(this.renderCanvas);
    }

    componentWillUnmount(){
        window['cancelRequestAnimFrame'](this.request);
    }

    render () {
        const videoEl: HTMLMediaElement = document.getElementById('___id') as HTMLMediaElement;

        return (
            <div>
                <video
                    controls id='___id' src='http://clips.vorwaerts-gmbh.de/VfE_html5.mp4'
                    onCanPlay={() => this.setState({ canPlay: true })}
                />
                <hr />
                <Canvas editorSize={size} ref='canvas'>
                    {this.state.canPlay && <Image top={5} left={100} width={200} height={200} element={videoEl} />}
                    {this.state.canPlay && <Image top={210} left={10} width={200} height={200} element={videoEl} />}
                </Canvas>
            </div>
        );
    }
}
