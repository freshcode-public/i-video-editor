import * as React from 'react';
import { storiesOf } from '@storybook/react';

import { PlayerComponent } from '../src/index';

storiesOf('Player', module)
    .add('Pure', () => {
        return (
            <PlayerComponent>
                <source src='http://clips.vorwaerts-gmbh.de/VfE_html5.mp4' />
            </PlayerComponent>
        );
    })
    .add('Control panel', () => {
        return (
            <PlayerComponent
                controls
            >
                <source src='http://clips.vorwaerts-gmbh.de/VfE_html5.mp4' />
            </PlayerComponent>
        );
    })
    .add('Auto resize controls panel', () => {
        return (
            <PlayerComponent
                controls
                autoResizePanelPosition
            >
                <source src='http://clips.vorwaerts-gmbh.de/VfE_html5.mp4' />
            </PlayerComponent>
        );
    })
    .add('Auto play', () => {
        return (
            <PlayerComponent
                controls
                autoPlay
            >
                <source src='http://clips.vorwaerts-gmbh.de/VfE_html5.mp4' />
            </PlayerComponent>
        );
    })
    .add('Source(url) is undefined', () => {
        return (
            <PlayerComponent controls />
        );
    })
    .add('URL is invalid', () => {
        return (
            <PlayerComponent
                controls
            >
                <source src='http://clips.vorwaerts-gmbh.de/VfE_html5.mp5' />
            </PlayerComponent>
        );
    });
