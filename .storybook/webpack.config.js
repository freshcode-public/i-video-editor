// load the default config generator.
const genDefaultConfig = require('@storybook/react/dist/server/config/defaults/webpack.config.js');
const path = require('path');
const webpack = require('webpack');
const loadSettings = require('../private/config/index');

const NODE_ENV = process.env.NODE_ENV;
const settings = loadSettings(NODE_ENV);
const isDEV = NODE_ENV === 'development';

module.exports = (baseConfig, env) => {
    const config = genDefaultConfig(baseConfig, env);
    const tsRules = {
        test: /\.(ts|tsx)$/,
        loader: require.resolve('ts-loader')
    };
    const scssRules = {
        test: /\.scss$/,
        loaders: [ "style-loader", "css-loader", "sass-loader" ],
        include: path.resolve(__dirname, '../')
    };

    config.plugins.push(
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(NODE_ENV),
            'process.env.COUCHDB_HOST': JSON.stringify(settings.couch),
            '__DEV__': isDEV
        })
    );

    config.module.rules.push(tsRules, scssRules);
    config.resolve.extensions.push('.ts', '.tsx', '.scss');

    return config;
};
