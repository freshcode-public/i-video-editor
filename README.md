# CanvasEditor v0.0.1

## Running the [storyBook](https://getstorybook.io/) application

```sh
# Developer mode
$ npm run start

# Production mode
$ npm run build:storybook && node tools/server.js
```

### Docker way
```sh
$ docker-compose up -d
$ docker exec -it canvas-editor-app bash
$ npm i

# Developer mode
$ npm run start

# Production mode
$ npm run build:storybook && node tools/server.js
```

### Build editor
```sh
$ npm run build:ts
```

### Tests
```sh
$ npm run test
$ npm run lint
```

#### NOTE:
1. [.gitlab-ci.yml](.gitlab-ci.yml) $WEBHOOK_MASTER_URL - Secret variables in gitlab

> FIXME:

> "babel-plugin-transform-regenerator" and "babel-plugin-transform-runtime" missing in @storybook/react from package.json.

> Remove the dependencies after fix this [issues](https://github.com/storybooks/storybook/issues/1729)
